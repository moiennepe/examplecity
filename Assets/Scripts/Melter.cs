using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melter : Enemy
{
    public override void Start()
    {       
        level = 3;
        xpReward = 30;
        jpReward = 10;
        types = Definition.EnemyTypes.MELTER;
        monsterName = "Melter";

        //array indexes of each stat:
        //STR - 0
        //SPD - 1
        //DEX - 2
        //CON - 3
        //INT - 4
        //WIS - 5
        //LUK - 6
        enemyStats = new int[] { 9, 5, 6, 10, 2, 2, 1 };
        hpStats = 450;
        maxhpStats = hpStats;
        randomNumber = UnityEngine.Random.Range(0, 101);
        weaponDamage = 3;
        coolDownAttack = Random.Range(0, 6);
    }

    public override void Attack()
    {
        if(coolDownAttack > 6)
        {
            randomAttackSkill = UnityEngine.Random.Range(1, 6);
            if (randomAttackSkill <= 2)
            {
                AttackBleedSkill();
            }
            else
            {
                NormalAttack();
            }
            coolDownAttack = 0;
        }      
    }

    void AttackBleedSkill()
    {        
        NormalAttack();
        charactersList[orderCharacterAttack].SetBleedStatus(true);
        // set status to bleeding       
    }

    public override void AddingWinningItem(List<ConsumableItem> consumableItemList, List<Weapon> weaponItemList, List<Armor> armorItemList, List<Accessories> accessoryItemList)
    {
        randomNumber = UnityEngine.Random.Range(0, 101);
        if (randomNumber > 5 && randomNumber < 40)
        {
            ConsumableItem bandage = new ConsumableItem();
            bandage.Init(1, "Bandage");
            bandage.EffectString = "Cure Bleeding";
            bandage.EffectDescription = "Your standard fiber bandage, used to stop bleeding and/or the evacuation of blood cells from your body.";
            bandage.HEALINGEFFECT = 0;
            bandage.SellPrice = 10;
            bandage.BuyPrice = 60;
            bandage.CONSUMABLEITEMTYPE = Definition.ConsumableItemType.BANDAGE;
            bandage.Type = Definition.ItemType.CONSUMABLE;
            consumableItemList.Add(bandage);
        }
        else if (randomNumber < 5)
        {
            ConsumableItem aedReviver = new ConsumableItem();
            aedReviver.Init(1, "AED-Reviver");
            aedReviver.CONSUMABLEITEMTYPE = Definition.ConsumableItemType.REVIVE;
            aedReviver.EffectString = "Revive KO'ed character";
            aedReviver.EffectDescription = "A portable device used by first respondents to resuscitate patients in critical condition, not as good as a cloner but also not as expensive as well.";
            aedReviver.HEALINGEFFECT = 50;
            aedReviver.SellPrice = 39;
            aedReviver.BuyPrice = 235;
            aedReviver.Type = Definition.ItemType.CONSUMABLE;
            consumableItemList.Add(aedReviver);
        }
    }
}
