﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange_Trigger : MonoBehaviour {

    public GameObject camera1;

    public GameObject camera2;

	// Use this for initialization
	void OnTriggerEnter (Collider other) {
        Debug.Log("Object Entered the trigger");
		
		camera1.gameObject.SetActive(false);
		
		camera2.gameObject.SetActive(false);

	}

	// Update is called once per frame
	void OnTriggerStay (Collider other) {
        Debug.Log("Object is within trigger");
		
		camera1.gameObject.SetActive(true);
		
		camera2.gameObject.SetActive(false);

	}

	private void OnTriggerExit(Collider other)
    {
        Debug.Log("Object Exited the trigger");
		
		camera1.gameObject.SetActive(false);
		
		camera2.gameObject.SetActive(true);

	}
}
