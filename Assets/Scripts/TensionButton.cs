﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TensionButton : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    bool skillTarget,isHyperTension,isTeamCombo;
    [SerializeField]
    int skillOrder;
    [SerializeField]
    CombatManagerScript combat_manager;
	void Start () {       
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(TensionButtonFunction);
	}
	
	// Update is called once per frame	

    public void SetCombatManager(CombatManagerScript _combatManager)
    {
        combat_manager = _combatManager;
    }

    void TensionButtonFunction()
    {
        combat_manager.SetPreviousTensionButton(this.gameObject);
        combat_manager.SetDescriptionTensionUI(skillOrder);
    }

    public void SetSkillOrder(int number)
    {
        skillOrder = number;
    }

    public void SetAllyTargetBool(bool isTarget)
    {
        skillTarget = isTarget;
    }

    public bool GetAllyTarget()
    {
        return skillTarget;
    }   

    public void SetHyperTensionBool(bool _isHyperTension)
    {
        isHyperTension = _isHyperTension;
    }

    public void SetTeamComboBool(bool _isTeamCombo)
    {
        isTeamCombo = _isTeamCombo;
    }

    public bool GetIsTeamCombo()
    {
        return isTeamCombo;
    }

    public bool GetIsHyperTension()
    {
        return isHyperTension;
    }
}
