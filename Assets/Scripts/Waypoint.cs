﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Waypoint : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    string wayPointName;
	void Start () {
        wayPointName = this.gameObject.name;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void TeleportCharacterToDifferentArea()
    {
        SceneManager.LoadScene(wayPointName);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Player")
        {
            TeleportCharacterToDifferentArea();
        }
    }
}
