﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : Item {
    public override void Init(int _amount, string _itemName)
    {
        base.Init(_amount, _itemName);
        type = Definition.ItemType.KEYITEM;
    }
}
