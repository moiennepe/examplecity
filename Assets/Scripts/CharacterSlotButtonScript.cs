﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSlotButtonScript : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    int characterIndex;
    [SerializeField]
    CombatManagerScript combat_manager;
    [SerializeField]
    Definition.CharacterName characterType;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(CharacterSlotFunction);
    }       

    public int GetCharacterIndex()
    {
        return characterIndex;
    }

    public void SetIndex(int _index)
    {
        characterIndex = _index;
    }

    public Definition.CharacterName GetCharacterNameType()
    {
        return characterType;
    }

    public void SetCharacterNameType(Definition.CharacterName _characterType)
    {
        characterType = _characterType;
    }

    public void SetCombatManager(CombatManagerScript _combat_manager)
    {
        combat_manager = _combat_manager;
    }

    void CharacterSlotFunction()
    {
        if(combat_manager)
        {
            combat_manager.SetSelectedGameObject(characterIndex,characterType);
        }
    }
}
