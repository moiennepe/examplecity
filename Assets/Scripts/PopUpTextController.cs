﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpTextController : MonoBehaviour {

    // Use this for initialization
   
    private static FloatingText popUpText;

    private static GameObject canvas;

    public static void Init()
    {
        canvas = GameObject.Find("UI_Canvas");
        popUpText = Resources.Load<FloatingText>("Pop_Up_Text_Parent");
    }

    public static void CreatePopUpText(string text, Transform location)
    {
        FloatingText instance = Instantiate(popUpText);
        instance.gameObject.SetActive(true);
        instance.transform.SetParent(canvas.transform, false);
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(location.position);
        instance.transform.position = screenPosition;
        instance.SetText(text);
    }

    public static void CreatePopUpTextColorNewVersion(string text, Transform location, Color color)
    {
        FloatingText instance = Instantiate(popUpText);
        instance.gameObject.SetActive(true);
        instance.transform.SetParent(canvas.transform, false);
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(location.position);
        instance.transform.position = screenPosition;
        instance.SetText(text);
        instance.SetColor(color);
    }
}
