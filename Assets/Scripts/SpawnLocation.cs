﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLocation : MonoBehaviour {

    [SerializeField]
    bool isColliding;
	// Use this for initialization
	void Start () {
        gameObject.GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
       
	}  

    public bool GetCollidingStatus()
    {
        return isColliding;
    }

    public void SetSpawnReserved()
    {
        isColliding = true;
    }

    public void SetUnReserve()
    {
        isColliding = false;
    }
}
