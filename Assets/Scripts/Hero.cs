﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Hero : Character
{
    enum HeroSkill
    {
        JABRUSH,
        CROSSUPER,
        HIGHRISER,
        COUNT
    }
	
	public Hero(List<EntityStats> stats) : base (stats)
	{

	}

    public override void Start()
    {
        playerStats.EXP = 0;
        playerStats.JP = 0;
        playerStats.Level = 1;
        playerStats.JPLevel = 1;
        apStats = 4;
        maxAPStats = apStats;
        tension = 0;
        maxTension = 200;
        playerStats.MaxJpExp = 500;
        playerStats.MaxExp = 1200;
        hyperTension = 0;
        bleedStatusTimer = 0;
        aggro = 35;

        playerStats.HP = 100;

        characterNameType = Definition.CharacterName.HERO;
        characterRole = Definition.CharacterRoles.SLAYER;
        characterSkillList = new List<CharacterSkill>();
        tensionList = new List<Tension>();        
        // skill init()
        HeroSkillInit();
        // tension init
        HeroTensionInit();        
        characterName = "Hero";
        
        SetBleedStatus(true);
        damage = playerStats.STR;
        isUnlockedHyperTension = true;
        originDamage = damage;
		Weapon studdedGloves = new Weapon();
		studdedGloves.Init(1, "Studded Gloves");
		studdedGloves.SetWeaponDamage(17);
		studdedGloves.SetWeaponForHero(characterNameType);
		studdedGloves.EffectString = "17 Attack";
		studdedGloves.EffectDescription = "Biker gloves with protective studs over the knuckles, good for punching things and people.";
		studdedGloves.SellPrice = -1;
		studdedGloves.BuyPrice = -1;
		studdedGloves.Type = Definition.ItemType.WEAPON;
		EquipWeapon(studdedGloves, null);
	}

    public override void LevelUpStats()
    {
		base.LevelUpStats();
		// We get a new ability at 3
        if (playerStats.Level == 3)
        {
			Tension skillTest1 = new Tension();
			skillTest1.AddTensionDescription("Heal yourself or an Ally for 50 HP");
			skillTest1.AddTensionName("Medkit");
			skillTest1.TARGETALLY = true;
			skillTest1.TOTALAP = 0;
			skillTest1.TOTALTENSION = 50;
			tensionList.Add(skillTest1);
		}
    }

	public override void LevelUpJob()
	{
		base.LevelUpJob();
		if (playerStats.JPLevel == 2)
		{
			Tension skillTest = new Tension();
			skillTest.AddTensionDescription("Damage is increased by 1.5");
			skillTest.AddTensionName("Attack Up");
			skillTest.TARGETALLY = false;
			skillTest.TOTALAP = 0;
			skillTest.TOTALTENSION = 30;
			tensionList.Add(skillTest);
		}
	}

	CharacterSkill.Ability JabRush = (Character c) => 
	{
		c.Tension += 5;
		c.HyperTension += 2.5f;
		return (int)(c.Damage * 2.5f);
	};
	CharacterSkill.Ability HighRiser = (Character c) =>
	{
		c.Tension += 6;
		c.HyperTension += 3;
		return (int)(c.Damage * 6);
	};
	CharacterSkill.Ability CrossUpper = (Character c) =>
	{
		c.Tension += 6;
		c.HyperTension += 3;
		return (int)(c.Damage * 9);
	};

	void HeroSkillInit()
    {
        CharacterSkill jabRush = new CharacterSkill("Jab Rush", JabRush);
		jabRush.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
		jabRush.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
		jabRush.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
		jabRush.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
		characterSkillList.Add(jabRush);

        CharacterSkill highRiser = new CharacterSkill("High Riser", HighRiser);
        highRiser.AddSkillCombo(Definition.NormalAttackType.MEDIUMATTACK);
        highRiser.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        characterSkillList.Add(highRiser);

        CharacterSkill crossUpper = new CharacterSkill("Cross Upper", CrossUpper);
        crossUpper.AddSkillCombo(Definition.NormalAttackType.HEAVYATTACK);
        crossUpper.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        characterSkillList.Add(crossUpper);
	}           

    void HeroTensionInit()
    {
        // init hyper tension and team combo here
        Tension hyperTension = new Tension();
        hyperTension.AddTensionDescription("Deal 5 critical hits to all enemies on the screen, give attack up status (stacked 3 times)  to all allies.");
        hyperTension.AddTensionName("Inward Smash");
        hyperTension.TARGETALLY = false;
        hyperTension.TOTALAP = 0;
        hyperTension.TOTALTENSION = 0;
        hyperTension.HYPERTENSION = true;
        tensionList.Add(hyperTension);

        Tension teamcombo = new Tension();
        teamcombo.AddTensionDescription("Characters Needed: Hero & Slayer");
        teamcombo.AddTensionName("Critical X");
        teamcombo.TARGETALLY = false;
        teamcombo.TOTALAP = 0;
        teamcombo.TOTALTENSION = 0;
        teamcombo.TEAMCOMBO = true;
        tensionList.Add(teamcombo);
    }

    public override void UsingTensionSkill(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly,int allyOrderInCombat)
    {        
        if (skillOrder == 2)
        {
            // do attack up          
            SetAttackStatus(true);         
            PopUpTextController.CreatePopUpTextColorNewVersion("Attack Up", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
            tension -= tensionList[skillOrder].TOTALTENSION;
            AddStatusEffect(new StatusEffect(OnStartSetAggroToHigh, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 3)
        {
            // do medkit
            targetAlly.HP += 50;
            PopUpTextController.CreatePopUpTextColorNewVersion("50 HP", combatMangerScript.GetCharacterList()[allyOrderInCombat].transform, Color.green);
            PopUpTextController.CreatePopUpTextColorNewVersion("Medkit", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
            if (targetAlly.HP > targetAlly.MaxHP)
            {
                targetAlly.HP = targetAlly.MaxHP;
            }
            tension -= tensionList[skillOrder].TOTALTENSION;
            AddStatusEffect(new StatusEffect(OnStartSetAggroToHigh, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 0) // hyper tension
        {
            DoHyperTension(skillOrder, enemyList, targetEnemy, characterList, targetAlly, allyOrderInCombat);
            AddStatusEffect(new StatusEffect(OnStartSetAggroToExtreme, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 1) // team combo
        {

        }
    }

    public override void DoHyperTension(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
    {        
        int totalDamageDeal = damage * 15;

		foreach (Enemy enemy in enemyList)
        {
			enemy.HP -= totalDamageDeal;
            PopUpTextController.CreatePopUpTextColorNewVersion(totalDamageDeal.ToString(), enemy.gameObject.transform, Color.yellow);
        }

        foreach (Character ch in characterList)
        {
			ch.SetAttackStatus(true, 3);
        }
        hyperTension = 0;
    }
}

