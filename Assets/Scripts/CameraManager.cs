﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    GameObject player;   
    Vector3 offset,offsetY;
    [SerializeField]
    float horizontalSpeed,verticalSpeed;
    float yaw, pitch = 0;

    public float turnSpeed = 4.0f;

    void Start ()
    { 
        player = GameObject.Find("Player");
        offset = new Vector3(-6, 6, 0);

		// Look at the player on start
		transform.position = player.transform.position + offset;
		transform.LookAt(player.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
      
    }
    private void LateUpdate()
    {
       if(player)
        {
			// Right click - Orbit camera
			if (Input.GetMouseButton(1))
				CameraOrbitPlayer();
			else
			{
				transform.position = player.transform.position + offset;
				transform.LookAt(player.transform.position);
			}
		}      
    }

	void CameraOrbitPlayer()
	{
		offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
		transform.position = player.transform.position + offset;
		transform.LookAt(player.transform.position);
	}

	// Unused
	void RotateObject()
    {
        float rotationX = Input.GetAxis("Mouse X");
        float rotationy = Input.GetAxis("Mouse Y");

        Camera cam = Camera.main;
        Vector3 cameraUp = cam.transform.TransformDirection(Vector3.up);
        Vector3 cameraDown = cam.transform.TransformDirection(Vector3.right);
        Vector3 objectiveUp = player.transform.InverseTransformDirection(cameraUp);
        Vector3 objectiveDown = player.transform.InverseTransformDirection(cameraDown);

        Quaternion modelRotate = Quaternion.AngleAxis(-rotationX / player.transform.localScale.x * 2, objectiveUp)
            * Quaternion.AngleAxis(rotationy / player.transform.localScale.x * 2, objectiveDown);

        player.transform.rotation *= modelRotate;
    }

	// Unused
    void FPSStyleCamera()
    {
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            yaw += horizontalSpeed * Input.GetAxis("Mouse X");
            pitch -= verticalSpeed * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(pitch, yaw, 0);
        }
    }

    
}
