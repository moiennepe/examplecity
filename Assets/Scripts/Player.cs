﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Use this for initialization
    static Player _instance;

    int randomNumber;

    [SerializeField]
    float playerSpeed;
    Inventory playerInventory;
    List<Character> currenteamCharacterList;

    [SerializeField]
    Definition.EnemyTypes enemyTypeCounter = Definition.EnemyTypes.DOUND;
    [SerializeField]    
    Definition.StartCombatConditions startCombatCondition = Definition.StartCombatConditions.PLAYERENEMYCOLLISION;
    [SerializeField]
    string lastSceneCollideEnemy;
    
    bool invicible,destroyAfterWin;

    float invicibleTimer;



    private void Awake()
    {
        PlayerInit();
    }



	//int[][][] charStats = { };
	Dictionary<string, List<EntityStats>> charStats = new Dictionary<string, List<EntityStats>>();


	void ReadCSVStats()
	{
		// 3 characters right now, 3 levels, 8 stats per level
		string file = ((TextAsset)Resources.Load("Stats - Character")).text;

		charStats.Clear();

		string currChar = "";
		bool doneWithChar = true;
		foreach (string line in file.Split('\n'))
		{
			string[] stats = line.Split(',');
			if (stats.Length < 2 || stats[1] == "")
			{
				doneWithChar = true;
			}
			else if (stats[1] == "HP")
			{
				currChar = stats[0];
				charStats.Add(currChar, new List<EntityStats>());
				doneWithChar = false;
				continue;
			}
			if (!doneWithChar)
			{
				EntityStats levelStats = new EntityStats();
				levelStats.HP = System.Int32.Parse(stats[1]);
				levelStats.STR = System.Int32.Parse(stats[2]);
				levelStats.SPD = System.Int32.Parse(stats[3]);
				levelStats.DEX = System.Int32.Parse(stats[4]);
				levelStats.CON = System.Int32.Parse(stats[5]);
				levelStats.INT = System.Int32.Parse(stats[6]);
				levelStats.WIS = System.Int32.Parse(stats[7]);
				levelStats.LUK = System.Int32.Parse(stats[8]);
				charStats[currChar].Add(levelStats);
			}
		}
		doneWithChar = true;
	}

    void Start()
    {
        randomNumber = Random.Range(0, 10);
        ReadCSVStats();
        Hero hero = new Hero(charStats["Hero"]);
        Slayer slayer = new Slayer(charStats["Slayer"]);
        Sparen sparen = new Sparen(charStats["Sparen"]);
        hero.Start();
        slayer.Start();
        sparen.Start();

        currenteamCharacterList = new List<Character>();
        currenteamCharacterList.Add(hero);
        currenteamCharacterList.Add(slayer);
        currenteamCharacterList.Add(sparen);

        playerInventory = new Inventory();
        playerInventory.InventoryInit();

        invicibleTimer = 0;
        destroyAfterWin = false;
       
    }

    // Update is called once per frame
    void Update()
    {      
        Move();
        transform.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);

        if(invicible == true)
        {
            invicibleTimer += Time.deltaTime;
            if(invicibleTimer >= 2.0f)
            {
                invicibleTimer = 0;
                invicible = false;
            }
        }
    }

    private void FixedUpdate()
    {

    }

    void Move()
    {
        if (Input.GetKey(KeyCode.D))
        {
            gameObject.transform.Translate(playerSpeed, 0, 0);
        }

        else if (Input.GetKey(KeyCode.A))
        {
            gameObject.transform.Translate(-playerSpeed, 0, 0);
        }

        else if (Input.GetKey(KeyCode.W))
        {
            gameObject.transform.Translate(0, 0, playerSpeed);
        }

        else if (Input.GetKey(KeyCode.S))
        {
            gameObject.transform.Translate(0, 0, -playerSpeed);
        }
        else if (Input.GetKey(KeyCode.Q))
        {
            Attack();
        }
    }

    void Attack()
    {
       
            if (HitEnemy())
            {
            startCombatCondition = Definition.StartCombatConditions.PLAYERATTACKSENEMY;            
            StartCombatScene();
            }
          
    }

    bool HitEnemy()
    {
        int layerMask = 1 << 8;
        layerMask = ~layerMask;
        RaycastHit hit;        
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 10.0f, layerMask))
        {
            if (hit.collider.gameObject.tag == "Enemy" && randomNumber>7)
            {                
                return true;
            }      
            
        }
        randomNumber = Random.Range(0, 10);
        Debug.Log("missed enemy");
        return false;        
    }

    void StartCombatScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Combat_Scene");
    }



    public Inventory GetPlayerInventory()
    {
        return playerInventory;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (destroyAfterWin)
        {
            Destroy(collision.gameObject);
            destroyAfterWin = false;
            invicible = true;
        }

        if (invicible == false)
        {
            if (collision.gameObject.tag == "Enemy")
            {
                if (collision.gameObject.name == "Dound")
                {
                    enemyTypeCounter = Definition.EnemyTypes.DOUND;
                }
                else if (collision.gameObject.name == "Melter")
                {
                    enemyTypeCounter = Definition.EnemyTypes.MELTER;
                }
                else if (collision.gameObject.name == "Shadow")
                {
                    enemyTypeCounter = Definition.EnemyTypes.SHADOW;
                }
                else if (collision.gameObject.name == "Unknown Dream")
                {
                    enemyTypeCounter = Definition.EnemyTypes.BOSSUNKNOWNDREAM;
                }
                lastSceneCollideEnemy = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
                UnityEngine.SceneManagement.SceneManager.LoadScene("Combat_Scene");
            }
        }           
    }     

    void PlayerInit()
    {
        // check to see if there is a player script in the scene
        if (!_instance)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public List<Character> GetCharacterList()
    {
        return currenteamCharacterList;
    }

    public Definition.EnemyTypes ENEMYCOUNTERTYPE
    {
        get
        {
            return enemyTypeCounter;
        }
        set
        {
            enemyTypeCounter = value;
        }
    }

    public Definition.StartCombatConditions STARTCOMBATCONDITIONS
    {
        get
        {
            return startCombatCondition;
        }
        set
        {
            startCombatCondition = value;
        }
    }

    public string GetLastScene()
    {
        return lastSceneCollideEnemy;
    }

    public void switchInvicibleState()
    {
        invicible = !invicible;
    }

    public void SwitchDestroyAfterWin()
    {
        destroyAfterWin = true;
    }
}
