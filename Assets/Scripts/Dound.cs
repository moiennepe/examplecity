using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dound : Enemy
{
    bool isAbleToSummon;

    public override void Start()
    {        
        level = 1;
        xpReward = 20;
        jpReward = 5;
     
        types = Definition.EnemyTypes.DOUND;

        monsterName = "Dound";
        //array indexes of each stat:
        //STR - 0
        //SPD - 1
        //DEX - 2
        //CON - 3
        //INT - 4
        //WIS - 5
        //LUK - 6
        enemyStats = new int[] { 5, 8, 6, 5, 2, 2, 3 };
        hpStats = 225;
        maxhpStats = hpStats;
        weaponDamage = 2;
        randomNumber = UnityEngine.Random.Range(0, 101);
        isAbleToSummon = true;
        coolDownAttack = Random.Range(0, 6);
    }

    public override void Attack()
    {
        if(coolDownAttack > 6)
        {
            randomAttackSkill = 1;
            if (randomAttackSkill == 1)
            {
                if(combat_manager.GetEnemyList().Count < combat_manager.GetTotalMaxMonster() && isAbleToSummon)
					SummonSkill();
                else if(combat_manager.GetEnemyList().Count >= combat_manager.GetTotalMaxMonster())
                {
                    NormalAttack();
                }
            }
            else
            {
                NormalAttack();
            }
            coolDownAttack = 0;
        }       
    }

    void SummonSkill()
    {
        // summon another dound      
        combat_manager.SummonMonster(Definition.EnemyTypes.DOUND);
        isAbleToSummon = false;
    }    

    public override void AddingWinningItem(List<ConsumableItem> consumableItemList, List<Weapon> weaponItemList, List<Armor> armorItemList, List<Accessories> accessoryItemList)
    {
        randomNumber = UnityEngine.Random.Range(0, 101);
        if (randomNumber > 10 && randomNumber < 30)
        {
            ConsumableItem healingAmpul = new ConsumableItem();
            healingAmpul.Init(1, "Healing Ampul");
            healingAmpul.EffectString = "Healing 50 HP";
            healingAmpul.EffectDescription = "A over the counter healing item that can be found at your local pharmacy. Used to instantly heal minor injuries";
            healingAmpul.HEALINGEFFECT = 50;
            healingAmpul.SellPrice = 35;
            healingAmpul.BuyPrice = 150;
            healingAmpul.CONSUMABLEITEMTYPE = Definition.ConsumableItemType.POTION;
            healingAmpul.Type = Definition.ItemType.CONSUMABLE;
            consumableItemList.Add(healingAmpul);
        }
        else if (randomNumber < 10)
        {
            ConsumableItem energydrink = new ConsumableItem();
            energydrink.Init(1, "Energy Drink");
            energydrink.CONSUMABLEITEMTYPE = Definition.ConsumableItemType.ENERGYDRINK;
            energydrink.EffectString = "Restores 25 Tension";
            energydrink.EffectDescription = "A common tool of college students and overnight workers, drink it to get that extra boost in energy! The warning label on the back seems rather small though...";
            energydrink.HEALINGEFFECT = 25;
            energydrink.SellPrice = 300;
            energydrink.BuyPrice = 43;
            energydrink.Type = Definition.ItemType.CONSUMABLE;
            consumableItemList.Add(energydrink);
        }
    }
}
