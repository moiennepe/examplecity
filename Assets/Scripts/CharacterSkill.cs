﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSkill {

    // Use this for initialization
    List<Definition.NormalAttackType> combo_input_list;
    string skillName;
    int totalAp;

	public delegate int Ability(Character c);
	Ability ability;

    public CharacterSkill(string _skillName, Ability _ability)
    {
        combo_input_list = new List<Definition.NormalAttackType>();
        skillName = _skillName;
		ability = _ability;
    }

    public void AddSkillCombo(Definition.NormalAttackType attackType)
    {
		combo_input_list.Add(attackType);

        if(attackType == Definition.NormalAttackType.LIGHTATTACK)
            totalAp += 1;
        else if (attackType == Definition.NormalAttackType.MEDIUMATTACK)
            totalAp += 2;
        else if (attackType == Definition.NormalAttackType.HEAVYATTACK)
            totalAp += 3;
    }

	public int ActivateSkill(Character character)
	{
		return ability(character);
	}

    public List<Definition.NormalAttackType> GetComboInput()
    {
        return combo_input_list;
    }

    public string GetSkillName()
    {
        return skillName;
    }

    public int GetTotalAp()
    {
        return totalAp;
    }
}
