﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item {

    protected int amount;
    protected string name;
    protected Definition.ItemType type;
    protected string effect;
    protected string effectDescription;
    protected int sellPrice;
    protected int buyPrice;

	public virtual void Init(int _amount, string _itemName)
    {
        amount = _amount;
        name = _itemName;
    }

    public virtual void SetItemEffectString(string _effect)
    {
        effect = _effect;
    }

    public virtual void SetItemDescription(string _effectDes)
    {
        effectDescription = _effectDes;
    }

    public int Amount
    {
        get
        {
            return amount;
        }
        set
        {
            amount = value;
        }
    }

    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }  

    public string EffectString
    {
        get
        {
            return effect;
        }
        set
        {
            effect = value;
        }
    }

    public string EffectDescription
    {
        get
        {
            return effectDescription;
        }
        set
        {
            effectDescription = value;
        }
    }

    public int BuyPrice
    {
        get
        {
            return buyPrice;
        }
        set
        {
            buyPrice = value;
        }
    }

    public int SellPrice
    {
        get
        {
            return sellPrice;
        }
        set
        {
            sellPrice = value;
        }
    }

    public Definition.ItemType Type
    {
        get
        {
            return type;
        }
        set
        {
            type = value;
        }
    }
    
}
