﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {

    // Use this for initialization
    public Animator animator;
    Text damageText;

    void Start()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        Destroy(this.gameObject, clipInfo[0].clip.length - 0.1f);
        damageText = animator.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetText(string text)
    {
        animator.GetComponent<Text>().text = text;
    }

    public void SetColor(Color color)
    {
        animator.GetComponent<Text>().color = color;     
    }
}
