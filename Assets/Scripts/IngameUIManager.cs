﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IngameUIManager : MonoBehaviour
{

    // Use this for initialization
    [SerializeField]
    GameObject uiPanel, pauseUIPanel, optionPanel, inventoryPanel, previousPanel, savePanel, kitPanel, partyPanel, partySlotPanel, item_list_panel,
      character_list_item_use, yes_or_no_confirm_panel; // all ui panel
    [SerializeField]
    GameObject masterVolumeInput, soundVolumeInput, musicVolumeInput; // option input
    [SerializeField]
    float masterVolume, soundVolume, musicVolume; // storing input as int
    [SerializeField]
    GameObject optionButton, exitButton, inventoryButton, saveButton, kitButton, partyButton, cancelUseButton, yesConfirmButton, noConfirmButton; // button
    [SerializeField]
    GameObject player;
    [SerializeField] // prefab place
    GameObject prefab_reserveSlotImage, prefab_characterSlotImage, prefab_item_scroll_button, prefab_hero_scroll_button;
    [SerializeField]
    List<GameObject> character_slot_ui_list, itemListScroll, characterListScroll; // list
    [SerializeField]
    GameObject goldText; // inventory text
    [SerializeField]
    GameObject item_scroll_view, item_Content, comsumableButton, weaponButton, keyitemButton, armorButton, useItemButton, hero_Content, accessoriesButton; // item scroll 
    [SerializeField]
    int getItemOrder, getHeroOrder;
    [SerializeField]
    Definition.ItemType itemTypeToUse;
    [SerializeField]
    GameObject effectTittleText, effectDescriptionText;

    bool firstTime = true;
    bool test = true;
    float screenScale;

    void Start()
    {
        screenScale = Screen.width / 1920.0f;
        UI_Init();
        ReadOptionFile();
    }

    // Update is called once per frame
    void Update()
    {
        PauseFunction();
        OptionUpdate();
        UI_Update();
    }

    void PauseFunction()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseUIPanel.activeSelf)
            {
                character_list_item_use.SetActive(false);
                item_list_panel.SetActive(true);
                previousPanel.SetActive(false);
                pauseUIPanel.SetActive(false);
                getItemOrder = -1;
                yes_or_no_confirm_panel.SetActive(false);

                for (int i = 0; i < itemListScroll.Count; i++)
                {
                    DestroyImmediate(itemListScroll[i]);
                }
                itemListScroll.Clear();

                if (characterListScroll.Count > 0)
                {
                    for (int i = 0; i < characterListScroll.Count; i++)
                    {
                        DestroyImmediate(characterListScroll[i]);
                    }
                    characterListScroll.Clear();
                }
            }
            else
            {
                InitPartySlot();
                pauseUIPanel.SetActive(true);
                previousPanel = partyPanel;
                partyPanel.SetActive(true);
            }
        }
        SaveOptionToNoteFile();
    }

    void OptionButton()
    {
        previousPanel.SetActive(false);
        optionPanel.SetActive(true);
        previousPanel = optionPanel;
    }

    void InventoryButton()
    {
        previousPanel.SetActive(false);
        inventoryPanel.SetActive(true);
        previousPanel = inventoryPanel;
        getItemOrder = -1;
        effectDescriptionText.GetComponent<Text>().text = "";
        effectTittleText.GetComponent<Text>().text = "";

        for (int i = 0; i < itemListScroll.Count; i++)
        {
            DestroyImmediate(itemListScroll[i]);
        }
        itemListScroll.Clear();

        if (characterListScroll.Count > 0)
        {
            for (int i = 0; i < characterListScroll.Count; i++)
            {
                DestroyImmediate(characterListScroll[i]);
            }
            characterListScroll.Clear();
        }

        character_list_item_use.SetActive(false);
        item_list_panel.SetActive(true);
        yes_or_no_confirm_panel.SetActive(false);

    }

    void KitButton()
    {
        previousPanel.SetActive(false);
        kitPanel.SetActive(true);
        previousPanel = kitPanel;
    }

    void SaveButton()
    {
        previousPanel.SetActive(false);
        savePanel.SetActive(true);
        previousPanel = savePanel;
    }

    void PartyButton()
    {
        previousPanel.SetActive(false);
        partyPanel.SetActive(true);
        previousPanel = partyPanel;
        InitPartySlot();
    }

    void ConsumableButton()
    {
        RefreshInventoryUI(Definition.ItemType.CONSUMABLE);
        effectDescriptionText.GetComponent<Text>().text = "";
        effectTittleText.GetComponent<Text>().text = "";
        getItemOrder = -1;
    }

    void ArmorButton()
    {
        RefreshInventoryUI(Definition.ItemType.ARMOR);
        effectDescriptionText.GetComponent<Text>().text = "";
        effectTittleText.GetComponent<Text>().text = "";
        getItemOrder = -1;
    }

    void WeaponButton()
    {
        RefreshInventoryUI(Definition.ItemType.WEAPON);
        effectDescriptionText.GetComponent<Text>().text = "";
        effectTittleText.GetComponent<Text>().text = "";
        getItemOrder = -1;
    }

    void KeyItemButton()
    {
        RefreshInventoryUI(Definition.ItemType.KEYITEM);
        effectDescriptionText.GetComponent<Text>().text = "";
        effectTittleText.GetComponent<Text>().text = "";
        getItemOrder = -1;
    }

    void AccessoriesButton()
    {
        RefreshInventoryUI(Definition.ItemType.ACCESSORIES);
        effectDescriptionText.GetComponent<Text>().text = "";
        effectTittleText.GetComponent<Text>().text = "";
        getItemOrder = -1;
    }

    void UseItemButton()
    {
        if (itemTypeToUse == Definition.ItemType.CONSUMABLE)
        {
            OpenCharacterListToUseConsumableType();
        }
        else if(itemTypeToUse == Definition.ItemType.ARMOR || itemTypeToUse == Definition.ItemType.WEAPON || itemTypeToUse == Definition.ItemType.ACCESSORIES)
        {
            OpenNormalCharacterList();
        }
    }

    void CancelUseItem()
    {
        character_list_item_use.SetActive(false);
        item_list_panel.SetActive(true);
    }

    void ConfirmNoUseItem()
    {
        yes_or_no_confirm_panel.SetActive(false);
    }

    void ConfirmYesUseItem()
    {
        if (itemTypeToUse == Definition.ItemType.CONSUMABLE)
        {
            if (player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.POTION)
            {
                player.GetComponent<Player>().GetCharacterList()[getHeroOrder].HP += player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].HEALINGEFFECT;
                if (player.GetComponent<Player>().GetCharacterList()[getHeroOrder].HP > player.GetComponent<Player>().GetCharacterList()[getHeroOrder].MaxHP)
                {
                    player.GetComponent<Player>().GetCharacterList()[getHeroOrder].HP = player.GetComponent<Player>().GetCharacterList()[getHeroOrder].MaxHP;
                }
            }
            else if (player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.REVIVE)
            {
                float hp = player.GetComponent<Player>().GetCharacterList()[getHeroOrder].MaxHP * 0.50f;
                Mathf.Round(hp);
                player.GetComponent<Player>().GetCharacterList()[getHeroOrder].HP += (int)hp;
            }

            yes_or_no_confirm_panel.SetActive(false);
            character_list_item_use.SetActive(false);
            item_list_panel.SetActive(true);

            if (player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].Amount - 1 > 0)
            {
                player.GetComponent<Player>().GetPlayerInventory().UseItem(itemTypeToUse, getItemOrder);
                itemListScroll[getItemOrder].GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].Name + " x " +
                player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].Amount;
            }
            else
            {
                player.GetComponent<Player>().GetPlayerInventory().UseItem(itemTypeToUse, getItemOrder);
                for (int i = 0; i < itemListScroll.Count; i++)
                {
                    DestroyImmediate(itemListScroll[i]);
                }
                itemListScroll.Clear();
                InitConsumableInventoryUI();

                getItemOrder = -1;
                effectDescriptionText.GetComponent<Text>().text = "";
                effectTittleText.GetComponent<Text>().text = "";
            }
        }
        else if (itemTypeToUse == Definition.ItemType.ARMOR || itemTypeToUse == Definition.ItemType.WEAPON || itemTypeToUse == Definition.ItemType.ACCESSORIES)
        {
            if(itemTypeToUse == Definition.ItemType.WEAPON)
            {
                player.GetComponent<Player>().GetCharacterList()[getHeroOrder].EquipWeapon(player.GetComponent<Player>().GetPlayerInventory().GetWeaponInvent()[getItemOrder]
                    ,player.GetComponent<Player>());
            }
            else if(itemTypeToUse == Definition.ItemType.ARMOR)
            {
                player.GetComponent<Player>().GetCharacterList()[getHeroOrder].EquipArmor(player.GetComponent<Player>().GetPlayerInventory().GetArmorInvent()[getItemOrder]
                                   , player.GetComponent<Player>());
            }

            yes_or_no_confirm_panel.SetActive(false);
            character_list_item_use.SetActive(false);
            item_list_panel.SetActive(true);

            for (int i = 0; i < itemListScroll.Count; i++)
            {
                DestroyImmediate(itemListScroll[i]);
            }
            itemListScroll.Clear();

            if (itemTypeToUse == Definition.ItemType.WEAPON)
                InitWeaponInventoryUI();
            else if (itemTypeToUse == Definition.ItemType.ARMOR)
                InitArmorInventoryUI();

            getItemOrder = -1;
            effectDescriptionText.GetComponent<Text>().text = "";
            effectTittleText.GetComponent<Text>().text = "";
        }
    }

    void UI_Init()
    {
        getItemOrder = -1;

        character_slot_ui_list = new List<GameObject>();
        characterListScroll = new List<GameObject>();

        uiPanel = GameObject.Find("UI_Panel");

        pauseUIPanel = GameObject.Find("Pause_UI_Panel");
        optionPanel = GameObject.Find("option_setting_panel");
        inventoryPanel = GameObject.Find("inventory_panel");
        savePanel = GameObject.Find("save_and_load_panel");
        partyPanel = GameObject.Find("party_panel");
        kitPanel = GameObject.Find("kit_panel");
        partySlotPanel = GameObject.Find("party_slot_panel");
        item_list_panel = GameObject.Find("item_list_panel");
        character_list_item_use = GameObject.Find("character_use_item_list");
        hero_Content = GameObject.Find("hero_Content");
        yes_or_no_confirm_panel = GameObject.Find("yes_or_no_confirm_use_item_panel");

        previousPanel = partyPanel;

        item_scroll_view = GameObject.Find("item_scroll_view");
        item_Content = GameObject.Find("item_Content");

        optionButton = GameObject.Find("option_button");
        exitButton = GameObject.Find("exit_button");
        inventoryButton = GameObject.Find("inventory_button");
        saveButton = GameObject.Find("save_and_load_button");
        kitButton = GameObject.Find("kit_button");
        partyButton = GameObject.Find("party_button");

        comsumableButton = GameObject.Find("consumable_button");
        weaponButton = GameObject.Find("weapon_button");
        armorButton = GameObject.Find("armor_button");
        keyitemButton = GameObject.Find("key_item_button");
        accessoriesButton = GameObject.Find("accessories_button");
        useItemButton = GameObject.Find("use_buton");
        cancelUseButton = GameObject.Find("cancel_use_button");
        yesConfirmButton = GameObject.Find("yes_confirm");
        noConfirmButton = GameObject.Find("no_confirm");

        goldText = GameObject.Find("gold_text");

        masterVolumeInput = GameObject.Find("master_volume_input");
        soundVolumeInput = GameObject.Find("sound_volume_input");
        musicVolumeInput = GameObject.Find("music_volume_input");

        effectTittleText = GameObject.Find("effect_title_text");
        effectDescriptionText = GameObject.Find("item_description_text ");

        optionButton.GetComponent<Button>().onClick.AddListener(OptionButton);
        exitButton.GetComponent<Button>().onClick.AddListener(ExitButtonFunction);
        inventoryButton.GetComponent<Button>().onClick.AddListener(InventoryButton);
        saveButton.GetComponent<Button>().onClick.AddListener(SaveButton);
        kitButton.GetComponent<Button>().onClick.AddListener(KitButton);
        partyButton.GetComponent<Button>().onClick.AddListener(PartyButton);
        comsumableButton.GetComponent<Button>().onClick.AddListener(ConsumableButton);
        weaponButton.GetComponent<Button>().onClick.AddListener(WeaponButton);
        armorButton.GetComponent<Button>().onClick.AddListener(ArmorButton);
        keyitemButton.GetComponent<Button>().onClick.AddListener(KeyItemButton);
        accessoriesButton.GetComponent<Button>().onClick.AddListener(AccessoriesButton);
        useItemButton.GetComponent<Button>().onClick.AddListener(UseItemButton);
        cancelUseButton.GetComponent<Button>().onClick.AddListener(CancelUseItem);
        yesConfirmButton.GetComponent<Button>().onClick.AddListener(ConfirmYesUseItem);
        noConfirmButton.GetComponent<Button>().onClick.AddListener(ConfirmNoUseItem);

        pauseUIPanel.SetActive(false);
        optionPanel.SetActive(false);
        inventoryPanel.SetActive(false);
        savePanel.SetActive(false);
        kitPanel.SetActive(false);
        character_list_item_use.SetActive(false);
        yes_or_no_confirm_panel.SetActive(false);

        // place holder input init in game        

        masterVolumeInput.GetComponent<InputField>().text = masterVolume.ToString();
        musicVolumeInput.GetComponent<InputField>().text = musicVolume.ToString();
        soundVolumeInput.GetComponent<InputField>().text = soundVolume.ToString();

        prefab_characterSlotImage = Resources.Load<GameObject>("character_slot_buttonImage");
        prefab_item_scroll_button = Resources.Load<GameObject>("item_button");
        prefab_hero_scroll_button = Resources.Load<GameObject>("hero_use_item_button");

        InitPartySlot();

        itemListScroll = new List<GameObject>();
    }

    void OptionUpdate()
    {
         if(masterVolumeInput.GetComponent<InputField>().text == "")
        {
            masterVolumeInput.GetComponent<InputField>().text = masterVolume.ToString();
           
        }
        else
        masterVolume = float.Parse(masterVolumeInput.GetComponent<InputField>().text);

        if (musicVolumeInput.GetComponent<InputField>().text == "")
        {
            musicVolumeInput.GetComponent<InputField>().text = musicVolume.ToString();

        }
        else
        musicVolume = float.Parse(musicVolumeInput.GetComponent<InputField>().text);

        if (soundVolumeInput.GetComponent<InputField>().text == "")
        {
            soundVolumeInput.GetComponent<InputField>().text = soundVolume.ToString();

        }
        else
        soundVolume = float.Parse(soundVolumeInput.GetComponent<InputField>().text);

        MusicManager.masterVolume = masterVolume;
        MusicManager.musicVolume = musicVolume;
        MusicManager.soundEffectVolume = soundVolume;
    }

    void ExitButtonFunction()
    {
        Destroy(player);
        SceneManager.LoadScene("MainMenu");
    }

    void InitPartySlot()
    {
        // init ui party 
        if (character_slot_ui_list.Count > 0)
        {
            for (int i = 0; i < character_slot_ui_list.Count; i++)
            {
                DestroyImmediate(character_slot_ui_list[i]);
            }
            character_slot_ui_list.Clear();
        }

        player = GameObject.Find("Player");

        for (int i = 0; i < player.GetComponent<Player>().GetCharacterList().Count; i++)
        {
            float lengthPartySlot = prefab_characterSlotImage.GetComponent<RectTransform>().rect.width;

            GameObject temp = Instantiate(prefab_characterSlotImage, partySlotPanel.transform);

            temp.transform.Translate((i * temp.GetComponent<RectTransform>().rect.width) * screenScale, 0, 0);

            Text[] textTemp = temp.GetComponentsInChildren<Text>();

            List<Character> tempList = player.GetComponent<Player>().GetCharacterList();

            textTemp[(int)Definition.CharacterSlotUIText.NAME].text = tempList[i].GetCharacterName; // character name

            if (tempList[i].GetCharacterRole == Definition.CharacterRoles.GUARDIAN) // character role
            {
                textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Guardian";
            }

            else if (tempList[i].GetCharacterRole == Definition.CharacterRoles.SLAYER) // character role
            {
                textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Fighter";
            }

            else if (tempList[i].GetCharacterRole == Definition.CharacterRoles.SUPPORTER) // character role
            {
                textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Medic";
            }

            else if (tempList[i].GetCharacterRole == Definition.CharacterRoles.REBUKER) // character role
            {
                textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Guardian";
            }

            textTemp[(int)Definition.CharacterSlotUIText.LEVEL].text = "Level " + tempList[i].GetCharacterLevel.ToString() + " | EXP:" + tempList[i].Exp; // level

            textTemp[(int)Definition.CharacterSlotUIText.JOB].text = "Job " + tempList[i].GetCharacerJob.ToString() + " | EXP:" + tempList[i].JP; // job

            textTemp[(int)Definition.CharacterSlotUIText.HP].text = "HP -  " + tempList[i].HP + "/" + tempList[i].MaxHP; //hp

            textTemp[(int)Definition.CharacterSlotUIText.AP].text = "AP - " + tempList[i].AP; // ap

            textTemp[(int)Definition.CharacterSlotUIText.STR].text = "STR - " + tempList[i].GetPlayerStats.STR.ToString();
            textTemp[(int)Definition.CharacterSlotUIText.SPD].text = "SPD - " + tempList[i].GetPlayerStats.SPD.ToString();
            textTemp[(int)Definition.CharacterSlotUIText.DEX].text = "DEX - " + tempList[i].GetPlayerStats.DEX.ToString();
            textTemp[(int)Definition.CharacterSlotUIText.CON].text = "CON - " + tempList[i].GetPlayerStats.CON.ToString();
            textTemp[(int)Definition.CharacterSlotUIText.INT].text = "INT - " + tempList[i].GetPlayerStats.INT.ToString();
            textTemp[(int)Definition.CharacterSlotUIText.WIS].text = "WIS - " + tempList[i].GetPlayerStats.WIS.ToString();
            textTemp[(int)Definition.CharacterSlotUIText.LUK].text = "LUK - " + tempList[i].GetPlayerStats.LUK.ToString();

            Image[] imageTemp = temp.GetComponentsInChildren<Image>();

            if (tempList[i].GetCharacterNameType == Definition.CharacterName.HERO)
            {
                imageTemp[1].sprite = Resources.Load<Sprite>("Hero_First_Concept_Art");
            }

            else if (tempList[i].GetCharacterNameType == Definition.CharacterName.SLAYER)
            {
                imageTemp[1].sprite = Resources.Load<Sprite>("SlayerFirstConcept_TrickerTriger_1801");
            }

            else if (tempList[i].GetCharacterNameType == Definition.CharacterName.SPARREN)
            {
                imageTemp[1].sprite = Resources.Load<Sprite>("SparenFirstConcept_TrickerTriger1801");
            }

            if (tempList[i].HP == 0)
                imageTemp[1].color = new Color(255.0f, 0, 0);

            character_slot_ui_list.Add(temp);
        }
    }

    public void AddPartySlot()
    {
        float lengthPartySlot = prefab_characterSlotImage.GetComponent<RectTransform>().rect.width;

        GameObject temp = Instantiate(prefab_characterSlotImage, partySlotPanel.transform);
        temp.transform.Translate((lengthPartySlot * character_slot_ui_list.Count) * screenScale, 0, 0);

        Text[] textTemp = temp.GetComponentsInChildren<Text>();

        List<Character> tempList = player.GetComponent<Player>().GetCharacterList();

        textTemp[(int)Definition.CharacterSlotUIText.NAME].text = tempList[tempList.Count - 1].GetCharacterName; // character name

        if (tempList[tempList.Count - 1].GetCharacterRole == Definition.CharacterRoles.GUARDIAN) // character role
        {
            textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Guardian";
        }

        else if (tempList[tempList.Count - 1].GetCharacterRole == Definition.CharacterRoles.SLAYER) // character role
        {
            textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Fighter";
        }

        else if (tempList[tempList.Count - 1].GetCharacterRole == Definition.CharacterRoles.SUPPORTER) // character role
        {
            textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Medic";
        }

        else if (tempList[tempList.Count - 1].GetCharacterRole == Definition.CharacterRoles.REBUKER) // character role
        {
            textTemp[(int)Definition.CharacterSlotUIText.ROLE].text = "Guardian";
        }

        textTemp[(int)Definition.CharacterSlotUIText.LEVEL].text = "Level " + tempList[tempList.Count - 1].GetCharacterLevel.ToString() + " | EXP:" + tempList[tempList.Count - 1].Exp; // level

        textTemp[(int)Definition.CharacterSlotUIText.JOB].text = "Job " + tempList[tempList.Count - 1].GetCharacerJob.ToString() + " | EXP:" + tempList[tempList.Count - 1].JP; // job

        textTemp[(int)Definition.CharacterSlotUIText.HP].text = "HP -  " + tempList[tempList.Count - 1].HP + "/" + tempList[tempList.Count - 1].MaxHP; //hp

        textTemp[(int)Definition.CharacterSlotUIText.AP].text = "AP - " + tempList[tempList.Count - 1].AP; // ap

        textTemp[(int)Definition.CharacterSlotUIText.STR].text = "STR - " + tempList[tempList.Count - 1].GetPlayerStats.STR.ToString();
        textTemp[(int)Definition.CharacterSlotUIText.SPD].text = "SPD - " + tempList[tempList.Count - 1].GetPlayerStats.SPD.ToString();
        textTemp[(int)Definition.CharacterSlotUIText.DEX].text = "DEX - " + tempList[tempList.Count - 1].GetPlayerStats.DEX.ToString();
        textTemp[(int)Definition.CharacterSlotUIText.CON].text = "CON - " + tempList[tempList.Count - 1].GetPlayerStats.CON.ToString();
        textTemp[(int)Definition.CharacterSlotUIText.INT].text = "INT - " + tempList[tempList.Count - 1].GetPlayerStats.INT.ToString();
        textTemp[(int)Definition.CharacterSlotUIText.WIS].text = "WIS - " + tempList[tempList.Count - 1].GetPlayerStats.WIS.ToString();
        textTemp[(int)Definition.CharacterSlotUIText.LUK].text = "LUK - " + tempList[tempList.Count - 1].GetPlayerStats.LUK.ToString();

        Image[] imageTemp = temp.GetComponentsInChildren<Image>();

        if (tempList[tempList.Count - 1].GetCharacterNameType == Definition.CharacterName.HERO)
        {
            imageTemp[1].sprite = Resources.Load<Sprite>("Hero_First_Concept_Art");
        }

        else if (tempList[tempList.Count - 1].GetCharacterNameType == Definition.CharacterName.SLAYER)
        {
            imageTemp[1].sprite = Resources.Load<Sprite>("SlayerFirstConcept_TrickerTriger_1801");
        }

        else if (tempList[tempList.Count - 1].GetCharacterNameType == Definition.CharacterName.SPARREN)
        {
            imageTemp[1].sprite = Resources.Load<Sprite>("SparenFirstConcept_TrickerTriger1801");
        }
        character_slot_ui_list.Add(temp);
    }

    void UI_Update()
    {
        if (inventoryPanel.activeSelf)
            goldText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().CREDITS.ToString();
    }

    void RefreshInventoryUI(Definition.ItemType _itemtype)
    {
        if (inventoryPanel.activeSelf == true)
        {
            for (int i = 0; i < itemListScroll.Count; i++)
            {
                DestroyImmediate(itemListScroll[i]);
            }
            itemListScroll.Clear();
        }

        if (_itemtype == Definition.ItemType.CONSUMABLE)
        {
            InitConsumableInventoryUI();
        }

        else if (_itemtype == Definition.ItemType.ARMOR)
        {
            InitArmorInventoryUI();
        }

        else if (_itemtype == Definition.ItemType.WEAPON)
        {
            InitWeaponInventoryUI();
        }

        else if (_itemtype == Definition.ItemType.KEYITEM)
        {
            InitKeyItemInventoryUI();
        }
        else if (_itemtype == Definition.ItemType.ACCESSORIES)
        {
            InitAccessoriesInventoryUI();
        }
    }

    void InitConsumableInventoryUI()
    {
        for (int i = 0; i < player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory().Count; i++)
        {
            GameObject temp = Instantiate(prefab_item_scroll_button, item_Content.transform);
            temp.transform.Translate(0, (-i * temp.GetComponent<RectTransform>().rect.height) * screenScale, 0);
            temp.GetComponent<ItemButton>().SLOTORDER = i;
            temp.GetComponent<ItemButton>().SetItemType(Definition.ItemType.CONSUMABLE);
            temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[i].Name + " x " +
                player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[i].Amount;
            temp.GetComponent<ItemButton>().SetInGameScript(this);
            itemListScroll.Add(temp);
        }
    }

    void InitWeaponInventoryUI()
    {
        for (int i = 0; i < player.GetComponent<Player>().GetPlayerInventory().GetWeaponInvent().Count; i++)
        {
            GameObject temp = Instantiate(prefab_item_scroll_button, item_Content.transform);
            temp.transform.Translate(0, -i * temp.GetComponent<RectTransform>().rect.height, 0);
            temp.GetComponent<ItemButton>().SLOTORDER = i;
            temp.GetComponent<ItemButton>().SetItemType(Definition.ItemType.WEAPON);
            temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetWeaponInvent()[i].Name;
            temp.GetComponent<ItemButton>().SetInGameScript(this);
            itemListScroll.Add(temp);
        }
    }

    void InitArmorInventoryUI()
    {
        for (int i = 0; i < player.GetComponent<Player>().GetPlayerInventory().GetArmorInvent().Count; i++)
        {
            GameObject temp = Instantiate(prefab_item_scroll_button, item_Content.transform);
            temp.transform.Translate(0, -i * temp.GetComponent<RectTransform>().rect.height, 0);
            temp.GetComponent<ItemButton>().SLOTORDER = i;
            temp.GetComponent<ItemButton>().SetItemType(Definition.ItemType.ARMOR);
            temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetArmorInvent()[i].Name;
            temp.GetComponent<ItemButton>().SetInGameScript(this);
            itemListScroll.Add(temp);
        }
    }

    void InitKeyItemInventoryUI()
    {
        for (int i = 0; i < player.GetComponent<Player>().GetPlayerInventory().GetKeyItemInvent().Count; i++)
        {
            GameObject temp = Instantiate(prefab_item_scroll_button, item_Content.transform);
            temp.transform.Translate(0, -i * temp.GetComponent<RectTransform>().rect.height, 0);
            temp.GetComponent<ItemButton>().SLOTORDER = i;
            temp.GetComponent<ItemButton>().SetItemType(Definition.ItemType.KEYITEM);
            itemListScroll.Add(temp);
            temp.GetComponent<ItemButton>().SetInGameScript(this);
        }
    }

    void InitAccessoriesInventoryUI()
    {
        for (int i = 0; i < player.GetComponent<Player>().GetPlayerInventory().GetAccessoriesInvent().Count; i++)
        {
            GameObject temp = Instantiate(prefab_item_scroll_button, item_Content.transform);
            temp.transform.Translate(0, -i * temp.GetComponent<RectTransform>().rect.height, 0);
            temp.GetComponent<ItemButton>().SLOTORDER = i;
            temp.GetComponent<ItemButton>().SetItemType(Definition.ItemType.ACCESSORIES);
            itemListScroll.Add(temp);
            temp.GetComponent<ItemButton>().SetInGameScript(this);
        }
    }

    public void StoreItemUseClickInformation(int _slot, Definition.ItemType _type)
    {
        getItemOrder = _slot;
        itemTypeToUse = _type;
        if (itemTypeToUse == Definition.ItemType.CONSUMABLE)
        {
            effectDescriptionText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].EffectDescription;
            effectTittleText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].EffectString;
        }

        else if (itemTypeToUse == Definition.ItemType.ARMOR)
        {
            effectDescriptionText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetArmorInvent()[getItemOrder].EffectDescription;
            effectTittleText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetArmorInvent()[getItemOrder].EffectString;
        }

        else if (itemTypeToUse == Definition.ItemType.WEAPON)
        {
            effectDescriptionText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetWeaponInvent()[getItemOrder].EffectDescription;
            effectTittleText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetWeaponInvent()[getItemOrder].EffectString;
        }

        else if (itemTypeToUse == Definition.ItemType.KEYITEM)
        {
            effectDescriptionText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetKeyItemInvent()[getItemOrder].EffectDescription;
            effectTittleText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetKeyItemInvent()[getItemOrder].EffectString;
        }
        else if (itemTypeToUse == Definition.ItemType.ACCESSORIES)
        {
            effectDescriptionText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetAccessoriesInvent()[getItemOrder].EffectDescription;
            effectTittleText.GetComponent<Text>().text = player.GetComponent<Player>().GetPlayerInventory().GetAccessoriesInvent()[getItemOrder].EffectString;
        }
    }

    void OpenCharacterListToUseConsumableType()
    {
        RefreshCharacterUseList();

        if (getItemOrder != -1)
        {
            if (player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.POTION ||
                 player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.REVIVE)
            {
                item_list_panel.SetActive(false);
                character_list_item_use.SetActive(true);
                // depend on how the item effect is the list will be different
                if (player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.POTION)
                {
                    for (int i = 0; i < player.GetComponent<Player>().GetCharacterList().Count; i++)
                    {
                        if (player.GetComponent<Player>().GetCharacterList()[i].HP < player.GetComponent<Player>().GetCharacterList()[i].MaxHP)
                        {
                            GameObject temp = Instantiate(prefab_hero_scroll_button, hero_Content.transform);
                            temp.transform.Translate(0, (-i * temp.GetComponent<RectTransform>().rect.height) * screenScale, 0);
                            temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetCharacterList()[i].GetCharacterName;
                            temp.GetComponent<HeroItemUseButton>().SetIngameUIScript(this);
                            temp.GetComponent<HeroItemUseButton>().HEROORDER = i;
                            characterListScroll.Add(temp);
                        }
                    }
                }
                else if (player.GetComponent<Player>().GetPlayerInventory().GetConsumableItemInventory()[getItemOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.REVIVE)
                {
                    for (int i = 0; i < player.GetComponent<Player>().GetCharacterList().Count; i++)
                    {
                        if (player.GetComponent<Player>().GetCharacterList()[i].HP <= 0)
                        {
                            GameObject temp = Instantiate(prefab_hero_scroll_button, hero_Content.transform);
                            temp.transform.Translate(0, (-i * temp.GetComponent<RectTransform>().rect.height) * screenScale, 0);
                            temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetCharacterList()[i].GetCharacterName;
                            temp.GetComponent<HeroItemUseButton>().SetIngameUIScript(this);
                            temp.GetComponent<HeroItemUseButton>().HEROORDER = i;
                            characterListScroll.Add(temp);
                        }
                    }
                }
            }
        }
    }

    void OpenNormalCharacterList()
    {
        RefreshCharacterUseList();

        if (getItemOrder != -1)
        {
            item_list_panel.SetActive(false);
            character_list_item_use.SetActive(true);
            if(itemTypeToUse == Definition.ItemType.WEAPON)
            {
                for (int i = 0; i < player.GetComponent<Player>().GetCharacterList().Count; i++)
                {
                    if (player.GetComponent<Player>().GetCharacterList()[i].GetCharacterNameType == 
                        player.GetComponent<Player>().GetPlayerInventory().GetWeaponInvent()[getItemOrder].GetWeaponHeroType())
                    {
                        GameObject temp = Instantiate(prefab_hero_scroll_button, hero_Content.transform);
                        temp.transform.Translate(0, (-i * temp.GetComponent<RectTransform>().rect.height) * screenScale, 0);
                        temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetCharacterList()[i].GetCharacterName;
                        temp.GetComponent<HeroItemUseButton>().SetIngameUIScript(this);
                        temp.GetComponent<HeroItemUseButton>().HEROORDER = i;
                        characterListScroll.Add(temp);
                    }
                }
            }
            else if(itemTypeToUse == Definition.ItemType.ARMOR || itemTypeToUse == Definition.ItemType.ACCESSORIES)
            {
                for (int i = 0; i < player.GetComponent<Player>().GetCharacterList().Count; i++)
                {

                    GameObject temp = Instantiate(prefab_hero_scroll_button, hero_Content.transform);
                    temp.transform.Translate(0, (-i * temp.GetComponent<RectTransform>().rect.height) * screenScale, 0);
                    temp.GetComponentInChildren<Text>().text = player.GetComponent<Player>().GetCharacterList()[i].GetCharacterName;
                    temp.GetComponent<HeroItemUseButton>().SetIngameUIScript(this);
                    temp.GetComponent<HeroItemUseButton>().HEROORDER = i;
                    characterListScroll.Add(temp);
                }
            }         
        }
    }

    void RefreshCharacterUseList()
    {
        if (characterListScroll.Count > 0)
        {
            for (int i = 0; i < characterListScroll.Count; i++)
            {
                DestroyImmediate(characterListScroll[i]);
            }
            characterListScroll.Clear();
        }
    }

    public void OpenYesOrNoUse(int _heroOrder)
    {
        getHeroOrder = _heroOrder;
        yes_or_no_confirm_panel.SetActive(true);
    }      

    void SaveOptionToNoteFile()
    {
        StreamWriter writer = new StreamWriter("Option.txt", false);
        writer.WriteLine(masterVolume);
        writer.WriteLine(musicVolume);
        writer.WriteLine(soundVolume);
        writer.Close();
    }

    void ReadOptionFile()
    {
        StreamReader reader = new StreamReader("Option.txt", true);
        masterVolume = float.Parse(reader.ReadLine());
        musicVolume = float.Parse(reader.ReadLine());
        soundVolume = float.Parse(reader.ReadLine());

        masterVolumeInput.GetComponent<InputField>().text = masterVolume.ToString();
        musicVolumeInput.GetComponent<InputField>().text = musicVolume.ToString();
        soundVolumeInput.GetComponent<InputField>().text = soundVolume.ToString();

        reader.Close();
    }
}
