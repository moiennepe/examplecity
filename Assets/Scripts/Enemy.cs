﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    // Use this for initialization      
    [SerializeField]
    protected int randomNumber,level, hpStats, maxhpStats,randomNumberForReward, weaponDamage,randomAttackSkill,orderCharacterAttack;
    [SerializeField]
    protected int[] enemyStats;
    [SerializeField]
    protected string monsterName;
    [SerializeField]
    protected Definition.EnemyTypes types;
    [SerializeField]
    protected List<Character> charactersList;
    [SerializeField]
    protected Character currentTarget;  
    [SerializeField]
    protected float coolDownAttack;
    [SerializeField]
    protected CombatManagerScript combat_manager;
    [SerializeField]
    protected static int xpReward, jpReward, credits;
    [SerializeField]
    protected int spawnPositionSlot;


    bool losing = false;

    public int HP
    {
        get
        {
            return hpStats;
        }
        set
        {
            hpStats = value;
        }

    }

    public int MAXHP
    {
        get
        {
            return maxhpStats;
        }
        set
        {
           maxhpStats = value;
        }

    }

    public string GetName()
    {
        return monsterName;
    }

    public Definition.EnemyTypes GetEnemyTypes()
    {
        return types;
    }
    
    public int[] GetEnemyStats()
    {
        return enemyStats;
    }

    public virtual void Update()
    {       
        if(!losing)
        {
            coolDownAttack += Time.deltaTime;
            if (currentTarget != null && currentTarget.HP > 0)
            {
                Attack();
            }
            else if (currentTarget.HP <= 0 || currentTarget == null)
            {
                SelectTargetForAttack();
            }
        }       
    }    

    public virtual void Start()
    {        
         
    }

    public virtual void SelectTargetForAttack()
    {
        int time = 0;
		//Try finding a living target <party size> times, could target a dead character
        while(true)
        {
            currentTarget = chooseTarget(charactersList);
            Debug.Log(currentTarget);
            if(currentTarget.HP > 0)
            {
                coolDownAttack = 0;
                break;
            }
            time++;
            if (time >= charactersList.Count)
            {
                losing = true;
                break;
            }                
        }       
    }

    public Character chooseTarget(List<Character> charactersList)
    {
        //if character aggro level = 50 enemies will attack them to the exclusion of everyone else, aka "extreme" level from design doc
        int cumul = 0;
        int listLength = charactersList.Count;
        if (listLength < 1) return null;
        int[] aggroValues = new int[charactersList.Count];
        for (int i = 0; i < listLength; i++)
        {
            if (charactersList[i].Aggro == 50) return charactersList[i];
            cumul = cumul + charactersList[i].Aggro;
        }
        int randomNumber = UnityEngine.Random.Range(0, cumul);
        for (int i = 0; i < listLength; i++)
        {
            cumul = cumul - charactersList[i].Aggro;
            if (randomNumber >= cumul) return charactersList[i];
        }
        return charactersList[0];
    }

    public virtual void Attack()
    {
             
    }
    
    public virtual void NormalAttack()
    {
        if (CheckIfPhysicalAttackHit(currentTarget))
        {
            int totalDamage = weaponDamage * enemyStats[0] * UnityEngine.Random.Range(4, 7) / currentTarget.GetPlayerStats.CON;

            if (currentTarget.GetGuardedStatus)
            {
                currentTarget.GetBodyGuard.HP -= totalDamage;
                PopUpTextController.CreatePopUpTextColorNewVersion("-" + totalDamage, combat_manager.GetCharacterList()[currentTarget.GetBodyGuard.GetNumberInOrderCombat].transform,Color.red);
                currentTarget.GetBodyGuard.SetGuardingSomeoneStatus(false);
                PopUpTextController.CreatePopUpTextColorNewVersion("Blocked", combat_manager.GetCharacterList()[orderCharacterAttack].transform,Color.green);
                if (currentTarget.HP - totalDamage > 0)
                {
                    currentTarget.GetBodyGuard.Tension += 40;
                    if (currentTarget.GetBodyGuard.Tension >= currentTarget.GetBodyGuard.MaxTension)
                    {
                        currentTarget.GetBodyGuard.Tension = currentTarget.GetBodyGuard.MaxTension;
                    }
                }
                hpStats -= currentTarget.GetBodyGuard.Damage;
                PopUpTextController.CreatePopUpTextColorNewVersion("-" + currentTarget.GetBodyGuard.Damage, this.transform,Color.red);
                currentTarget.SetGuardingStatus(false, null);
            }
            else if(currentTarget.GetSuperGuardedStatus)
            {
                currentTarget.HP -= (int)(totalDamage * 0.3f);
                PopUpTextController.CreatePopUpTextColorNewVersion("-" + ((int)(totalDamage * 0.3f)).ToString(), combat_manager.GetCharacterList()[orderCharacterAttack].transform, Color.red);
                hpStats -= (int)(totalDamage * 0.7f);
                PopUpTextController.CreatePopUpTextColorNewVersion("-" + ((int)(totalDamage * 0.7f)).ToString(), this.transform, Color.red);
            }
            else
            {
                currentTarget.HP = currentTarget.HP - totalDamage;
                PopUpTextController.CreatePopUpTextColorNewVersion("-" + totalDamage.ToString(), combat_manager.GetCharacterList()[orderCharacterAttack].transform,Color.red);
            }
        }
        SelectTargetForAttack();
    }  

    public virtual bool CheckIfPhysicalAttackHit(Character target)
    {
        if(target != null)
        {
            if (enemyStats[2] * 9 / 10 + 1 - target.GetPlayerStats.DEX / 2 > 0) return true;
        }      
        return false;
    }
    
    public void SetCharacterList(List<Character> updatedCharactersList)
    {
        charactersList = new List<Character>();
        for (int i = 0; i < updatedCharactersList.Count;i ++)
        {
            charactersList.Add(updatedCharactersList[i]);
        }
    }

    public int GetLevel()
    {
        return level;
    }

    public int GetXPReward()
    {
        return xpReward;
    }

    public int GetJPReward()
    {
        return jpReward;
    }

    public void SetCombatManagerScript(CombatManagerScript _script)
    {
        combat_manager = _script;
    }

    public int GetCreditReward()
    {
        return credits;
    }

    public virtual void AddingWinningItem(List <ConsumableItem> consumableItemList, List<Weapon> weaponItemList, List<Armor> armorItemList, List<Accessories> accessoryItemList)
    {

    }

    //public virtual string GetItemReward()
    //{
    //    return Definition.EnemyTypes[0];
    //}

    public void SetSlotSpawnnPosition(int _slot)
    {
        spawnPositionSlot = _slot;
    }

    public int GetSlotSpawnPosition()
    {
        return spawnPositionSlot;
    }
}


