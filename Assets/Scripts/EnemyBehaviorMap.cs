﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class EnemyBehaviorMap : MonoBehaviour
{
    public Transform PlayerTransform;

    protected string playerTag;

    protected int currentWaypoint, randomNumber;

    protected float attackCooldownTime, distanceToPlayer, playerAwarenessRadius, waypointAccuracy, attackDistance, chaseTime, initialChaseTime, surprisedTime, wanderRadius, wanderTime, initialWanderTime, enemyFOVAngle, maxRadius, wanderDistanceXAxis, wanderDistanceZAxis;

    protected bool isChasing, playerNoticed;

    Vector3 startingPosition, wanderDestination;
    Transform playerLocation;
    NavMeshAgent agent;

    protected List<GameObject> waypoints;


    // Use this for initialization
    void Start()
    {
        playerTag = "Player";

        currentWaypoint = 0;
        randomNumber = Random.Range(0, 10);

        playerAwarenessRadius = 5.0f;
        attackDistance = 3.0f;
        initialChaseTime = 10.0f;
        chaseTime = initialChaseTime;
        initialWanderTime = 3.0f;
        wanderTime = initialWanderTime;
        surprisedTime = 10.0f;
        enemyFOVAngle = 60.0f;
        maxRadius = 180.0f;
        wanderRadius = 10.0f;
        wanderDistanceXAxis = 3.0f;
        wanderDistanceZAxis = 3.0f;
        waypointAccuracy = 3.0f;
        attackCooldownTime = 3.0f;

        isChasing = false;
        playerNoticed = false;

        startingPosition = transform.position;
        playerLocation = GameObject.FindGameObjectWithTag(playerTag).transform;
        PlayerTransform = playerLocation;
        agent = GetComponent<NavMeshAgent>();
        wanderDestination = new Vector3(Random.Range(-wanderDistanceXAxis, wanderDistanceXAxis), transform.position.y, Random.Range(-wanderDistanceZAxis, wanderDistanceZAxis));
        
        waypoints = new List<GameObject>();
        PopulateWaypoints();
    }

    // Update is called once per frame
    void Update()
    {
        distanceToPlayer = Vector3.Distance(playerLocation.position, transform.position);

        if (playerNoticed)
        {
            if (PlayerIsAttackable())
            {
                AttackPlayer();
            }
            else
            {
                ChasePlayer();
                chaseTime -= Time.deltaTime;
                if (chaseTime <=0) playerNoticed = false;
            }
        }

        else if (PlayerInFOV(transform, playerLocation, enemyFOVAngle, maxRadius))
        {
            playerNoticed = true;
        }
        else if (PlayerInAwarenessZone())
        {
            BeSurprised();
            playerNoticed = true;
        }
        else Patrol();
    }

    void PopulateWaypoints()
    {
        foreach (Transform child in agent.transform.parent)
        {            
            if (child.gameObject.tag == "Waypoint")
            {
                waypoints.Add(child.gameObject);
            }
        }
    }

    void ChasePlayer()
    {
        agent.SetDestination(playerLocation.position);
        chaseTime -= Time.deltaTime;
        if (PlayerIsAttackable()) AttackPlayer();
    }

    void GoHome()
    {
        agent.SetDestination(startingPosition);
    }

    void Patrol()
    {
        if (waypoints.Count <= 0) return;
        if (Vector3.Distance(waypoints[currentWaypoint].transform.position, transform.position)<waypointAccuracy)
        {
            currentWaypoint++;
            if(currentWaypoint >= waypoints.Count)
            {
                currentWaypoint = 0;
            }
        }
        agent.SetDestination(waypoints[currentWaypoint].transform.position);
    }

    void Wander()
    {
        agent.SetDestination(wanderDestination);
        wanderTime -= Time.deltaTime;
        Debug.Log(wanderTime);
    }
        
    void AttackPlayer()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(playerLocation.position - transform.position), 5.0f);
        if (randomNumber > 51) StartCombatScene();
        else
        {
            Debug.Log("missed player");
            randomNumber = Random.Range(0, 10);
            return;
        }
    }

    void StartCombatScene()
    {
        
        UnityEngine.SceneManagement.SceneManager.LoadScene("Combat_Scene");
    }

    void BeSurprised()
    {
        surprisedTime -= Time.deltaTime;
    }

    

    bool AtStartingPosition()
    {
        if (Vector3.Distance(startingPosition, transform.position) < 0.5f) return true;
        return false;
    }

    bool PlayerInAwarenessZone()
    {
        if (distanceToPlayer > playerAwarenessRadius) return false;
        return true;
    }

    bool PlayerIsAttackable()
    {
        if (PlayerInFOV(transform, playerLocation, enemyFOVAngle, maxRadius) && distanceToPlayer < attackDistance) return true;
        return false;
    }

    bool PlayerInFOV(Transform checkingObject, Transform target, float FOVAngle, float maxRayRadius)
    {
        Vector3 targetDir = target.position - transform.position;
        float angleToPlayer = (Vector3.Angle(targetDir, transform.forward));

        if (angleToPlayer >= -FOVAngle && angleToPlayer <= FOVAngle)
        {           
            Ray ray = new Ray(checkingObject.position, target.position - checkingObject.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, maxRayRadius))
            {
                if (hit.transform == target)
                {
                    return true;
                }
            }
        }

        return false;

    }
}