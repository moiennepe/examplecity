﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemButton : MonoBehaviour {
       
    // Use this for initialization  
    [SerializeField]
    int slotOrder;
    [SerializeField]
    Definition.ItemType itemType;
    [SerializeField]
    IngameUIManager inGameUIScript;

    void Start()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(SetSlotOrder);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int SLOTORDER
    {
        get
        {
            return slotOrder;
        }
        set
        {
            slotOrder = value;
        }
    }

    public void SetSlotOrder()
    {
        inGameUIScript.StoreItemUseClickInformation(slotOrder,itemType);
    }

    public void SetItemType(Definition.ItemType _type)
    {
        itemType = _type;
    }

    public void SetInGameScript(IngameUIManager _script)
    {
        inGameUIScript = _script;
    }
}
