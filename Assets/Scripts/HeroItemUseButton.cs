﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroItemUseButton : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    int heroOrder;
    [SerializeField]
    IngameUIManager inGameUIScript;    
	void Start () {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(SetChracterOrderToUseItem);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public int HEROORDER
    {
        get
        {
            return heroOrder;
        }
        set
        {
            heroOrder = value;
        }
    }

    public void SetIngameUIScript(IngameUIManager _script)
    {
        inGameUIScript = _script;
    }

    public void SetChracterOrderToUseItem()
    {
        inGameUIScript.OpenYesOrNoUse(heroOrder);
    }
}
