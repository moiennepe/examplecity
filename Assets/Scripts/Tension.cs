﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tension
{
    // Use this for initialization
    string tensionName;
    string tensionDescription;
  
    int totalAp;
    int totalTension;
    bool targetAllySkill;
    bool isBuffedSkill;
    bool isHyperTensionSkill;
    bool isTeamComboSkill;

    public void AddTensionName(string _tensionName)
    {
        tensionName = _tensionName;
    }

    public void AddTensionDescription(string _tensionDescription)
    {
        tensionDescription = _tensionDescription;
    }  

    public string GetTensionName()
    {
        return tensionName;
    }

    public string GetTensionDescription()
    {
        return tensionDescription;
    }  

    public int TOTALAP
    {
        get
        {
            return totalAp;
        }
        set
        {
            totalAp = value;
        }
    }

    public int TOTALTENSION
    {
        get
        {
            return totalTension;
        }
        set
        {
            totalTension = value;
        }
    }

    public bool TARGETALLY
    {
        get
        {
            return targetAllySkill;
        }
        set
        {
            targetAllySkill = value;
        }
    }

    public bool BUFFEDSKILL
    {
        get
        {
            return isBuffedSkill;
        }
        set
        {
            isBuffedSkill = value;
        }
    }

    public bool TEAMCOMBO
    {
        get
        {
            return isTeamComboSkill;
        }
        set
        {
            isTeamComboSkill = value;
        }
    }

    public bool HYPERTENSION
    {
        get
        {
            return isHyperTensionSkill;
        }
        set
        {
            isHyperTensionSkill = value;
        }
    }
}
