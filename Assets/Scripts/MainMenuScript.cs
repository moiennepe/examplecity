﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    string gameScene;
    [SerializeField]
    Button continueButton, newGameButton, creditsButton, optionsButton,exitButton, optionReturnButton ,exitCreditButton,nextPageCreditButton; // ui button
    [SerializeField]
    GameObject mainMenuPanel, optionPanel, creditsPanel, continuePanel; // ui panel
    [SerializeField] 
    InputField masterVolumeInput,soundVolumeInput,musicVolumeInput; // option input
    [SerializeField]
    float masterVolume, soundVolume, musicVolume; // storing input as int   
    [SerializeField]
    GameObject[] creditPages;
    [SerializeField]
    MusicManager musicManager;
    int page = 0;
    void Start () {
        ReadOptionFile();
        MenuInitFunction();
        gameScene = "warehouse";
    }
	
	// Update is called once per frame
	void Update () {

        if(masterVolumeInput.text == "")
        {
            masterVolumeInput.text = masterVolume.ToString();
           
        }
        else
        masterVolume = float.Parse(masterVolumeInput.text);

        if (musicVolumeInput.text == "")
        {
            musicVolumeInput.text = musicVolume.ToString();

        }
        else
        musicVolume = float.Parse(musicVolumeInput.text);

        if (soundVolumeInput.text == "")
        {
            soundVolumeInput.text = soundVolume.ToString();

        }
        else
        soundVolume = float.Parse(soundVolumeInput.text);

        MusicManager.masterVolume = masterVolume;
        MusicManager.musicVolume = musicVolume;
        MusicManager.soundEffectVolume = soundVolume;
    }

    void NewGameButtonFunction()
    {      
        SceneManager.LoadScene(gameScene);       
    }

    void OptionButtonFunction()
    {
        mainMenuPanel.SetActive(false);
        optionPanel.SetActive(true);
    }

    void ExitButtonFunction()
    {
        SaveOptionToNoteFile();
        Application.Quit();        
    }
    
    void CreditsButtonFunction()
    {
        mainMenuPanel.SetActive(false);
        creditsPanel.SetActive(true);
        musicManager.PlayMusic(1);
    }

    void ContinueButtonFunction()
    {
        mainMenuPanel.SetActive(false);
        continuePanel.SetActive(true);
    }

    void ExitCreditButton()
    {
        mainMenuPanel.SetActive(true);
        creditsPanel.SetActive(false);
        page = 0;
        musicManager.PlayMusic(0);
    }

    void NextPageCreditButton()
    {
        if(page + 1 < creditPages.Length)
        {
            creditPages[page].SetActive(false);
            page++;
            creditPages[page].SetActive(true);
        }
    }

    void MenuInitFunction()
    {
        newGameButton.onClick.AddListener(NewGameButtonFunction);
        optionsButton.onClick.AddListener(OptionButtonFunction);
        exitButton.onClick.AddListener(ExitButtonFunction);
        creditsButton.onClick.AddListener(CreditsButtonFunction);
        optionReturnButton.onClick.AddListener(OptionReturnButtonFunction);
        exitCreditButton.onClick.AddListener(ExitCreditButton);
        nextPageCreditButton.onClick.AddListener(NextPageCreditButton);        

        masterVolumeInput.text = masterVolume.ToString();
        musicVolumeInput.text = musicVolume.ToString();
        soundVolumeInput.text = soundVolume.ToString();

        musicManager = FindObjectOfType<MusicManager>();
    }

    void OptionReturnButtonFunction()
    {
        mainMenuPanel.SetActive(true);
        optionPanel.SetActive(false);
        SaveOptionToNoteFile();
    }

    void SaveOptionToNoteFile()
    {
        StreamWriter writer = new StreamWriter("Option.txt",false);
        writer.WriteLine(masterVolume);
        writer.WriteLine(musicVolume);
        writer.WriteLine(soundVolume);
        writer.Close();
    }

    void ReadOptionFile()
    {             
        StreamReader reader = new StreamReader("Option.txt",true);
        masterVolume = float.Parse(reader.ReadLine());
        musicVolume = float.Parse(reader.ReadLine());
        soundVolume = float.Parse(reader.ReadLine());

        masterVolumeInput.GetComponent<InputField>().text = masterVolume.ToString();
        musicVolumeInput.GetComponent<InputField>().text = musicVolume.ToString();
        soundVolumeInput.GetComponent<InputField>().text = soundVolume.ToString();

        reader.Close();
    }
}
