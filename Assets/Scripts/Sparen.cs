﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Sparen : Character
{
	public Sparen(List<EntityStats> stats) : base(stats)
	{

	}

	public override void Start()
    {
        playerStats.EXP = 0;
        playerStats.JP = 0;
        playerStats.Level = 1;
        playerStats.JPLevel = 1;
        apStats = 4;
        tension = 0;
        maxAPStats = apStats;
        maxTension = 200;
        playerStats.MaxJpExp = 500;
        playerStats.MaxExp = 1200;
        hyperTension = 0;
        bleedStatusTimer = 0;
        aggro = 20;
        originAggro = aggro;

        playerStats.HP = 80;

        characterNameType = Definition.CharacterName.SPARREN;
        characterRole = Definition.CharacterRoles.SUPPORTER;
        characterName = "Sparren";
        characterSkillList = new List<CharacterSkill>();
        tensionList = new List<Tension>();

        damage = playerStats.STR;
        originDamage = damage;
        //skill init
        SparenSkillInit();
        isUnlockedHyperTension = true;
        //tension init
        SparenTensionInit();
    }   
       

    public override void LevelUpStats()
    {
		base.LevelUpStats();
    }
	public override void LevelUpJob()
	{
		base.LevelUpJob();
		if (playerStats.Level == 3)
		{
			Tension skillTest2 = new Tension();
			skillTest2.AddTensionDescription("Revives a Ally at 20% HP");
			skillTest2.AddTensionName("NR-Reviver");
			skillTest2.TARGETALLY = true;
			skillTest2.TOTALAP = 0;
			skillTest2.TOTALTENSION = 80;
			tensionList.Add(skillTest2);
		}
	}

	CharacterSkill.Ability RapidFire = (Character c) =>
	{
		c.Tension += 3;
		c.HyperTension += 1.5f;
		return (int)(c.Damage * 3);
	};
	CharacterSkill.Ability SafetyCircle = (Character c) =>
	{
		c.Tension += 4;
		c.HyperTension += 2;
		return (int)(c.Damage * 4);
	};
	CharacterSkill.Ability JuanWhick = (Character c) =>
	{
		c.Tension += 7;
		c.HyperTension += 3.5f;
		return (int)(c.Damage * 4.5f);
	};

	void SparenSkillInit()
    {
        CharacterSkill rapidFire = new CharacterSkill("Rapid Fire", RapidFire);
        rapidFire.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        rapidFire.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        rapidFire.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        characterSkillList.Add(rapidFire);

        CharacterSkill safetyCircle = new CharacterSkill("Safety Circle", SafetyCircle);
        safetyCircle.AddSkillCombo(Definition.NormalAttackType.MEDIUMATTACK);
        safetyCircle.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        characterSkillList.Add(safetyCircle);

        CharacterSkill juanWhick = new CharacterSkill("Juan Whick", JuanWhick);
        juanWhick.AddSkillCombo(Definition.NormalAttackType.HEAVYATTACK);
        juanWhick.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        characterSkillList.Add(juanWhick);
    }

    void SparenTensionInit()
    { 
        // init hyper tension and team combo here
        Tension hyperTension = new Tension();
        hyperTension.AddTensionDescription("Fully Heal all allies, cure any status effects, give all allies Regen Buff, and revive any KO'd allies.");
        hyperTension.AddTensionName("Salvation");
        hyperTension.TARGETALLY = false;
        hyperTension.TOTALAP = 0;
        hyperTension.HYPERTENSION = true;
        hyperTension.TOTALTENSION = 0;
        tensionList.Add(hyperTension);

        Tension teamcombo = new Tension();
        teamcombo.AddTensionDescription("Characters Needed: Sparen & Slayer");
        teamcombo.AddTensionName("Patcher");
        teamcombo.TARGETALLY = false;
        teamcombo.TOTALAP = 0;
        teamcombo.TOTALTENSION = 0;
        teamcombo.TEAMCOMBO = true;
        tensionList.Add(teamcombo);


        Tension skillTest = new Tension();
        skillTest.AddTensionDescription("Heals HP over time");
        skillTest.AddTensionName("Regeneration");
        skillTest.TARGETALLY = true;
        skillTest.TOTALAP = 0;
        skillTest.TOTALTENSION = 30;
        tensionList.Add(skillTest);

        Tension skillTest1 = new Tension();
        skillTest1.AddTensionDescription("Cures all status effects");
        skillTest1.AddTensionName("Panacea");
        skillTest1.TARGETALLY = true;
        skillTest1.TOTALAP = 0;
        skillTest1.TOTALTENSION = 40;         
        tensionList.Add(skillTest1);
    }

    public override void UsingTensionSkill(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
    {
        if (skillOrder == 2)
        {
            // do regenation
            targetAlly.SetRegenStatus(true);
            PopUpTextController.CreatePopUpTextColorNewVersion("Regenaration", combatMangerScript.GetCharacterList()[allyOrderInCombat].transform, Color.green);
            PopUpTextController.CreatePopUpTextColorNewVersion("Regenaration", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
            tension -= tensionList[skillOrder].TOTALTENSION;
            AddStatusEffect(new StatusEffect(OnStartIncreaseAggroToMedium, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 3)
        {
            // do panacea
            targetAlly.ResetBadStatus();
            PopUpTextController.CreatePopUpTextColorNewVersion("Remove Debuff", combatMangerScript.GetCharacterList()[allyOrderInCombat].transform, Color.green);
            PopUpTextController.CreatePopUpTextColorNewVersion("Panacea", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
            tension -= tensionList[skillOrder].TOTALTENSION;
            AddStatusEffect(new StatusEffect(OnStartIncreaseAggroToMedium, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 2)
        {
            // do Revive
            if(targetAlly.HP <= 0)
            {
                targetAlly.HP = (int)(targetAlly.MaxHP * 0.2f);
                PopUpTextController.CreatePopUpTextColorNewVersion("Revive", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
                tension -= tensionList[skillOrder].TOTALTENSION;
                AddStatusEffect(new StatusEffect(OnStartIncreaseAggroToHigh, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
            }
            else
            {
                // do nothing maybe spam a feed back
                combatMangerScript.CreatePopUpNotificationInCombatScene("Ally is still alive");
            }
        }

        else if (skillOrder == 0) // hyper tension
        {
            DoHyperTension(skillOrder, enemyList, targetEnemy, characterList, targetAlly, allyOrderInCombat);
            AddStatusEffect(new StatusEffect(OnStartIncreaseAggroToExtreme, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 1)
        {

        }
        

    }

    public override void DoHyperTension(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
    {
        for(int i = 0; i < characterList.Count;i++)
        {
            characterList[i].HP = characterList[i].MaxHP;
            PopUpTextController.CreatePopUpTextColorNewVersion(characterList[i].MaxHP.ToString(), combatMangerScript.GetCharacterList()[i].transform, Color.green);
            characterList[i].ResetBadStatus();
            characterList[i].SetRegenStatus(true);
        }
        hyperTension = 0;
    }
}
