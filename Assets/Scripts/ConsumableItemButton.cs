﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableItemButton : MonoBehaviour {

    // Use this for initialization  
    [SerializeField]
    int slotOrder;
    [SerializeField]
    Definition.ItemType itemType;

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int SLOTORDER
    {
        get
        {
            return slotOrder;
        }
        set
        {
            slotOrder = value;
        }
    }

    public void SetSlotOrder(int _order)
    {
        _order = slotOrder;
    }
}
