﻿using System;

public class StatusEffect
{
    protected float timer;
    protected float initialDuration;
    public Character Target;

    private readonly Action _OnStart;
    private readonly Action _OnUpdate;
    private readonly Action _OnEnd;

    public float Timer
    {
        get
        {
            return timer;
        }
        set
        {
            timer = value;
        }
    }

    public float InitialDuration
    {
        get
        {
            return initialDuration;
        }
        set
        {
            initialDuration = value;
        }
    }

    public StatusEffect(Action onStart, Action onUpdate, Action onEnd, float timer)
    {
        _OnStart = onStart;
        _OnUpdate = onUpdate;
        _OnEnd = onEnd;
        Timer = timer;
    }

    public virtual void OnStart()
    {
        _OnStart();
    }

    public virtual void OnUpdate()
    {
        _OnUpdate();        
    }

    public virtual void OnEnd()
    {
        _OnEnd();        
    }

}