﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipInventory : MonoBehaviour
{
    [SerializeField]
    protected EquipmentSlot[] eSlots = new EquipmentSlot[8];    
    private EntityStats inventoryStats;

    public void Equip(InventoryItem value, BodyPart part)
    {
        for(int x = 0;x < eSlots.Length;x++)
        {
            if(part == eSlots[x].bPart)
            {               
                eSlots[x].equipment = value;
                UpdateStats();
                return;
            }
        }
    }
    public void UnEquip(InventoryItem value, BodyPart part)
    {
        for (int x = 0; x < eSlots.Length; x++)
        {
            if (part == eSlots[x].bPart)
            {
                eSlots[x].equipment = null;
                UpdateStats();
                return;
            }
        }
    }
    private void UpdateStats() //calculates overall equipment stats total
    {
        //purge existing stats
        inventoryStats.Clear();

        for(int x =0; x < eSlots.Length;x++)
        {
            inventoryStats.STR += eSlots[x].equipment.item.ItemStats.STR;
            inventoryStats.SPD += eSlots[x].equipment.item.ItemStats.SPD;
            inventoryStats.DEX += eSlots[x].equipment.item.ItemStats.DEX;
            inventoryStats.DEF += eSlots[x].equipment.item.ItemStats.DEF;
            inventoryStats.CON += eSlots[x].equipment.item.ItemStats.CON;
            inventoryStats.INT += eSlots[x].equipment.item.ItemStats.INT;
            inventoryStats.WIS += eSlots[x].equipment.item.ItemStats.WIS;
            inventoryStats.LUK += eSlots[x].equipment.item.ItemStats.LUK;
            inventoryStats.HP += eSlots[x].equipment.item.ItemStats.HP;
            inventoryStats.ATK += eSlots[x].equipment.item.ItemStats.ATK;
        }
    }
    public EntityStats GetEquipmentStats()
    {
        UpdateStats(); //just in case it wasn't updated
        return inventoryStats;
    }
}
