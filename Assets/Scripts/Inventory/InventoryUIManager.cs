﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InventoryUIManager : MonoBehaviour
{
    [SerializeField]
    private List<InventorySlot> slots;
    [SerializeField]
    private InventorySystem inventory;

    public static Action CompactInventory = delegate { };

    private void OnEnable()
    {
        InventorySlot.RefreshUI += RefreshUI;
        InventorySystem.CompactComplete += LoadInventoryUI;
        LoadInventoryUI();
    }
    private void LoadInventoryUI()
    {
        List<InventoryItem> items = inventory.GetItems;

        for (int x = 0; x < slots.Count; x++)
        {
            if (x < items.Count)
                slots[x].UpdateState(items[x].item.ItemSprite, inventory, x);
            else
                slots[x].ClearSlot();
        }
    }
    private void RefreshUI()
    {
        CompactInventory(); //triggers CompactComplete on completion
    }
}
