﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InventorySystem : MonoBehaviour
{
    [SerializeField]
    protected List <InventoryItem> items = new List<InventoryItem>();
    [SerializeField]
    protected Character player;
    [SerializeField]
    protected EquipInventory playerEquipment;

    public List<InventoryItem> GetItems { get { return items; } }
    public static Action<int> UpdateInventory = delegate { };
    public static Action CompactComplete = delegate { };

    public void OnEnable()
    {
        InventoryUIManager.CompactInventory += Compact;
    }
    public void AddItem(GenericItem item, int amount)
    {
        InventoryItem cacheItem = new InventoryItem(item,amount);
        items.Add(cacheItem);
    }
    public void RemoveItem(GenericItem item)
    {
        InventoryItem cacheItem = new InventoryItem(item,0);
        items.Remove(cacheItem);
    }
    public void EquipItem()
    {

    }
    public void UnEquipItem()
    {

    }
    public void UseItem(int index)
    {
        if (items[index].item.IsConsumable)
        {
            player.ApplyConsumable(items[index].item);
            items[index].Quantity--;
        }
        else
        {
            //condition needs to be added to verify it can be equipped
            items[index].Quantity--;
        }        
        UpdateInventory(index);
    }
    public InventoryItem GetItem(int index)
    {
        return items[index];
    }
    public void Compact()
    {
        for(int x=0; x < items.Count;x++)
        {
            if (items[x].Quantity == 0)
                items.Remove(items[x]);
        }
        CompactComplete();
    }
}
[System.Serializable]
public class InventoryItem
{
    public GenericItem item;
    [SerializeField]
    private int quantity;
    
    public InventoryItem(GenericItem value, int count)
    {
        item = value;
        Quantity = count;
    }

    public int Quantity
    {
        get
        {
            return quantity;
        }

        set
        {
            quantity = Mathf.Clamp(value,0,99);
        }
    }
}
