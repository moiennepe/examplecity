﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =("TrickerTrigger/Item"))]
public class GenericItem : ScriptableObject
{
    [SerializeField]
    protected string itemName;
    [SerializeField]
    protected ItemType itemType;
    [SerializeField]
    protected int buyPrice, sellPrice,craftCost,hashID;
    [SerializeField]
    protected EntityStats itemStats;
    [SerializeField]
    protected EntityJob[] equibleClasses;
    [SerializeField]
    protected GameObject itemPrefab;
    [SerializeField]
    protected Sprite itemSprite;
    [SerializeField]
    protected string itemDescription;
    [SerializeField]
    protected bool bConsumable; //if set to false its a weapon/armor/accessory

    public int HashID { get { return hashID; } }
    public int BuyPrice { get { return buyPrice; } }
    public int SellPirce { get { return sellPrice; } }
    public int CraftCost { get { return craftCost; } }
    public Sprite ItemSprite { get { return itemSprite; } }
    public string ItemDescription { get { return itemDescription; } }
    public EntityStats ItemStats { get { return itemStats; } }
    public bool IsConsumable { get { return bConsumable; } }

    private void OnEnable()
    {
        hashID = Animator.StringToHash(itemName);
    }
    public bool CanEquip(EntityJob eClass)
    {
        if (equibleClasses.Length == 0) return true; //equipable to all classes

        for(int x=0;x<equibleClasses.Length;x++)
        {
            if (eClass == equibleClasses[x])
                return true;
        }
        return false;
    }   
}
