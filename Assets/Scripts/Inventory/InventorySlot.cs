﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class InventorySlot : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textMesh;
    [SerializeField]
    private Image itemIcon;
    [SerializeField]
    private int inventoryIndex;   
    [SerializeField]
    private InventorySystem inventory;
    [SerializeField]
    private Sprite defaultSprite;
    public bool bEmpty = true;

    public static Action RefreshUI = delegate { };

    private void OnEnable()
    {
        bEmpty = true;
        textMesh.text = "";
        defaultSprite = itemIcon.sprite;
    }
    public void UpdateState(Sprite icon, InventorySystem curInventory,int index)
    {        
        inventoryIndex = index;
        inventory = curInventory;
       
        if(textMesh != null)
            textMesh.text = inventory.GetItem(inventoryIndex).Quantity.ToString();
        itemIcon.sprite = icon;
        bEmpty = false;
    }
    public void SetIndex(int value)
    {
        inventoryIndex = value;
    }
    public int GetIndex()
    {
        return inventoryIndex;
    }
    public InventorySlot(Sprite iconValue, int indexValue)
    {
        itemIcon.sprite = iconValue;
        inventoryIndex = indexValue;
    }
    public int GetAmount()
    {
        return inventory.GetItem(inventoryIndex).Quantity;
    }
    public void UseItem()
    {
        if (bEmpty) return;
        inventory.UseItem(inventoryIndex);       
        textMesh.text = inventory.GetItem(inventoryIndex).Quantity.ToString();
        if (inventory.GetItem(inventoryIndex).Quantity == 0)
            RefreshUI();
    }
    public void ClearSlot()
    {
        textMesh.text = "";
        itemIcon.sprite = defaultSprite;       
        bEmpty = true;
    }
}
