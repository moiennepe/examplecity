﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Item {
	Definition.CharacterName armorForHero;

	public int DefenseStat { get; set; }

	public override void Init(int _amount, string _itemName)
    {
        base.Init(_amount, _itemName);
        Type = Definition.ItemType.ARMOR;
    }

    public void SetArmorForHero(Definition.CharacterName _armorForHero)
    {
        armorForHero = _armorForHero;
    }  
} 
