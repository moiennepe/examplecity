﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Slayer : Character
{

	public Slayer(List<EntityStats> stats) : base(stats)
	{

	}

	public override void Start()
    {
        playerStats.EXP = 0;
        playerStats.JP = 0;
        playerStats.Level = 1;
        playerStats.JPLevel = 1;
        apStats = 4;
        tension = 0;
        maxAPStats = apStats;
        maxTension = 200;
        playerStats.MaxJpExp = 500;
        playerStats.MaxExp = 1200;
        hyperTension = 0;
        bleedStatusTimer = 0;
        aggro = 45;
        originAggro = aggro;

        playerStats.HP = 120;

        characterNameType = Definition.CharacterName.SLAYER;
        characterName = "Slayer";
        characterRole = Definition.CharacterRoles.GUARDIAN;

        characterSkillList = new List<CharacterSkill>();
        tensionList = new List<Tension>();

        // skill init
        SlayerSkillInit();

        // slayer tension init
        SlayerTensionInit();

        damage = playerStats.STR;
        originDamage = damage;
        isUnlockedHyperTension = true;
        hyperTension = 200;
    }  

    public override void LevelUpStats()
    {
		base.LevelUpStats();
		if (playerStats.Level == 2)
		{
			Tension skillTest1 = new Tension();
			skillTest1.AddTensionDescription("Deal physical damage to all enemies on screen");
			skillTest1.AddTensionName("Sweep");
			skillTest1.TARGETALLY = false;
			skillTest1.TOTALAP = 0;
			skillTest1.TOTALTENSION = 80;
			tensionList.Add(skillTest1);
		}
	}

	public override void LevelUpJob()
	{
		base.LevelUpJob();
	}

	CharacterSkill.Ability UpDowner = (Character c) =>
	{
		c.Tension += 4;
		c.HyperTension += 2;
		return (int)(c.Damage * 4);
	};
	CharacterSkill.Ability PumbleStroke = (Character c) =>
	{
		c.Tension += 4;
		c.HyperTension += 2;
		return (int)(c.Damage * 4);
	};
	CharacterSkill.Ability Crusher = (Character c) =>
	{
		c.Tension += 3;
		c.HyperTension += 1.5f;
		return (int)(c.Damage * 3);
	};

	void SlayerSkillInit()
    {
		CharacterSkill upDowner = new CharacterSkill("Up Downer", UpDowner);
		upDowner.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
		upDowner.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
		upDowner.AddSkillCombo(Definition.NormalAttackType.MEDIUMATTACK);
        characterSkillList.Add(upDowner);

        CharacterSkill pumbleStroke = new CharacterSkill("Pumble Stroke", PumbleStroke);
        pumbleStroke.AddSkillCombo(Definition.NormalAttackType.MEDIUMATTACK);
        pumbleStroke.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        characterSkillList.Add(pumbleStroke);

        CharacterSkill crusher = new CharacterSkill("Crusher", Crusher);
        crusher.AddSkillCombo(Definition.NormalAttackType.LIGHTATTACK);
        crusher.AddSkillCombo(Definition.NormalAttackType.HEAVYATTACK);
        characterSkillList.Add(crusher);
    }

    void SlayerTensionInit()
    {

        // init hyper tension and team combo here
        Tension hyperTension = new Tension();
        hyperTension.AddTensionDescription("Reduce all damage to allies by 50% for 120 seconds, any physical attacks made on a character will damage the attacking enemy.");
        hyperTension.AddTensionName("Mighty Wire");
        hyperTension.TARGETALLY = false;
        hyperTension.TOTALAP = 0;
        hyperTension.HYPERTENSION = true;
        hyperTension.TOTALTENSION = 0;
        tensionList.Add(hyperTension);

        Tension teamcombo = new Tension();
        teamcombo.AddTensionDescription("Characters Needed: Hero & Slayer");
        teamcombo.AddTensionName("Critical X");
        teamcombo.TARGETALLY = false;
        teamcombo.TOTALAP = 0;
        teamcombo.TOTALTENSION = 0;
        teamcombo.TEAMCOMBO = true;
        tensionList.Add(teamcombo);

        Tension skillTest = new Tension();
        skillTest.AddTensionDescription("Protect an ally from incoming damage from an enemy while dealing a counterattack and gaining 40 Tension.");
        skillTest.AddTensionName("Guard");
        skillTest.TARGETALLY = true;
        skillTest.TOTALAP = 0;
        skillTest.TOTALTENSION = 0;
        tensionList.Add(skillTest);     
    }

    public override void UsingTensionSkill(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
    {
        // skill order = 1000 for hyper tension , 2000 for team combo
        if (skillOrder == 2)
        {
            // do guard
            if(isGuardingSomeone)
            {
                // feed back alreayd guarded
                combatMangerScript.CreatePopUpNotificationInCombatScene("Character is guarding someone");
            }
            else
            {
                if(targetAlly != this)
                {
                    isGuardingSomeone = true;
                    targetAlly.SetGuardingStatus(true, this);
                    PopUpTextController.CreatePopUpTextColorNewVersion("Guard", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
                    PopUpTextController.CreatePopUpTextColorNewVersion("Guarded", combatMangerScript.GetCharacterList()[allyOrderInCombat].transform, Color.green);
                }             
                else
                {
                    //feed back cant guardhim self
                    combatMangerScript.CreatePopUpNotificationInCombatScene("Cannot guard himself");
                }
            }
        }

        else if (skillOrder == 3)
        {
            // do sweep
            for (int i = 0; i < enemyList.Count; i++)
            {
                enemyList[i].HP -= damage;
                PopUpTextController.CreatePopUpTextColorNewVersion(damage.ToString(), enemyList[i].transform, Color.red);
            }
            tension -= tensionList[skillOrder].TOTALTENSION;
        }
        else if(skillOrder == 0) // hyper tension
        {
            DoHyperTension(skillOrder, enemyList,targetEnemy,characterList,targetAlly, allyOrderInCombat);
            AddStatusEffect(new StatusEffect(OnStartIncreaseAggroToExtreme, OnUpdateSetAggro, OnEndSetAggro, 60.0f));
        }

        else if (skillOrder == 1)
        {

        }
    }

    public override void DoHyperTension(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
    {
        for(int i = 0; i < characterList.Count;i++)
        {
            characterList[i].SetSuperGuardStatus(true);
            PopUpTextController.CreatePopUpTextColorNewVersion("Super Guard", combatMangerScript.GetCharacterList()[i].transform, Color.green);            
        }
        hyperTension = 0;      
    }
}


