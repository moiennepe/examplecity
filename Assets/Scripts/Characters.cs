﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    [SerializeField]
    protected string characterName;
    [SerializeField]
    protected EquipInventory equipInventory;
	protected int numberOrderInCombat;
   
	//protected int characterExp, characterJp, characterLevel, characterJobLevel, characterMaxJpExp, characterMaxNormalExp;

    protected EntityStats playerStats;

	private List<EntityStats> statProgression;

	protected int hpStats, apStats, tension, maxhpStats, maxAPStats, damage, defense, originDamage, originDefense, maxTension, aggro, originAggro;

	protected float hyperTension;

	protected Definition.CharacterRoles characterRole;

	protected Definition.AlignmentType alignmentType;

	protected Definition.CharacterName characterNameType;

	protected List<CharacterSkill> characterSkillList;

	protected List<Tension> tensionList;

    public List<StatusEffect> statusEffectList = new List<StatusEffect>();

	protected bool isBleeding;

	protected bool isRegeneration, isAttackUp, isBeingGuarded, isGuardingSomeone, isUnlockedHyperTension, IsUnlockedTeamCombo, isSuperGuarded;

	protected float timer, AttackUpTimer, regenerationTimer, regenNormalTimer, superGuardTimer;

	protected float bleedStatusTimer, normalTimer;

	protected int bleedStackDamage, regenStackCount, attackStackCount;

	protected Weapon currentEquipWeapon;

	protected Armor currentEquipArmor;

	protected Accessories currentEquipAccessory;

	protected Character bodyGuard;

	protected CombatManagerScript combatMangerScript;

    protected EntityStats totalStats; //possible name change

    public EntityStats PlayerStats { get { return playerStats; } set { playerStats = value; } }
    public int Exp	{get{return playerStats.EXP;}set{ playerStats.EXP = value;}}
	public int JP{get{return playerStats.JP;}set{ playerStats.JP = value;}}    
	public int HP{get{return playerStats.HP;}set{playerStats.HP = value;}}
	public int MaxHP{get{return maxhpStats;}set{maxhpStats = value;}}    
	public int AP{get{return apStats;}set{apStats = value;}}
	public int MaxAP{get{return maxAPStats;}set{maxAPStats = value;}}
	public int Tension{get{return tension;}set{tension = value;}}
	public int MaxTension{get{return maxTension;}set{maxTension = value;}}
	public int Damage{get{return damage;}set{damage = value;}}
	public int OriginDamage{get{return originDamage;}set{originDamage = value;}}
	public int OriginDefense{get{return originDefense;}set{originDefense = value;}}
	public int Defense{get{return defense;}set{defense = value;}}
    public int Aggro{get{return aggro;}set{aggro = value;}}
    public int OriginAggro{get{return originAggro;}set{originAggro = value;}}
	public float HyperTension{get{return hyperTension;}set{hyperTension = value;}}
	public bool UnlockedHyperTension{get{return isUnlockedHyperTension;}set{isUnlockedHyperTension = value;}}
	public bool UnlockedTeamCombo{get{ return IsUnlockedTeamCombo; }set{IsUnlockedTeamCombo = value;}}  
	public bool GetBleedingStatus{get{return isBleeding;}}
	public string GetCharacterName { get { return characterName; } }
	public Definition.CharacterRoles GetCharacterRole { get { return characterRole; } }
    public Definition.CharacterName GetCharacterNameType { get { return characterNameType; } }
	public int GetCharacterLevel { get { return playerStats.Level; } }
	public int GetCharacerJob{get{ return playerStats.JPLevel; } }
    public EntityStats GetPlayerStats { get { CalculateStats(); return totalStats; } }
    public List<CharacterSkill> GetCharacterSkillList { get { return characterSkillList; } }
    public List<Tension> GetTensionList { get { return tensionList; } }
    public bool GetGuardedStatus { get { return isBeingGuarded; } }
    public bool GetSuperGuardedStatus { get { return isSuperGuarded; } }
    public Character GetBodyGuard { get { return bodyGuard; } }
    public int GetNumberInOrderCombat { get { return numberOrderInCombat; } }

    private void CalculateStats() //this function can include debuffs 
    {
        totalStats = playerStats /*CombineStats(playerStats, equipInventory.GetEquipmentStats())*/;
    }
	public Character(List<EntityStats> stats)
	{
		statProgression = stats;

		hpStats = statProgression[0].HP;
		playerStats.Add(statProgression[0]);

		maxhpStats = hpStats;
	}

	// Gain experience and level up if needed
	public virtual void PlayerExpProgression(int exp)
	{
		int totalExp = exp + playerStats.EXP;
		while (true)
		{
			if (totalExp >= playerStats.MaxExp)
			{
				totalExp -= playerStats.MaxExp;
                playerStats.EXP = 0;
                playerStats.MaxExp *= 2;
				//Trigger level up event
				LevelUpStats();
			}
			else
			{
                playerStats.EXP += totalExp;
				break;
			}
		}
	}

	public virtual void PlayerJpProgression(int exp)
	{
		int totalExp = exp + playerStats.JP;

		while (true)
		{
			if (totalExp >= playerStats.MaxJpExp)
			{
                playerStats.JP = 0;
				totalExp -= playerStats.MaxJpExp;
                playerStats.MaxJpExp *= 3;
				LevelUpJob();
			}
			else
			{
                playerStats.JP += totalExp;
				break;
			}
		}
	}

	public virtual void Update()
	{
		UpdateBadStatus();
		UpdateGoodStatus();
        UpdateAP();
        UpdateStatusEffects();
    }

	public int DamageCalcuation(Enemy _enemytoAttack, int _totalDamage, Transform _enemyPostion)
	{
		// calculate damage later
		_enemytoAttack.HP -= _totalDamage;
		PopUpTextController.CreatePopUpTextColorNewVersion(_totalDamage.ToString(), _enemyPostion, Color.red);
		return _totalDamage;
	}

	// Increase our stats
	public virtual void LevelUpStats()
	{
		// Are we max level?
		if (statProgression.Count <= playerStats.Level)
			return;

		playerStats.LevelUp();

		maxhpStats += statProgression[playerStats.Level - 1].HP;
		playerStats.Add(statProgression[playerStats.Level - 1]);

		hpStats = maxhpStats;
	}

	// Level up job
	public virtual void LevelUpJob()
	{
		playerStats.JPLevel++;
	}
    public virtual void Start() { }
	public virtual void HyperTensionGainCalculation(int _tensionGained)
	{
		int temp = _tensionGained / 2;
		hyperTension += temp;
	}

	public virtual void CheckingCombo(List<Definition.NormalAttackType> combo_input, Transform enemyPosition, Enemy enemyToAttack, int totalAPUsed)
	{
		CharacterSkill correctCombo = null;
		foreach (CharacterSkill skill in characterSkillList)
		{
			List<Definition.NormalAttackType> combo = skill.GetComboInput();
			if (combo_input.Count < combo.Count)
				continue;

			bool isCorrect = true;
			for (int i = 0; i < combo.Count; i++)
			{
				if (combo_input[i] != combo[i])
				{
					isCorrect = false;
					break;
				}
			}
			if (isCorrect)
			{
				correctCombo = skill;
				break;
			}
		}

		DoComboInput(combo_input, correctCombo, enemyPosition, enemyToAttack);

		AP -= totalAPUsed;
		if (tension > maxTension)
		{
			tension = maxTension;
		}

		if (hyperTension > 200)
		{
			hyperTension = 200;
		}
	}

	public virtual void CheckingComboTest(List<Definition.NormalAttackType> combo_input, GameObject Enemy)
	{

	}

	public virtual void UsingTensionSkill(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
	{

	}

	public void SetBleedStatus(bool status)
	{
		bleedStatusTimer += 90.0f;
		isBleeding = status;

		if (!isBleeding)
		{
			bleedStatusTimer = 0;
			bleedStackDamage = 0;
		}

		// Gain one bleed, or triple existing bleed
		bleedStackDamage = bleedStackDamage == 0 ? 1 : bleedStackDamage * 3;

		if (bleedStackDamage >= 9)
			bleedStackDamage = 9;
	}

	public void SetRegenStatus(bool status)
	{
		isRegeneration = status;
		if (isRegeneration)
		{
			regenerationTimer = 60;
			regenNormalTimer = 0;
		}
		else
		{
			regenerationTimer = 0;
		}
	}

	public void SetSuperGuardStatus(bool status)
	{
		isSuperGuarded = status;
		if (isSuperGuarded)
		{
			superGuardTimer = 90;
		}
		else
		{
			superGuardTimer = 0;
		}
	}

	public void SetAttackStatus(bool status, int stacks = 1)
	{
		if (!status)
		{
			// Remove AttackUp
			AttackUpTimer = 0;
			isAttackUp = false;
			damage = originDamage;
			attackStackCount = 0;
		}
		else
		{
			// Enable or Stack AttackUp
			AttackUpTimer = 60;
			isAttackUp = true;
			attackStackCount += stacks;

			//Scale damage 1.5x, 2.0x, 2.5x
			damage = (int) (originDamage * (1.0f + Math.Min(attackStackCount, 3) * 0.5f));

			PopUpTextController.CreatePopUpTextColorNewVersion("Attack Up", combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
		}
	}

	public void SetGuardingStatus(bool status, Character _bodyGuard)
	{
		isBeingGuarded = status;
		bodyGuard = _bodyGuard;
	}

	public void SetGuardingSomeoneStatus(bool status)
	{
		isGuardingSomeone = status;
	}
	public void ResetBadStatus()
	{
		normalTimer = 0;
		bleedStatusTimer = 0;
		isBleeding = false;
	}

	public virtual void ResetGoodStatus()
	{
		isAttackUp = false;
		isRegeneration = false;
		isBeingGuarded = false;
		isGuardingSomeone = false;
		isSuperGuarded = false;
		superGuardTimer = 0;
		AttackUpTimer = 0;
		regenerationTimer = 0;
		regenNormalTimer = 0;
		damage = originDamage;
		defense = originDefense;
	}

	void UpdateBadStatus()
	{
		
		normalTimer += Time.deltaTime;

		// Bleed using normalTimer leads to inconsistent damage from bleed (0-45 seconds until first damage)
		if (isBleeding)
		{
			if (normalTimer >= 45)
			{
				// Lose 1% MaxHP per bleed stack
				int bleedDamage = (int)(playerStats.MaxHP * 0.01f * bleedStackDamage);
				playerStats.HP -= bleedDamage;
				if (combatMangerScript != null)
				{
					PopUpTextController.CreatePopUpTextColorNewVersion("-" + bleedDamage, combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.magenta);
				}
			}

			bleedStatusTimer -= Time.deltaTime;
			if (bleedStatusTimer <= 0)
			{
				isBleeding = false;
			}
		}

		if (normalTimer >= 45)
		{
			normalTimer = 0;
		}
	}

	public virtual void UpdateGoodStatus()
	{
		if (isAttackUp)
		{
			AttackUpTimer -= Time.deltaTime;
			if (AttackUpTimer <= 0)
			{
				isAttackUp = false;
				damage = originDamage;
				attackStackCount = 0;
			}
		}

		if (isSuperGuarded)
		{
			superGuardTimer -= Time.deltaTime;
			if (superGuardTimer <= 0)
			{
				superGuardTimer = 0;
				isSuperGuarded = false;
			}
		}

		if (isRegeneration)
		{
			regenerationTimer -= Time.deltaTime;
			regenNormalTimer += Time.deltaTime;
			if (regenNormalTimer >= 3)
			{
				if (hpStats < maxhpStats)
				{
					int hpGain = (int)(maxhpStats * 0.1f);
					hpStats += hpGain;
					PopUpTextController.CreatePopUpTextColorNewVersion(hpGain.ToString(), combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
					if (hpStats > maxhpStats)
					{
						hpStats = maxhpStats;
					}
				}
				regenNormalTimer = 0;
			}
			if (regenerationTimer <= 0)
			{
				isRegeneration = false;
			}
		}
	}

    public virtual void UpdateAP()
    {
        if (AP < MaxAP)
        {
            timer += Time.deltaTime * playerStats.SPD * 2 / 3;     
            if (timer>10)
            {
                AP++;
                timer = 0;   
            }
        }
    }
    public void ApplyConsumable(GenericItem item)
    {
        //add code for consumable behavior
    }
    //this will be replaced by the Equipment Inventory
    public void EquipItem(GenericItem item)
    {
        playerStats.Add(item.ItemStats);
    }
    public void UnEquipItem(GenericItem item)
    {
        playerStats.Remove(item.ItemStats);
    }
    public void EquipWeapon(Weapon weaponEquip, Player player)
	{
		if (weaponEquip.GetWeaponHeroType() != characterNameType)
			return;

		if (player)
		{
			List<Weapon> inv = player.GetPlayerInventory().GetWeaponInvent();
			if (currentEquipWeapon != null)
			{
				// Replace existing weapon
				inv.Add(currentEquipWeapon);
				inv.Remove(weaponEquip);
			}
			else
			{
				// New weapon
				inv.Remove(weaponEquip);
			}
		}

		currentEquipWeapon = weaponEquip;
        damage = playerStats.STR * currentEquipWeapon.GetWeaponDamage(); 
		originDamage = damage;
	}

	public void EquipArmor(Armor armorEquip, Player player)
	{
		defense += armorEquip.DefenseStat;
		List<Armor> inv = player.GetPlayerInventory().GetArmorInvent();

		if (currentEquipArmor != null)
		{
			defense -= currentEquipArmor.DefenseStat;
			inv.Add(currentEquipArmor);
			inv.Remove(armorEquip);
		}
		else
		{
			inv.Remove(armorEquip);
		}
		currentEquipArmor = armorEquip;
		originDefense = defense;
	}

	//void EquipAcessory(Weapon weaponEquip)
	//{
	//	damage += weaponEquip.GetWeaponDamage();
	//}

	public void UnequipWeapon(Player playerScript)
	{
		playerScript.GetPlayerInventory().GetWeaponInvent().Add(currentEquipWeapon);
		currentEquipWeapon = null;
        damage = playerStats.STR;  
		originDamage = damage;
	}

	public void UnequipArmor(Player playerScript)
	{
		defense -= currentEquipArmor.DefenseStat;
		playerScript.GetPlayerInventory().GetArmorInvent().Add(currentEquipArmor);
		currentEquipArmor = null;
		originDefense = defense;
	}

	public void SetCombatManager(CombatManagerScript _combatManger)
	{
		combatMangerScript = _combatManger;
	}

	public int DoComboInput(List<Definition.NormalAttackType> combo_input, CharacterSkill combo, Transform enemyPostion, Enemy enemyToAttack)
	{
		int totalDamage = 0;
		for (int i = 0; i < combo_input.Count; i++)
		{
			if (combo_input[i] == Definition.NormalAttackType.LIGHTATTACK)
			{
				// do normal light attack
				totalDamage += damage * UnityEngine.Random.Range(1, 4);
				tension += 1;
				hyperTension += 0.5f;
			}

			else if (combo_input[i] == Definition.NormalAttackType.MEDIUMATTACK)
			{
				//do normal medium attack
				totalDamage += damage * UnityEngine.Random.Range(4, 8);
				tension += 2;
				hyperTension += 1;
			}

			else if (combo_input[i] == Definition.NormalAttackType.HEAVYATTACK)
			{
				// do normal heavy attack
				totalDamage += damage * UnityEngine.Random.Range(8, 12);
				tension += 3;
				hyperTension += 1.5f;
			}
		}
		if (combo != null)
		{
			totalDamage += combo.ActivateSkill(this);
			PopUpTextController.CreatePopUpTextColorNewVersion(combo.GetSkillName(), combatMangerScript.GetCharacterList()[numberOrderInCombat].transform, Color.green);
		}

		return DamageCalcuation(enemyToAttack, totalDamage, enemyPostion);
	}

	public virtual void DoHyperTension(int skillOrder, List<Enemy> enemyList, Enemy targetEnemy, List<Character> characterList, Character targetAlly, int allyOrderInCombat)
	{

	}

	public virtual void TeamCombo()
	{

	}

	public void SetNumberInOrderCombat(int _number)
	{
		numberOrderInCombat = _number;
	}
    public void AddStatusEffect(StatusEffect statusEffect)
    {
        statusEffectList.Add(statusEffect);
        statusEffect.OnStart();
    }

    public void UpdateStatusEffects()
    {
        for (int i = 0; i<statusEffectList.Count; i++)
        {
            statusEffectList[i].OnUpdate();
            statusEffectList[i].Timer -= Time.deltaTime;
            if (statusEffectList[i].Timer<0)
            {
                statusEffectList[i].OnEnd();
                statusEffectList.RemoveAt(i);
            }
        }
    }
    public void OnStartIncreaseAggroToMedium()
    {
        if (aggro < 35) OnStartSetAggroToMedium();
    }

    public void OnStartIncreaseAggroToHigh()
    {
        if (aggro < 45) OnStartSetAggroToHigh();
    }

    public void OnStartIncreaseAggroToExtreme()
    {
        if (aggro < 55) OnStartSetAggroToExtreme();
    }

    public void OnStartSetAggroToLow()
    {
        Aggro = 20;
    }

    public void OnStartSetAggroToMedium()
    {
        Aggro = 35;
    }

    public void OnStartSetAggroToHigh()
    {
        Aggro = 45;
    }

    public void OnStartSetAggroToExtreme()
    {
        Aggro = 50;        
    }

    public void OnUpdateSetAggro()
    {
             
    }

    public void OnEndSetAggro()
    {
        aggro = originAggro;
        Debug.Log(aggro);
    }
}
