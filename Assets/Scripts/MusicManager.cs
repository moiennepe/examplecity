﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    // Use this for initialization
    enum AUDIOSOURCEORDER
    {
        MUSIC,
        SOUNDEFFECT
    }

    [SerializeField]
    AudioClip[] musicArray,soundEffectArray;
    [SerializeField]
    AudioSource [] audioCurrent; 

    public static float musicVolume,soundEffectVolume,masterVolume;

    void Start () {
        audioCurrent = GetComponents<AudioSource>();
        audioCurrent[0].clip = musicArray[0];
        audioCurrent[0].Play();
        audioCurrent[0].loop = true;      
       
	}
	
	// Update is called once per frame
	void Update () {
        audioCurrent[0].volume = (musicVolume / 100) * (masterVolume / 100);
        audioCurrent[1].volume = (soundEffectVolume / 100) * (masterVolume / 100);
    }

    public void UpdateSoundLevel()
    {   // todo update music and volume use text file
       
    }

    public void PlayMusic(int songOrder)
    {
        audioCurrent[0].clip = musicArray[songOrder];
        audioCurrent[0].Play();
    }

    public void PlaySoundEffect(int songOrder)
    {
        audioCurrent[1].clip = soundEffectArray[songOrder];
        audioCurrent[1].Play();
    }
}
