﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item {

    int weaponDamage;
    Definition.CharacterName weaponForHero;

    public override void Init(int _amount, string _itemName)
    {
        base.Init(_amount, _itemName);
        type = Definition.ItemType.WEAPON;
    }

    public int GetWeaponDamage()
    {
        return weaponDamage;
    }

    public void SetWeaponDamage(int stat)
    {
        weaponDamage = stat;
    }

    public void SetWeaponForHero(Definition.CharacterName _weaponForHero)
    {
        weaponForHero = _weaponForHero;
    }

    public Definition.CharacterName GetWeaponHeroType()
    {
        return weaponForHero;
    }
}
