﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2DScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 relativePos = Camera.main.transform.position - transform.position;
        Quaternion.LookRotation(relativePos,Vector3.up);
	}
}
