﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[CreateAssetMenu(menuName =("GameSaves/PC"))]
public class PCSavingSO : ScriptableObject
{
    public void Save(byte[] data,string name)
    {
        File.WriteAllBytes(Application.persistentDataPath + "/" + name + ".tt", data);
    }
    public byte[] Load(string name)
    {
        if (File.Exists(Application.persistentDataPath + "/" + name + ".tt"))
            return File.ReadAllBytes(Application.persistentDataPath + "/" + name + ".tt");
        else
            return null;
    }
    public bool SaveExist(string name)
    {
        if (File.Exists(Application.persistentDataPath + "/" + name + ".tt"))
            return true;
        else
            return false;
    }
}
