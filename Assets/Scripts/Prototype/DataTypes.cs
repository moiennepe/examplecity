﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EntityJob
{
    Fighter, Blader, Saga, Saboteur, Guardian, Medic, Respondent, Era, ManAtArms,Scoundrel, Ronin, Vigilanti,Polis,
    Noire, ParaMedic, Ward, Doctor, Outbreaker, Knight, Nyangineer, Maiden, Suavem, Velites, Odyssey, Vagrant,
    Operator,Crusader, Cascade, Calculator, Philisitine, Dame, Mania, Dancer, Warlord, Technonmanacer, Apothercar, Practitioner
}
[System.Serializable]
public struct EntityStats
{       
    public int STR, SPD, DEX, CON, INT, WIS, LUK, HP,EXP,JP, ATK, DEF, Level,JPLevel,MaxJpExp,MaxExp,MaxHP;

    public void LevelUp()
    {
        Level++;
    }
    public bool IsMax { get { if (Level == 99) return true; else return false;}}
    public void Add(EntityStats value)
    {
        STR += value.STR;
        SPD += value.SPD;
        DEX += value.DEX;
        CON += value.CON;
        INT += value.INT;
        WIS += value.WIS;
        LUK += value.LUK;
    }
    public void Remove(EntityStats value)
    {
        STR -= value.STR;
        SPD -= value.SPD;
        DEX -= value.DEX;
        CON -= value.CON;
        INT -= value.INT;
        WIS -= value.WIS;
        LUK -= value.LUK;
    }
    public EntityStats CombineStats(EntityStats a, EntityStats b)
    {
        EntityStats temp = new EntityStats();
        temp.STR = a.STR + b.STR;
        temp.SPD = a.SPD + b.SPD;
        temp.DEX = a.DEX + b.DEX;
        temp.DEF = a.DEF + b.DEF;
        temp.CON = a.CON + b.CON;
        temp.INT = a.INT + b.INT;
        temp.WIS = a.WIS + b.WIS;
        temp.LUK = a.LUK + b.LUK;
        temp.HP = a.HP + b.HP;
        temp.ATK = a.ATK + b.ATK;

        return temp;
    }
    public void Clear()
    {
        STR = 0;
        SPD = 0;
        DEX = 0;
        DEF = 0;
        CON = 0;
        INT = 0;
        WIS = 0;
        LUK = 0;
        HP = 0;
        ATK = 0;
    }
}
public enum ItemType
{
    Healing,Crafting,Weapon,Armor,Accessory
}
public enum BodyPart
{
    Head,Torso,Waist,Legs,Feet,Neck,Finger1,Finger2
}
public struct EquipmentSlot
{
    public InventoryItem equipment;
    public BodyPart bPart;
}