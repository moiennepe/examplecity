﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SaveData
{
    [SerializeField]
    private string saveName;
    
    public string SaveName { get { return saveName; } set { saveName = value; } }

    [SerializeField]
    private EntityAttributes[] eAttributes;
}
