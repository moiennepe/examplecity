﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveSystemSO : ScriptableObject
{
    [SerializeField]
    private PCSavingSO pcSave;
    [SerializeField]
    private SaveData sData;

    public void Load(string name)
    {
        byte[] data = new byte[1024 * 1024];      
        
        data = pcSave.Load(name);

        if (data != null)
        {
            string json = System.Text.Encoding.ASCII.GetString(data);
            sData = JsonUtility.FromJson<SaveData>(json);
            LoadData(sData);
        }
    }
    public void Save(string name)
    {

    }
    private void LoadData(SaveData value)
    {

    }
}
