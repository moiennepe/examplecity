﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =("TrickerTrigger/EntityAttributes"))]
public class EntityAttributes : ScriptableObject
{
    [SerializeField]
    private EntityJob eClass;
    [SerializeField]
    private string name;
    [SerializeField]
    private EntityStats eStats;    

    public int Level { get { return eStats.Level; } set { eStats.Level = value; } }
    public int HP { get { return eStats.HP; } set { eStats.HP = value; } }
    public int Strength { get { return eStats.STR; } set { eStats.STR = value; } }
    public int Speed { get { return eStats.SPD; } set { eStats.SPD = value; } }
    public int Dexterity { get { return eStats.DEX; } set { eStats.DEX = value; } }
    public int Constitution { get { return eStats.CON; } set { eStats.CON = value; } }
    public int Intelligence { get { return eStats.INT; } set { eStats.INT = value; } }
    public int Wisdom { get { return eStats.WIS; } set { eStats.WIS = value; } }
    public int Luck { get { return eStats.LUK; } set { eStats.LUK = value; } }   
    public string Name { get { return name; } set { name = value; } }
}
