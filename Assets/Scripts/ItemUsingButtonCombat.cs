﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUsingButtonCombat : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    int leftToUse;
    [SerializeField]
    int orderButton,playerInventoryOrder;   
    [SerializeField]
    CombatManagerScript combat_manager;
    [SerializeField]
    Definition.ConsumableItemType consumableItemType;
    UnityEngine.UI.Text[] textList;  

    void Start () {    
        textList = GetComponentsInChildren<UnityEngine.UI.Text>();
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(ButtonFunction);
	}
	
	// Update is called once per frame
	void Update () {
        textList[1].text = leftToUse.ToString() + " left to use";       
        if(leftToUse == 0)
        {
            combat_manager.RefreshSlotItemList(orderButton);
        }
	}

    void ButtonFunction()
    {
        if (leftToUse > 0)
        {
            combat_manager.SetCurrentItemPress(this.gameObject);
            combat_manager.UsingItemInCombat(playerInventoryOrder, orderButton);           
        }
    }        

    public int GetLeftToUse()
    {
        return leftToUse;
    }

    public void SetLeftToUse(int _amount)
    {
        leftToUse = _amount;
    } 

    public int ORDERBUTTON
    {
        get
        {
            return orderButton;
        }
        set
        {
            orderButton = value;
        }
    }

    public int PLAYERINVENTBUTTON
    {
        get
        {
            return playerInventoryOrder;
        }
        set
        {
            playerInventoryOrder = value;
        }
    }

    public void SetCombatManager(CombatManagerScript _script)
    {
        combat_manager = _script;
    }

    public Definition.ConsumableItemType ITEMTYPE
    {
        get
        {
            return consumableItemType;
        }
        set
        {
            consumableItemType = value;
        }
    }

    public void ReductOneUse()
    {
        leftToUse--;
    }
}
