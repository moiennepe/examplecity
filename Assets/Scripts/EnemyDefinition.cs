using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDefinition {

    public enum stats
    {
        STR,
        SPD,
        DEX,
        CON,
        INT,
        WIS,
        LUK,
        COUNT
    }
    
    public enum Types
    {
        DOUND,
        MELTER
    }   

    public enum ConsumableItemType
    {
        HEALING,
        REVIVE
    }

    public enum NormalAttackType
    {
        LIGHTATTACK,
        MEDIUMATTACK,
        HEAVYATTACK
    }

       public enum ConsumableItemSize
    {
       SMALL,
       MEDIUM,
       LARGE
    }



    public enum slotUIText
    {
        NAME,
        ROLE,
        LEVEL,
        JOB,
        HP,
        AP,
        STR,
        SPD,
        DEX,
        CON,
        INT,
        WIS,
        LUK
    }

}
