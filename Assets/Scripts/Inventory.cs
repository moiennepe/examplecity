﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory {

    // Use this for initialization
    List<ConsumableItem> consumableInventory;
    List<Armor> armorInventory;
    List<Weapon> weaponInventory;
    List<KeyItem> keyItemInventory;
    List<Accessories> accessoriesInventory;
    int credits;

	public void InventoryInit()
    {
        consumableInventory = new List<ConsumableItem>();
        armorInventory = new List<Armor>();
        weaponInventory = new List<Weapon>();
        keyItemInventory = new List<KeyItem>();
        accessoriesInventory = new List<Accessories>();
        credits = 0;    
    }

    public void AddConsumbableItem(ConsumableItem _itemToAdd)
    {
        bool isInventoryAlreadyHave = false;
        for (int i = 0; i < consumableInventory.Count; i++)
        {
            if (consumableInventory[i].Name == _itemToAdd.Name)
            {
                consumableInventory[i].Amount += _itemToAdd.Amount;
                isInventoryAlreadyHave = true;
                break;
            }
        }
        if (isInventoryAlreadyHave == false)
        {
            consumableInventory.Add(_itemToAdd);
        }      
    }

    public void AddWeapon(Weapon _itemToAdd)
    {
        weaponInventory.Add(_itemToAdd);
    }

    public void AddArmor(Armor _itemToAdd)
    {
        armorInventory.Add(_itemToAdd);
    }

    public void AddKeyItem(KeyItem _itemToAdd)
    {
        keyItemInventory.Add(_itemToAdd);
    }

    public void AddAccessories(Accessories _itemToAdd)
    {      
        accessoriesInventory.Add(_itemToAdd);
    }

    public int CREDITS
    {
        get
        {
            return credits;
        }
        set
        {
            credits = value;
        }
    }

    public List<ConsumableItem> GetConsumableItemInventory()
    {
        return consumableInventory;
    }

    public List<Armor> GetArmorInvent()
    {
        return armorInventory;
    }

    public List<Weapon> GetWeaponInvent()
    {
        return weaponInventory;
    }

    public List<KeyItem> GetKeyItemInvent()
    {
        return keyItemInventory;
    }

    public List<Accessories> GetAccessoriesInvent()
    {      
        return accessoriesInventory;
    }

    public void UseItem(Definition.ItemType _type, int _slotOrder)
    {
        if(_type == Definition.ItemType.CONSUMABLE)
        {
            consumableInventory[_slotOrder].Amount -= 1;
            if(consumableInventory[_slotOrder].Amount <= 0)
            {
                consumableInventory.Remove(consumableInventory[_slotOrder]);
            }
        }
    }   
}
