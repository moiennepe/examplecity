﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Definition {

    public enum PlayerStats
    {
        STR,
        SPD,
        DEX,
        CON,
        INT,
        WIS,
        LUK,
        COUNT
    }
    
    public enum CharacterRoles
    {
        SLAYER,
        GUARDIAN,
        SUPPORTER,
        REBUKER,
        COUNT
    }   

    public enum ItemType
    {
        CONSUMABLE,
        WEAPON,
        ARMOR,
        KEYITEM,
        ACCESSORIES
    }

    public enum NormalAttackType
    {
        LIGHTATTACK,
        MEDIUMATTACK,
        HEAVYATTACK
    }

    public enum AlignmentType
    {
        ORDER,
        NEUTRAL,
        FREEDOM
    }

    public enum ConsumableItemSize
    {
       SMALL,
       MEDIUM,
       LARGE
    }

    public enum CharacterName
    {
        HERO,
        SLAYER,
        SPARREN,
        COUNT
    }

    public enum CharacterSlotUIText
    {
        NAME,
        ROLE,
        LEVEL,
        JOB,
        HP,
        AP,
        STR,
        SPD,
        DEX,
        CON,
        INT,
        WIS,
        LUK,
        COUNT
    }

    public enum EnemyTypes
    {
        DOUND,
        DOUND_SUMMONED,
        MELTER,
        SHADOW,
        BOSSUNKNOWNDREAM
    }

    public enum StartCombatConditions
    {
        PLAYERATTACKSENEMY,
        ENEMYATTACKSPLAYER,
        PLAYERENEMYCOLLISION,
        SCRIPTEDEVENTENCOUNTER
    }

    public enum ConsumableItemType
    {
        POTION,
        REVIVE,
        ENERGYDRINK,
        BANDAGE
    }
}
