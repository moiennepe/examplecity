using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : Enemy
{
    public override void Start()
    {       
        level = 2;
        xpReward = 27;
        jpReward = 8;
        types = Definition.EnemyTypes.SHADOW;
        monsterName = "Shadow";

        //array indexes of each stat:
        //STR - 0
        //SPD - 1
        //DEX - 2
        //CON - 3
        //INT - 4
        //WIS - 5
        //LUK - 6
        enemyStats = new int[] { 6, 6, 6, 6, 2, 2, 1};
        hpStats = 450;
        maxhpStats = hpStats;
        randomNumber = UnityEngine.Random.Range(0, 101);
        weaponDamage = 3;
        coolDownAttack = Random.Range(0, 6);
    }

    public override void Attack()
    {
        if(coolDownAttack > 6)
        {
            randomAttackSkill = UnityEngine.Random.Range(1, 5);
            if (randomAttackSkill == 1)
            {
                AttackTearDown();
            }
            else
            {
                NormalAttack();
            }
            coolDownAttack = 0;
        }      
    }

    void AttackTearDown()
    {        
        NormalAttack();
        charactersList[orderCharacterAttack].AP--;              
    }

    public override void AddingWinningItem(List<ConsumableItem> consumableItemList, List<Weapon> weaponItemList, List<Armor> armorItemList, List<Accessories> accessoryItemList)
    {
        randomNumber = UnityEngine.Random.Range(0, 101);
        if (randomNumber > 0 && randomNumber < 40)
        {
            ConsumableItem bandage = new ConsumableItem();
            bandage.Init(1, "Bandage");
            bandage.EffectString = "Cure Bleeding";
            bandage.EffectDescription = "Your standard fiber bandage, used to stop bleeding and/or the evacuation of blood cells from your body.";
            bandage.HEALINGEFFECT = 0;
            bandage.SellPrice = 10;
            bandage.BuyPrice = 60;
            bandage.CONSUMABLEITEMTYPE = Definition.ConsumableItemType.BANDAGE;
            bandage.Type = Definition.ItemType.CONSUMABLE;
            consumableItemList.Add(bandage);
        }        
    }
}
