﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatManagerScript : MonoBehaviour
{
    //this are the serialized functions that can be seen using the editor
    #region
// Use this for initialization
    [SerializeField]
	GameObject prefab_monster_placeholder, prefabHero, prefabSparren, prefabSlayer, prefabTensionUsingButton, prefabFeedBackNoTension, prefabItemUsingButton,prefabFeedBackNoCharSelect;

	[SerializeField]
    List<GameObject> enemyList, characterListIcon, characterList, combo_input_list, tension_list_button, item_list_button, hyperTension_list_button, team_combo_list_button;

    [SerializeField]
	int increaseAPSpeed, totalExpGained, totalJpGained, totalCreditsGain;

    [SerializeField]
	Character selectedCharacter, targetAlly;

    [SerializeField]
	List<Enemy> enemyListScript;

    [SerializeField]
	GameObject player;

    [SerializeField]
	GameObject attackButton, tensionButton, itemButton, fleeButton, returnToMenuButton, returnToSavePointButton, continueButton;

    [SerializeField]
	GameObject prefab_characterIconSlot, prefab_combo_text_input_ui;

    [SerializeField]
	GameObject previous_panel, combo_list_panel, input_list_panel, tension_list_panel, tension_description_panel, character_panel_list, tension_firstpart_panel
		, feed_back_choose_target_panel, combat_ui_panel, item_list_panel, item_conent, losing_panel, winning_panel, first_tension_list_content, second_tension_list_content;

    [SerializeField]
	GameObject arrow_choose_enemy_text, combo_input_text, tension_description_text, item_description_text;

    [SerializeField]
	List<Definition.NormalAttackType> combo_input;

    [SerializeField]
	GameObject[] prefab_monster_list;

    [SerializeField]
	MusicManager musicManager;

    [SerializeField]
	List<int> hpListBeforeCombat, comsumableInventoryBeforeCombat;
	[SerializeField]
	GameObject[] SpawnLocation;

    [SerializeField]
    GameObject previousTensionPress, currrentTensionPress, previousItemUse, currentItemUse;
    #endregion 


    int enemyChoose = 0;
	public int playerIndex, allyIndex;
	int totalAPUsed = 0;
	int skillOrderChoose = 0;
	int numberOfMonsterInstance;
	int maxMonster = 5;
	int totalCreditReward, totalJobExpgain, totalXpReward;

    float battleTime = 0;
	float timer = 0;
	float screenScale;

    bool pressed = false;
	bool isOnTargetAllyMode = false;
	bool winning, losing, calculatingWinningDone;

	List<ConsumableItem> consumableItemDropWinning;
	List<Weapon> weaponItemDropWinning;
	List<Armor> armorItemDropWinning;
	List<Accessories> accessoryItemDropWinning;


	Player playerScript;

	void Start()
	{
		screenScale = Screen.width / 1920.0f;
		PopUpTextController.Init();
		CombatUI_Init();
		InitPlayerCharacterList();
		InitPlayerPrefab();
		InitEnemyPrefab();
		ItemListUpdate();
		SavingInformationBeforeCombat();
        SetCombatConditions();

		maxMonster = 5;

		musicManager = FindObjectOfType<MusicManager>();
		calculatingWinningDone = true;
	}

	// Update is called once per frame
	void Update()
	{
		if (losing)
		{
			// set retry panel
			losing_panel.SetActive(true);
		}
		else if (winning)
		{
			// set winning panel          
			winning_panel.SetActive(true);
		}
		else if (losing != true || winning != true)
		{
			PlayerControlCombatTest();
			CombatUpdateUI();
			CombatUpdate();
			ComboListPanelUpdate();
		}

		if (calculatingWinningDone)
		{
			CombatWinningRewardCalculation();
		}
	}

	private void InitEnemyPrefab()
	{

    /*<summary>
    -generates an enemy list/item drops list
    -iterate trough the enemies ? (i still dont understand why just iterate twice )and set all the attacking variables 
    -generate the items to win after the battle

     <summary>*/
		enemyList = new List<GameObject>();
		enemyListScript = new List<Enemy>();
		numberOfMonsterInstance = Random.Range(1, 4);
		consumableItemDropWinning = new List<ConsumableItem>();
		weaponItemDropWinning = new List<Weapon>();
		armorItemDropWinning = new List<Armor>();
		accessoryItemDropWinning = new List<Accessories>();
		Definition.EnemyTypes type = player.GetComponent<Player>().ENEMYCOUNTERTYPE;

        //note: ask Phil what is this for
		for (int i = 0; i < 2; i++)
		{
			GameObject newEnemy = Instantiate(prefab_monster_list[(int)type]);

			enemyList.Add(newEnemy);
			newEnemy.transform.position = SpawnLocation[i].transform.position;
			newEnemy.SetActive(true);
			SpawnLocation[i].GetComponent<SpawnLocation>().SetSpawnReserved();

			Enemy enemyComp = newEnemy.GetComponent<Enemy>();
			enemyComp.Start();
			enemyComp.SetCombatManagerScript(this);
			enemyComp.SetCharacterList(playerScript.GetCharacterList());
			enemyComp.SelectTargetForAttack();
			enemyComp.SetSlotSpawnnPosition(i);
		}
        //end of note
		for (int i = 0; i < enemyList.Count; i++)
		{
			enemyListScript.Add(enemyList[i].GetComponent<Enemy>());
			enemyListScript[i].AddingWinningItem(consumableItemDropWinning, weaponItemDropWinning, armorItemDropWinning, accessoryItemDropWinning);
		}

		Vector2 screenPosition = Camera.main.WorldToScreenPoint(enemyList[0].transform.position);
		arrow_choose_enemy_text.transform.position = screenPosition;
	}

	public void SummonMonster(Definition.EnemyTypes _type)
	{
        /*<summary>
         
        -creates new enemy objects and add them to the enemy list
        -set the enemy location for spawn
        -sets all variables the enemy needs to work(target, winning item etc..)
        
        <summary>*/


        GameObject newEnemy = Instantiate(prefab_monster_list[(int)_type]);
        Enemy enemyComp = newEnemy.GetComponent<Enemy>();
        enemyList.Add(newEnemy);

        for (int i = 0; i < SpawnLocation.Length; i++)
        {
            if (SpawnLocation[i].GetComponent<SpawnLocation>().GetCollidingStatus() == false)
            {
                newEnemy.transform.position = SpawnLocation[i].transform.position;
                SpawnLocation[i].GetComponent<SpawnLocation>().SetSpawnReserved();
                enemyComp.SetSlotSpawnnPosition(i);
                break;
            }
        }
        newEnemy.SetActive(true);

        enemyComp.SetCombatManagerScript(this);
        enemyComp.SetCharacterList(playerScript.GetCharacterList());
        enemyComp.SelectTargetForAttack();
        enemyComp.AddingWinningItem(consumableItemDropWinning, weaponItemDropWinning, armorItemDropWinning, accessoryItemDropWinning);

        enemyListScript.Add(enemyComp);
    }

	private void InitPlayerPrefab()
	{

        /*<summary>
         
        -create character list and fill it up with PC(playable character)information

        
        <summary>*/

        characterList = new List<GameObject>();
		combo_input = new List<Definition.NormalAttackType>();

		if (playerScript)
		{
			for (int i = 0; i < playerScript.GetCharacterList().Count; i++)
			{
				Definition.CharacterName type = playerScript.GetCharacterList()[i].GetCharacterNameType;
				if (type == Definition.CharacterName.HERO)
				{
					characterList.Add(Instantiate(prefabHero));
				}
				else if (type == Definition.CharacterName.SLAYER)
				{
					characterList.Add(Instantiate(prefabSlayer));
				}
				else if (type == Definition.CharacterName.SPARREN)
				{
					characterList.Add(Instantiate(prefabSparren));
				}

				characterList[i].transform.Translate(0, 0, -0.15f + (0.15f * i));
				characterList[i].SetActive(true);

				playerScript.GetCharacterList()[i].SetNumberInOrderCombat(i);
				playerScript.GetCharacterList()[i].SetCombatManager(this);
			}

			//Select first character
			selectedCharacter = playerScript.GetCharacterList()[0];
		}
	}

	private void PlayerControlCombatTest()
	{
        /*<summary>
        - enemy target selection
        - attack combo system
        
        <summary>*/
        // use the arrow key to select wich enemy to attack (Yellow arrow)
        int keyDirection = 0;
        #region Enemy selection
        if (Input.GetKeyDown(KeyCode.DownArrow))
			keyDirection--;
		else if (Input.GetKeyDown(KeyCode.UpArrow))
			keyDirection++;

		if (keyDirection != 0)
		{
			enemyChoose += keyDirection;

			if (enemyChoose < 0)
			{
				enemyChoose = enemyList.Count - 1;
			}
			else if (enemyChoose + 1 > enemyList.Count)
			{
				enemyChoose = 0;
			}

			arrow_choose_enemy_text.transform.position = Camera.main.WorldToScreenPoint(enemyList[enemyChoose].transform.position);
		}
        #endregion

        #region Combos and actions
        // tested later will be change to ap of selected character and also allt he AP HP TENSION DMG / selected enemy
        if (combo_list_panel.activeSelf)
		{
			if (selectedCharacter != null)
			{
				if (selectedCharacter.HP > 0)
				{
					if (Input.GetKeyDown(KeyCode.Z) && selectedCharacter.AP - totalAPUsed - 1 >= 0)
					{
						// do light attack
						//selectedCharacter.AP -= 1;
						//selectedCharacter.TENSION += 1;                       
						combo_input_text.GetComponent<Text>().text += "L > ";
						combo_input.Add(Definition.NormalAttackType.LIGHTATTACK);
						totalAPUsed += 1;
						pressed = true;
					}
					else if (Input.GetKeyDown(KeyCode.X) && selectedCharacter.AP - totalAPUsed - 2 >= 0)
					{
						// do medium attack
						//selectedCharacter.AP -= 2;
						//selectedCharacter.TENSION += 2;
						combo_input_text.GetComponent<Text>().text += "M > ";
						combo_input.Add(Definition.NormalAttackType.MEDIUMATTACK);
						totalAPUsed += 2;
						pressed = true;
					}

					else if (Input.GetKeyDown(KeyCode.C) && selectedCharacter.AP - totalAPUsed - 3 >= 0)
					{
						// do heavy attack          
						//selectedCharacter.AP -= 3;
						//selectedCharacter.TENSION += 3;
						combo_input_text.GetComponent<Text>().text += "H > ";
						combo_input.Add(Definition.NormalAttackType.HEAVYATTACK);
						totalAPUsed += 3;
						pressed = true;
					}

					else if (Input.GetKeyDown(KeyCode.Space))
					{
						ResetComboAPUsed();
						UpdatingAllComboOnUI();
					}

					else if (Input.GetKeyDown(KeyCode.Return))
					{
						selectedCharacter.CheckingCombo(combo_input, enemyList[enemyChoose].transform, enemyListScript[enemyChoose], totalAPUsed);
						ResetComboAPUsed();
						UpdatingAllComboOnUI();
					}
				}
			}
		}
        #endregion
    }

    private void InitPlayerCharacterList()
	{
        /*<summary>
         
        -sets up the icons and other info for each of the playable characters

        
        <summary>*/

        player = GameObject.Find("Player");

		if (player)
		{
			playerScript = player.GetComponent<Player>();
			player.gameObject.SetActive(false);
		}

		if (playerScript)
		{
			float length = prefab_characterIconSlot.GetComponent<RectTransform>().rect.height;
			for (int i = 0; i < playerScript.GetCharacterList().Count; i++)
			{
				GameObject temp = Instantiate(prefab_characterIconSlot, character_panel_list.transform);
				temp.GetComponentInChildren<CharacterSlotButtonScript>().SetIndex(i);
				temp.GetComponentInChildren<CharacterSlotButtonScript>().SetCharacterNameType(playerScript.GetCharacterList()[i].GetCharacterNameType);
				temp.GetComponentInChildren<CharacterSlotButtonScript>().SetCombatManager(GetComponent<CombatManagerScript>());
				Image tempImage = temp.GetComponentsInChildren<Image>()[1];
				if (playerScript.GetCharacterList()[i].GetCharacterName == "Hero")
				{
					tempImage.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("hero_icon_placement");
				}
				else if (playerScript.GetCharacterList()[i].GetCharacterName == "Sparren")
				{
					tempImage.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("sparren_icon_placement");
				}
				else if (playerScript.GetCharacterList()[i].GetCharacterName == "Slayer")
				{
					tempImage.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("slayer_icon_placement");
				}
				temp.transform.Translate(0, (-i * length) * screenScale, 0);
				characterListIcon.Add(temp);
			}
		}
	}

	void CombatUI_Init()
	{
        /*<summary>
         -Set up all the UI for the combat

        
        <summary>*/

        characterListIcon = new List<GameObject>();
		combo_input_list = new List<GameObject>();
		item_list_button = new List<GameObject>();
		tension_list_button = new List<GameObject>();
		hyperTension_list_button = new List<GameObject>();
		team_combo_list_button = new List<GameObject>();

		prefab_characterIconSlot = Resources.Load<GameObject>("character_icon_button_combat_panel");
		prefab_combo_text_input_ui = Resources.Load<GameObject>("combo_list_input_text_name");
		prefabTensionUsingButton = Resources.Load<GameObject>("tension_using_button");
		prefabFeedBackNoTension = Resources.Load<GameObject>("feed_back_not_enough_tension");
		prefabItemUsingButton = Resources.Load<GameObject>("item_using_button_combat");
		prefabFeedBackNoCharSelect = Resources.Load<GameObject>("feed_back_no_item_use");

		combo_list_panel = GameObject.Find("combo_list_panel");
		input_list_panel = GameObject.Find("input_list");
		tension_list_panel = GameObject.Find("tension_list_panel");
		tension_description_panel = GameObject.Find("tension_description_panel");
		character_panel_list = GameObject.Find("character_panel_list");
		tension_firstpart_panel = GameObject.Find("tension_list_first_part_panel");
		feed_back_choose_target_panel = GameObject.Find("feed_back_choose_target_panel");
		combat_ui_panel = GameObject.Find("Combat_UI_Panel");
		item_list_panel = GameObject.Find("item_list_panel");
		item_conent = GameObject.Find("item_Content");
		winning_panel = GameObject.Find("winning_panel");
		losing_panel = GameObject.Find("losing_panel");
		first_tension_list_content = GameObject.Find("first_tension_list_content");
		second_tension_list_content = GameObject.Find("second_tension_list_content");

		attackButton = GameObject.Find("attack_button");
		fleeButton = GameObject.Find("flee_button");
		itemButton = GameObject.Find("item_button");
		tensionButton = GameObject.Find("tension_button");
		returnToMenuButton = GameObject.Find("return_to_main_menu_button");
		returnToSavePointButton = GameObject.Find("load_save_point");
		continueButton = GameObject.Find("continue_button");

		arrow_choose_enemy_text = GameObject.Find("arrow_choose_enemy_text");
		combo_input_text = GameObject.Find("combo_input_text");
		combo_input_text.GetComponent<Text>().text = "";
		tension_description_text = GameObject.Find("tension_skill_description_text");
		item_description_text = GameObject.Find("item_description");
		tension_description_text.GetComponent<Text>().text = "";
		item_description_text.GetComponent<Text>().text = "";

		attackButton.GetComponent<Button>().onClick.AddListener(AttackButtonFunction);
		tensionButton.GetComponent<Button>().onClick.AddListener(TensionButtonFunction);
		itemButton.GetComponent<Button>().onClick.AddListener(ItemButtonFunction);
		fleeButton.GetComponent<Button>().onClick.AddListener(FleeButton);
		returnToMenuButton.GetComponent<Button>().onClick.AddListener(ReturnToMenuButton);
		returnToSavePointButton.GetComponent<Button>().onClick.AddListener(ReturnToSavePoint);
		continueButton.GetComponent<Button>().onClick.AddListener(ContinueButton);


		combo_list_panel.gameObject.SetActive(false);
		tension_list_panel.gameObject.SetActive(false);
		feed_back_choose_target_panel.gameObject.SetActive(false);
		item_list_panel.SetActive(false);
		losing_panel.SetActive(false);
		winning_panel.SetActive(false);
	}
    #region Buttons for the combat ui
    void AttackButtonFunction()
	{
       
		if (previous_panel == combo_list_panel)
        return;
		if (previous_panel)
        previous_panel.SetActive(false);
		combo_list_panel.SetActive(true);
		previous_panel = combo_list_panel;
		ResetComboAPUsed();

		isOnTargetAllyMode = false;
		tension_description_text.GetComponent<Text>().text = "";
		previousTensionPress = null;
		currrentTensionPress = null;
		previousItemUse = null;
		currentItemUse = null;
		if (selectedCharacter != null)
			UpdatingAllComboOnUI();
	}

	void TensionButtonFunction()
	{
		if (previous_panel == tension_list_panel)
			return;
		if (previous_panel)
			previous_panel.SetActive(false);
		tension_list_panel.SetActive(true);
		previous_panel = tension_list_panel;

		isOnTargetAllyMode = false;
		tension_description_text.GetComponent<Text>().text = "";
		previousItemUse = null;
		currentItemUse = null;
		if (selectedCharacter != null)
		{
			ResetTension();
			TensionListUpdate();
		}
	}

	void ItemButtonFunction()
	{
		if (previous_panel == item_list_panel)
			return;
		if (previous_panel)
			previous_panel.SetActive(false);
		item_list_panel.SetActive(true);
		previous_panel = item_list_panel;

		isOnTargetAllyMode = false;
		tension_description_text.GetComponent<Text>().text = "";
		previousTensionPress = null;
		currrentTensionPress = null;
	}

	void FleeButton()
	{
		player.SetActive(true);
		playerScript.switchInvicibleState();
		CombatEnd();
		UnityEngine.SceneManagement.SceneManager.LoadScene(playerScript.GetLastScene());
	}
    
    private void CombatUpdateUI()
	{
		if (playerScript)
		{
			for (int i = 0; i < playerScript.GetCharacterList().Count; i++)
			{
				if (playerScript.GetCharacterList()[i].HP <= 0)
				{
					Image[] tempListImage = characterListIcon[i].GetComponentsInChildren<Image>();
					tempListImage[1].color = Color.red;
				}
				else
				{
					Image[] tempListImage = characterListIcon[i].GetComponentsInChildren<Image>();
					tempListImage[1].color = Color.white;
				}
				Text[] textList = characterListIcon[i].GetComponentsInChildren<Text>();
				textList[0].text = "HP " + playerScript.GetCharacterList()[i].HP.ToString();
				textList[1].text = "Tension " + playerScript.GetCharacterList()[i].Tension.ToString();
				textList[2].text = "AP " + playerScript.GetCharacterList()[i].AP.ToString();

				characterListIcon[i].GetComponentInChildren<Slider>().value = playerScript.GetCharacterList()[i].HyperTension;

				if (playerScript.GetCharacterList()[i].HyperTension >= characterListIcon[i].GetComponentInChildren<Slider>().maxValue)
				{
					characterListIcon[i].GetComponentInChildren<Animator>().enabled = true;
				}
				else
				{
					characterListIcon[i].GetComponentInChildren<Animator>().Play(0);
				}
			}
		}
		if (isOnTargetAllyMode == true)
		{
			feed_back_choose_target_panel.SetActive(true);
		}
		else
		{
			feed_back_choose_target_panel.SetActive(false);
		}
	}

    #endregion


    private void CombatUpdate()
	{


        /*<summary>
         
        -Main Combat loop
        -- check if party is still alive
        -- the ap gain per second to perform actions
        --remove enemies as they die in combat
        <summary>*/


        if (enemyListScript.Count <= 0)
		{
			winning = true;
            RemoveAllStatusEffects();
		}

		List<Character> party = playerScript.GetCharacterList();

		// If anybody is alive, we can't lose
		losing = true;
		foreach (Character c in party)
		{
			if (c.HP > 0)
				losing = false;
		}

		timer += Time.deltaTime;
		//// Every 3 seconds, gain 1 AP
		//if (timer >= 3)
		//{
		//	if (playerScript)
		//	{
		//		foreach (Character c in party)
		//		{
		//			if (c.AP < c.MaxAP)
		//				c.AP++;
		//		}
		//	}
		//	timer = 0;
		//}

		foreach (Character c in party)
		{
			c.Update();            
		}

		for (int i = enemyList.Count - 1; i >= 0; i--)
		{
			if (enemyList[i].GetComponent<Enemy>().HP <= 0)
			{
				SpawnLocation[enemyListScript[i].GetSlotSpawnPosition()].GetComponent<SpawnLocation>().SetUnReserve();
				Destroy(enemyList[i]);
				enemyList.RemoveAt(i);
				enemyListScript.RemoveAt(i);
				if (enemyList.Count > 0)
				{
					enemyChoose = enemyList.Count - 1;
					arrow_choose_enemy_text.transform.position = Camera.main.WorldToScreenPoint(enemyList[enemyChoose].transform.position);
				}
			}
		}
	}





	public void SetSelectedGameObject(int characterIndex, Definition.CharacterName type)
	{
		if (playerScript)
		{
			if (!isOnTargetAllyMode)
			{
				if (playerIndex != characterIndex)
				{
					selectedCharacter = playerScript.GetCharacterList()[(int)(type)];
					playerIndex = characterIndex;
					ResetComboAPUsed();
					ResetTension();
					TensionListUpdate();
					UpdatingAllComboOnUI();
				}
			}
			else
			{
				allyIndex = characterIndex;
				targetAlly = playerScript.GetCharacterList()[(int)(type)];
				selectedCharacter.UsingTensionSkill(skillOrderChoose, enemyListScript, enemyListScript[enemyChoose], playerScript.GetCharacterList(), targetAlly, allyIndex);
				isOnTargetAllyMode = false;
			}
		}
	}

	void ResetComboAPUsed()
	{
		totalAPUsed = 0;
		combo_input.Clear();
		combo_input_text.GetComponent<Text>().text = "";
		for (int i = 0; i < combo_input_list.Count; i++)
		{
			DestroyImmediate(combo_input_list[i]);
		}
		combo_input_list.Clear();
	}

	void ComboListPanelUpdate()
	{
		if (player && selectedCharacter != null)
		{
			if (selectedCharacter.HP > 0)
			{
				if (pressed)
				{
					for (int i = 0; i < combo_input_list.Count; i++)
					{
						DestroyImmediate(combo_input_list[i]);
					}
					combo_input_list.Clear();

					int create = 0;

					foreach (CharacterSkill skill in selectedCharacter.GetCharacterSkillList)
					{
						bool test = true;
						List<Definition.NormalAttackType> combo = skill.GetComboInput();
						if (combo_input.Count <= combo.Count)
						{
							if (combo_input.Count >= 1)
							{
								for (int k = 0; k < combo_input.Count; k++)
								{
									if (combo_input[k] != combo[k])
									{
										test = false;
										break;
									}
								}
								if (test)
								{
									GameObject instance = Instantiate(prefab_combo_text_input_ui, combo_list_panel.transform);
									instance.transform.Translate(0, (-instance.GetComponent<RectTransform>().rect.height * create) * screenScale, 0);
									instance.GetComponent<Text>().text = skill.GetSkillName() + "(" +
										skill.GetTotalAp().ToString() + " AP)";
									Text[] textList = instance.GetComponentsInChildren<Text>();
									textList[1].text = "";
									for (int j = 0; j < combo.Count; j++)
									{
										if (combo[j] == Definition.NormalAttackType.LIGHTATTACK)
										{
											textList[1].text += "L > ";
										}

										else if (combo[j] == Definition.NormalAttackType.MEDIUMATTACK)
										{
											textList[1].text += "M > ";
										}

										else if (combo[j] == Definition.NormalAttackType.HEAVYATTACK)
										{
											textList[1].text += "H > ";
										}
									}
									combo_input_list.Add(instance);
									create++;
								}
							}
						}
					}
					pressed = false;
				}
			}
		}
	}

	void TensionListUpdate()
	{
		float lenght = prefabTensionUsingButton.GetComponent<RectTransform>().rect.height * 1.25f;
		if (playerScript && selectedCharacter != null)
		{
			for (int i = 0; i < selectedCharacter.GetTensionList.Count; i++)
			{
				GameObject instance;
				if (selectedCharacter.GetTensionList[i].HYPERTENSION == true || selectedCharacter.GetTensionList[i].TEAMCOMBO == true)
				{
					instance = Instantiate(prefabTensionUsingButton, second_tension_list_content.transform);
				}
				else
				{
					instance = Instantiate(prefabTensionUsingButton, first_tension_list_content.transform);
				}

				instance.transform.Translate(0, (-i * lenght) * screenScale, 0);
				Text[] temp = instance.GetComponentsInChildren<Text>();
				temp[0].text = selectedCharacter.GetTensionList[i].GetTensionName();
				if (selectedCharacter.GetTensionList[i].HYPERTENSION == true)
				{
					temp[1].text = "Hyper Tension";
				}
				else
				{
					temp[1].text = selectedCharacter.GetTensionList[i].TOTALTENSION.ToString() + " Tension";
				}
				instance.GetComponent<TensionButton>().SetSkillOrder(i);
				instance.GetComponent<TensionButton>().SetCombatManager(this);
				instance.GetComponent<TensionButton>().SetAllyTargetBool(selectedCharacter.GetTensionList[i].TARGETALLY);
				instance.GetComponent<TensionButton>().SetHyperTensionBool(selectedCharacter.GetTensionList[i].HYPERTENSION);
				instance.GetComponent<TensionButton>().SetTeamComboBool(selectedCharacter.GetTensionList[i].TEAMCOMBO);
				tension_list_button.Add(instance);

				if (i == 0)
				{
					if (selectedCharacter.UnlockedHyperTension == false)
					{
						tension_list_button[i].SetActive(false);
					}
				}
				else if (i == 1)
				{
					if (selectedCharacter.UnlockedTeamCombo == false)
					{
						tension_list_button[i].SetActive(false);
					}
				}
			}
		}
	}

	public void SetDescriptionTensionUI(int _skillOrder)
	{
		skillOrderChoose = _skillOrder;
		tension_description_text.GetComponent<Text>().text = selectedCharacter.GetTensionList[_skillOrder].GetTensionDescription();

		if (currrentTensionPress)
		{
			if (selectedCharacter != null && enemyListScript.Count > 0)
			{
				if (selectedCharacter.HP > 0)
				{
					if (previousTensionPress != currrentTensionPress || previousTensionPress == null)
					{
						previousTensionPress = currrentTensionPress;
						isOnTargetAllyMode = false;
					}
					else
					{
						if (selectedCharacter.GetTensionList[_skillOrder].HYPERTENSION || selectedCharacter.GetTensionList[_skillOrder].TEAMCOMBO)
						{
							if (selectedCharacter.HyperTension <= 200)
							{
								selectedCharacter.UsingTensionSkill(_skillOrder, enemyListScript, enemyListScript[enemyChoose], playerScript.GetCharacterList(), targetAlly, allyIndex);
							}
							else
							{
								GameObject temp = Instantiate(prefabFeedBackNoTension, combat_ui_panel.transform);
								temp.GetComponentInChildren<Text>().text = "Not enough hyper tension";
								Destroy(temp, 1);
							}
						}
						else
						{
							if (selectedCharacter.GetTensionList[_skillOrder].TOTALTENSION <= selectedCharacter.Tension)
							{
								if (tension_list_button[_skillOrder].GetComponent<TensionButton>().GetAllyTarget() == true)
								{
									isOnTargetAllyMode = true;
								}
								else
								{
									selectedCharacter.UsingTensionSkill(_skillOrder, enemyListScript, enemyListScript[enemyChoose], playerScript.GetCharacterList(), targetAlly, allyIndex);
								}
							}
							else
							{
								GameObject temp = Instantiate(prefabFeedBackNoTension, combat_ui_panel.transform);
								Destroy(temp, 1);
							}
						}
					}
				}
			}
		}
	}

	void ResetTension()
	{
		tension_description_text.GetComponent<Text>().text = "";
		for (int i = 0; i < tension_list_button.Count; i++)
		{
			DestroyImmediate(tension_list_button[i]);
		}

		for (int i = 0; i < hyperTension_list_button.Count; i++)
		{
			DestroyImmediate(hyperTension_list_button[i]);
		}

		for (int i = 0; i < team_combo_list_button.Count; i++)
		{
			DestroyImmediate(team_combo_list_button[i]);
		}
		hyperTension_list_button.Clear();
		team_combo_list_button.Clear();
		tension_list_button.Clear();
		previousTensionPress = null;
		currrentTensionPress = null;
		isOnTargetAllyMode = false;
	}

	void ItemListUpdate()
	{
		float length = prefabItemUsingButton.GetComponent<RectTransform>().rect.height * 1.25f;
		if (playerScript)
		{
			List<ConsumableItem> consumables = playerScript.GetPlayerInventory().GetConsumableItemInventory();
			for (int i = 0; i < consumables.Count; i++)
			{
				ConsumableItem item = consumables[i];
				GameObject instance = Instantiate(prefabItemUsingButton, item_conent.transform);
				instance.transform.Translate(0, (-i * length) * screenScale, 0);
				Text[] textList = instance.GetComponentsInChildren<Text>();
				textList[0].text = item.Name;
				if (item.Amount > 9)
				{
					instance.GetComponent<ItemUsingButtonCombat>().SetLeftToUse(9);
				}
				else if (item.Amount <= 9)
				{
					instance.GetComponent<ItemUsingButtonCombat>().SetLeftToUse(item.Amount);
				}
				instance.GetComponent<ItemUsingButtonCombat>().SetCombatManager(this);
				instance.GetComponent<ItemUsingButtonCombat>().ORDERBUTTON = i;
				instance.GetComponent<ItemUsingButtonCombat>().PLAYERINVENTBUTTON = i;
				instance.GetComponent<ItemUsingButtonCombat>().ITEMTYPE = item.CONSUMABLEITEMTYPE;
				item_list_button.Add(instance);
			}
		}
	}

	public void SetPreviousTensionButton(GameObject _tensionButotn)
	{
		currrentTensionPress = _tensionButotn;
	}

	public void RemoveInventoryItemCombatEnd()
	{
		// todo after finish combat  
		List<ConsumableItem> consumables = playerScript.GetPlayerInventory().GetConsumableItemInventory();

		for (int i = consumables.Count - 1; i > 0; i--)
		{
			ConsumableItem item = consumables[i];

			if (item.Amount == 0)
			{
				consumables.RemoveAt(i);
			}
		}
	}

	public GameObject GetPlayer()
	{
		return player;
	}

	public void UsingItemInCombat(int _slotOrder, int _uislotOrder)
	{
		if (selectedCharacter != null)
		{
			List<ConsumableItem> consumables = playerScript.GetPlayerInventory().GetConsumableItemInventory();
			if (previousItemUse == currentItemUse && previousItemUse != null)
			{
				if (consumables[_slotOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.POTION)
				{
					if (selectedCharacter.HP > 0)
					{
						selectedCharacter.HP += consumables[_slotOrder].HEALINGEFFECT;
						consumables[_slotOrder].Amount--;
						if (selectedCharacter.HP > selectedCharacter.MaxHP)
						{
							selectedCharacter.HP = selectedCharacter.MaxHP;
						}
						item_list_button[_uislotOrder].GetComponent<ItemUsingButtonCombat>().ReductOneUse();
					}
				}
				else if (consumables[_slotOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.REVIVE)
				{
					// TODO small or large revive
					if (selectedCharacter.HP <= 0)
					{
						consumables[_slotOrder].Amount--;
						if (consumables[_slotOrder].HEALINGEFFECT == 50)
						{
							selectedCharacter.HP = selectedCharacter.MaxHP / 2;
						}
						else if (consumables[_slotOrder].HEALINGEFFECT == 100)
						{
							selectedCharacter.HP = selectedCharacter.MaxHP;
						}
						item_list_button[_uislotOrder].GetComponent<ItemUsingButtonCombat>().ReductOneUse();
					}
				}
				else if (consumables[_slotOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.BANDAGE)
				{
					// TODO Effect
					if (selectedCharacter.HP > 0)
					{
						consumables[_slotOrder].Amount--;
						selectedCharacter.SetBleedStatus(false);

						item_list_button[_uislotOrder].GetComponent<ItemUsingButtonCombat>().ReductOneUse();
					}
				}
				else if (consumables[_slotOrder].CONSUMABLEITEMTYPE == Definition.ConsumableItemType.ENERGYDRINK)
				{
					if (selectedCharacter.HP > 0)
					{
						consumables[_slotOrder].Amount--;
						selectedCharacter.Tension += consumables[_slotOrder].HEALINGEFFECT;
						if (selectedCharacter.Tension > 200)
						{
							selectedCharacter.Tension = 200;
						}
						item_list_button[_uislotOrder].GetComponent<ItemUsingButtonCombat>().ReductOneUse();
					}
				}
			}
			else if (previousItemUse != currentItemUse || previousItemUse == null)
			{
				previousItemUse = currentItemUse;
				item_description_text.GetComponent<Text>().text = consumables[_slotOrder].EffectDescription;
			}
		}
		else if (selectedCharacter == null)
		{
			// notice no char selected
			GameObject temp = Instantiate(prefabFeedBackNoCharSelect, combat_ui_panel.transform);
			Destroy(temp, 1);
		}
	}

	public void RefreshSlotItemList(int _uiSlotOrder)
	{
		DestroyImmediate(item_list_button[_uiSlotOrder]);
		item_list_button.RemoveAt(_uiSlotOrder);
		item_description_text.GetComponent<Text>().text = "";
		for (int i = 0; i < item_list_button.Count; i++)
		{
			item_list_button[i].GetComponent<ItemUsingButtonCombat>().ORDERBUTTON = i;
		}
	}

	public Character GetSelectedCharacter()
	{
		return selectedCharacter;
	}

	public List<GameObject> GetCharacterList()
	{
		return characterList;
	}

	void CombatEnd()
	{
		RemoveInventoryItemCombatEnd();
		for (int i = 0; i < playerScript.GetCharacterList().Count; i++)
		{
			Character character = playerScript.GetCharacterList()[i];
			if (character.HP < 0)
			{
				character.HP = 0;
			}
			character.Tension = 0;
			character.AP = character.MaxAP;
			character.ResetBadStatus();
			character.ResetGoodStatus();
			character.HyperTension = 0;
		}

	}

	void CombatWinningRewardCalculation()
	{
		foreach (Enemy enemy in enemyListScript)
		{
			totalXpReward += enemy.GetXPReward();
			totalJobExpgain += enemy.GetJPReward();
			//totalCreditReward += enemy.GetCreditReward();
		}

		calculatingWinningDone = false;
	}

	void CombatWinningRewardSend()
	{
		foreach (Character character in playerScript.GetCharacterList())
		{
			character.PlayerExpProgression(totalXpReward);
			character.PlayerJpProgression(totalJobExpgain);
		}
		//playerScript.GetPlayerInventory().CREDITS += totalCreditReward;
		for (int i = 0; i < consumableItemDropWinning.Count; i++)
		{
			playerScript.GetPlayerInventory().AddConsumbableItem(consumableItemDropWinning[i]);
		}

		for (int i = 0; i < weaponItemDropWinning.Count; i++)
		{
			playerScript.GetPlayerInventory().AddWeapon(weaponItemDropWinning[i]);
		}

		for (int i = 0; i < armorItemDropWinning.Count; i++)
		{
			playerScript.GetPlayerInventory().AddArmor(armorItemDropWinning[i]);
		}

		for (int i = 0; i < accessoryItemDropWinning.Count; i++)
		{
			playerScript.GetPlayerInventory().AddAccessories(accessoryItemDropWinning[i]);
		}
	}

	public void SetCurrentItemPress(GameObject _itemPress)
	{
		currentItemUse = _itemPress;
	}

	void ReturnToMenuButton()
	{
		Destroy(player);
		UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
	}

	void ReturnToSavePoint()
	{
		for (int i = 0; i < playerScript.GetCharacterList().Count; i++)
		{
			playerScript.GetCharacterList()[i].HP = hpListBeforeCombat[i];
		}

		for (int i = 0; i < playerScript.GetPlayerInventory().GetConsumableItemInventory().Count; i++)
		{
			playerScript.GetPlayerInventory().GetConsumableItemInventory()[i].Amount = comsumableInventoryBeforeCombat[i];
		}

		player.SetActive(true);
		playerScript.switchInvicibleState();
		UnityEngine.SceneManagement.SceneManager.LoadScene(playerScript.GetLastScene());
	}

	void SavingInformationBeforeCombat()
	{
		//temporary before finishing saving combat info(checkpoint)
		hpListBeforeCombat = new List<int>();
		comsumableInventoryBeforeCombat = new List<int>();

		for (int i = 0; i < playerScript.GetCharacterList().Count; i++)
		{
			hpListBeforeCombat.Add(playerScript.GetCharacterList()[i].HP);
		}

		for (int i = 0; i < playerScript.GetPlayerInventory().GetConsumableItemInventory().Count; i++)
		{
			comsumableInventoryBeforeCombat.Add(playerScript.GetPlayerInventory().GetConsumableItemInventory()[i].Amount);
		}
	}

	void ContinueButton()
	{
		CombatEnd();
		CombatWinningRewardSend();
		player.SetActive(true);
		playerScript.switchInvicibleState();
		UnityEngine.SceneManagement.SceneManager.LoadScene(playerScript.GetLastScene());
	}

	public List<Enemy> GetEnemyList()
	{
		return enemyListScript;
	}

	public int GetTotalMaxMonster()
	{
		return maxMonster;
	}

	void UpdatingAllComboOnUI()
	{
		for (int i = 0; i < selectedCharacter.GetCharacterSkillList.Count; i++)
		{
			GameObject instance = Instantiate(prefab_combo_text_input_ui, combo_list_panel.transform);
			instance.transform.Translate(0, (-instance.GetComponent<RectTransform>().rect.height * i) * screenScale, 0);
			CharacterSkill skill = selectedCharacter.GetCharacterSkillList[i];
			instance.GetComponent<Text>().text = skill.GetSkillName() + "(" +
				skill.GetTotalAp().ToString() + " AP)";
			Text[] textList = instance.GetComponentsInChildren<Text>();
			textList[1].text = "";
			for (int j = 0; j < skill.GetComboInput().Count; j++)
				if (skill.GetComboInput()[j] == Definition.NormalAttackType.LIGHTATTACK)
				{
					textList[1].text += "L > ";
				}

				else if (skill.GetComboInput()[j] == Definition.NormalAttackType.MEDIUMATTACK)
				{
					textList[1].text += "M > ";
				}

				else if (skill.GetComboInput()[j] == Definition.NormalAttackType.HEAVYATTACK)
				{
					textList[1].text += "H > ";
				}
			combo_input_list.Add(instance);
		}
	}

	public void CreatePopUpNotificationInCombatScene(string text)
	{
		GameObject temp = Instantiate(prefabFeedBackNoTension, combat_ui_panel.transform);
		temp.GetComponentInChildren<Text>().text = text;
		Destroy(temp, 1);
	}

    private void RemoveAllStatusEffects()
    {
        foreach (Character Character in playerScript.GetCharacterList())
        {
            for (int i = 0; i<Character.statusEffectList.Count; i++)
            {
                Character.statusEffectList[i].OnEnd();
                Character.statusEffectList.RemoveAt(i);
            }
        }
    }

    private void SetCombatConditions()
    {
        if (player.GetComponent<Player>().STARTCOMBATCONDITIONS== Definition.StartCombatConditions.PLAYERATTACKSENEMY)
        {
            foreach (Character chara in playerScript.GetCharacterList())
            {
                chara.AP += 2;
            }
        }
    }

}
