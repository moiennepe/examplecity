﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableItem : Item {

    // Use this for initialization

    Definition.ConsumableItemType consumableItemType;
    int healingEffect;

    public override void Init(int _amount, string _itemName)
    {
        base.Init(_amount, _itemName);
        type = Definition.ItemType.CONSUMABLE;
    }

    public Definition.ConsumableItemType CONSUMABLEITEMTYPE
    {
        get
        {
            return consumableItemType;
        }
        set
        {
            consumableItemType = value;
        }
    }

    public int HEALINGEFFECT
    {
        get
        {
            return healingEffect;
        }
        set
        {
            healingEffect = value;
        }
    }
}
