﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
using SETUtil;

[CustomEditor(typeof(ERMeshGen))]
public class ERMeshGenEditor : Editor
{
    private string[] constantVertsUpdateStr = {"Automatic (Recommended)", "Manual Update", "Update Vertices Only", "Realtime"};
	private string[] includeColliderStr = {"Off", "On"};
	private string[] enableHelpStr = {"Off", "On"};
	private string[] uvSetStr = {"Per Segment", "Top Projection", "Match Width", "Stretch Single Texture"};
	private string[] enableMeshBordersStr = {"Off", "On"};
	private string[] borderUvSetStr = {"Straight Unwrap", "Top Projection"};

	static bool pendingDuplication = false;
	
	ERMeshGen meshGen;
	
	SerializedObject so_meshGen_;
	SerializedProperty
		meshGen_deltaWidth,
		meshGen_subdivision,
		meshGen_uvScale,
		meshGen_groundOffset,
		meshGen_pointControl,
		meshGen_vertsUpdate,
		meshGen_runtimeBehavior,
		meshGen_includeCollider,
		meshGen_enableHelp,
		meshGen_uvSet,
		meshGen_enableMeshBorders,
		meshGen_borderCurve,
		meshGen_borderUvSet;
	
	public void OnEnable ()
	{
		Init();
	}
	
	public void Init ()
	{	
		meshGen = (ERMeshGen)target; 
		so_meshGen_ = new SerializedObject(meshGen);
		meshGen_deltaWidth = so_meshGen_.FindProperty("deltaWidth");
		meshGen_subdivision = so_meshGen_.FindProperty("subdivision");
		meshGen_uvScale = so_meshGen_.FindProperty("uvScale");
		meshGen_groundOffset = so_meshGen_.FindProperty("groundOffset");
		meshGen_pointControl = so_meshGen_.FindProperty("pointControl");
		meshGen_vertsUpdate = so_meshGen_.FindProperty("updatePointsMode");
		meshGen_runtimeBehavior = so_meshGen_.FindProperty("runtimeBehavior");
		meshGen_includeCollider = so_meshGen_.FindProperty("includeCollider");
		meshGen_enableHelp = so_meshGen_.FindProperty("enableHelp");
		meshGen_uvSet = so_meshGen_.FindProperty("uvSet");
		meshGen_enableMeshBorders = so_meshGen_.FindProperty("enableMeshBorders");
		meshGen_borderCurve = so_meshGen_.FindProperty("borderCurve");
		meshGen_borderUvSet = so_meshGen_.FindProperty("borderUvSet");
		
		DuplicationHandler();
	}
	
	public void OnSceneGUI () {
		Event e = Event.current;
			if(e != null)
				if (e.commandName == "Duplicate")
					pendingDuplication = true;
		DuplicationHandler();
	}
	
	void DuplicationHandler ()
	{						
		if(meshGen){
			//update ER
			if(ERMeshGen.lastSelectedMeshGen){
				if(meshGen.gameObject.GetInstanceID() != ERMeshGen.lastSelectedMeshGen.gameObject.GetInstanceID()){
					if(pendingDuplication){
						pendingDuplication = false;
						meshGen.OnDuplicationEvent();
					}
					ERMeshGen.lastSelectedMeshGen.UpdateData();
					ERMeshGen.lastSelectedMeshGen = meshGen;
				}
			}else{
				ERMeshGen.lastSelectedMeshGen = meshGen;
			}
		}
	}
	
	public override void OnInspectorGUI()
	{
		DuplicationHandler();
		
		EditorUtil.BeginColorPocket();

		if(meshGen.GetComponent<MeshFilter>()){
			meshGen_deltaWidth.floatValue = EditorGUILayout.FloatField("Delta Width", meshGen.deltaWidth);
			meshGen_subdivision.intValue = EditorGUILayout.IntField("Subdivision", meshGen.subdivision);
			meshGen_uvScale.floatValue = EditorGUILayout.FloatField("UV Scale", meshGen.uvScale);
			
			EditorUtil.BeginColorPocket(new Color(meshGen.updatePointsMode/2f,1,0.3f));
			meshGen_vertsUpdate.intValue = EditorGUILayout.Popup("Update Mode", meshGen.updatePointsMode, constantVertsUpdateStr);
			EditorUtil.EndColorPocket();

			EditorGUILayout.PropertyField(meshGen_runtimeBehavior);

			meshGen_pointControl.enumValueIndex = (int) (ERMG.PointControl) EditorGUILayout.EnumPopup("Point Control", meshGen.pointControl);
			meshGen_includeCollider.intValue = EditorGUILayout.Popup("Include Collider", meshGen.includeCollider, includeColliderStr);
			meshGen_uvSet.intValue = EditorGUILayout.Popup("UVs", meshGen.uvSet, uvSetStr);
			
			GUILayout.BeginVertical(EditorStyles.helpBox);
			meshGen_enableMeshBorders.intValue = EditorGUILayout.Popup("Borders Mesh", meshGen.enableMeshBorders, enableMeshBordersStr);
			
			if(meshGen.enableMeshBorders == 1){
				EditorGUI.indentLevel++;
				meshGen_borderCurve.animationCurveValue = EditorGUILayout.CurveField("Border Shape", meshGen.borderCurve);
				meshGen_borderUvSet.intValue = EditorGUILayout.Popup("Border UVs", meshGen.borderUvSet, borderUvSetStr);
				EditorGUI.indentLevel--;
			}

			GUILayout.EndVertical();
			
			EditorUtil.BeginColorPocket(new Color(0.3f,1,0.1f));
			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Add Nav Point"))
			{
				meshGen.CreateNavPoint();
			}
			GUI.color = new Color(1,1,0.1f);
			if(meshGen.navPoints.Count > 1)
				if(GUILayout.Button("Delete Nav Point")){
					meshGen.DeleteNavPoint();
				}
			GUILayout.EndHorizontal();
			GUI.color = new Color(0.3f,1,0.1f);
			if(meshGen.updatePointsMode > 0 && meshGen.updatePointsMode != 3)
				if(GUILayout.Button("Apply Changes"))
				{
					meshGen.UpdateData();
				}
				
			GUI.color = new Color(0.6f,0.6f,1);
			EditorUtil.HorizontalRule();
			
			GUILayout.Box("Ground nav points to surface underneath.\nOffset: " + meshGen.groundOffset, EditorStyles.helpBox, GUILayout.MaxWidth(999));
			meshGen_groundOffset.floatValue = EditorGUILayout.Slider(meshGen.groundOffset,0,1);
			
			if(GUILayout.Button("Ground Points")){
				meshGen.GroundPoints(meshGen.groundOffset);
			}
			
			EditorUtil.HorizontalRule();

			GUILayout.BeginHorizontal();
			{
				EditorUtil.BeginColorPocket(new Color(1,0.2f,0.2f));
				
				if(GUILayout.Button("Reset")) {
					if(!EditorUtility.DisplayDialog("Reset?", "Are you sure you want to clear the current mesh and delete all Nav Points?", "No", "Yes"))
						meshGen.ResetMesh();
				}
				
				EditorUtil.EndColorPocket();

				if(GUILayout.Button("Export (Experimental)")) {
					if(EditorUtility.DisplayDialog("Warning", "This is an experimental featre. Results may be inaccurate or partially incomplete.", "Continue", "Cancel")){
						ERMG.Util.ExportToOBJ(meshGen.gameObject);
					}
				}
				
			}
			GUILayout.EndHorizontal();
			
			SETUtil.EditorUtil.BeginColorPocket(Color.yellow);
			if(GUILayout.Button("Finalize")) {
				if(EditorUtility.DisplayDialog("Warning", "This will remove all components except MeshCollider, MeshFilter and MeshRenderer and will export all related mesh assets. " + 
					"Path Tracer component will no longer have a Mesh Gen component to draw data from. Changes cannot be reverted.", "Continue", "Cancel")){
					meshGen.FinalizeMeshGen();
					EditorGUIUtility.ExitGUI();
					return;
				}
			}
			SETUtil.EditorUtil.EndColorPocket();

			EditorUtil.EndColorPocket();
		}

		
		meshGen_enableHelp.intValue = EditorGUILayout.Popup("Enable Tips", meshGen.enableHelp, enableHelpStr);
		if(meshGen.enableHelp != 0){
			SETUtil.EditorUtil.BeginColorPocket(new Color(1,1,0.7f));
			GUILayout.Box("Important: Object rotation should be (0,0,0) at all times!", EditorStyles.helpBox);
			GUI.color = new Color(1,0.2f,0.2f);
			SETUtil.EditorUtil.EndColorPocket();
			GUILayout.Box("Report any problems on e-mail: hropenmail01@abv.bg", EditorStyles.helpBox);
		}

		EditorUtil.EndColorPocket();
		
		so_meshGen_.ApplyModifiedProperties();
	}
}
#endif