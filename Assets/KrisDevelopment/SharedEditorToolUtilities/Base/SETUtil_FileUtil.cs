﻿
////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System.IO;
using System.Collections;
using System.Collections.Generic;
using SETUtil.Common.Extend;

namespace SETUtil
{
	public static class FileUtil
	{
		public static string CreateFilePathString (string fileName, string fileExtension, bool relativePath = true) 
		{
			return (UnityEngine.Application.dataPath + "/").ShowIf(!relativePath) + fileName + "." + fileExtension;
		}

		public static string CreateFolderPathString (string folderName, bool relativePath = true) 
		{
			return (UnityEngine.Application.dataPath + "/").ShowIf(!relativePath) + folderName;
		}

		public static void WriteTextToFile (string path, string content, bool relativePath = true)
		{
			string _filePath = (UnityEngine.Application.dataPath + "/").ShowIf(!relativePath) + path;
			FileInfo _fileInfo = new FileInfo(_filePath);

			_fileInfo.Directory.Create();
			StreamWriter _writer = File.CreateText(_filePath);
			_writer.Close();
			File.WriteAllText(_filePath, content);
		}

		public static bool ReadTextFromFile (string path, out string content, bool relativePath = true)
		{
			string _filePath = (UnityEngine.Application.dataPath + "/").ShowIf(!relativePath) + path;
			content = string.Empty;

			if(File.Exists(_filePath)){
				content = File.ReadAllText(_filePath);
				return true; //read success
			}

			return false; //read failed
		}
	}
}