﻿
	////////////////////////////////////////
	//    Shared Editor Tool Utilities    //
	//    by Kris Development             //
	////////////////////////////////////////

	//License: MIT
	//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System.Collections;
using System.Collections.Generic;
using SETUtil;
using SETUtil.Extend;

using U = UnityEngine;
using Gl = UnityEngine.GUILayout;
using System.Linq;

#if UNITY_EDITOR
using E = UnityEditor;
#endif

//SETUtil.Common contains class names that might overlap with system namespaces
namespace SETUtil.Common
{
	public enum MouseButton {
		Left = 0,
		Right = 1,
		Middle = 2,
	}
	
	public class RandomAccessStack<T>{
		private static int MAX_LIFE = 50; //if an element lives this long then delete it
		private int
			idIterator = 0,
			index = 0;
		List<RandomAccessElement<T>> elements = new List<RandomAccessElement<T>>();

		public int Next {
			get{ return ++idIterator; }
		}
		public int Count
		{
			get { return elements.Count; }
		}
		public T this[int i]{
			get{ return elements[i].el; }
		}
		
		//METHODS:
		private void Push (RandomAccessElement<T> element)
		{
			elements.Add(element);
		}

		public T Pop ()
		{
			if (index > 0) {
				RandomAccessElement<T> element = elements[elements.Count - 1];
				elements.Remove(element);
				index--;
				return element.el;
			}

			EditorUtil.Debug("[SETUtil.RandomAccessStack ERROR] Stack is Empty.");
			return default(T);
		}

		public void SmartPush (T element, int id){
			//push or overwrite the element with corresponding id
			int _foundMatch = -1;
			for(int i = 0; i < elements.Count; i++)
				if(elements[i].id == id)
					_foundMatch = i;
			
			if(_foundMatch > -1){
				elements[_foundMatch].el = element;
				elements[_foundMatch].id = id;
				elements[_foundMatch].life = 0;
			}
			else{
				Push(new RandomAccessElement<T>(element, id, 0));
			}
		}
		
		public void Age (){
			for(int i = 0; i < elements.Count; elements[i].life++, i++);
			Trim();
		}
		
		public void Trim () {
			//check for life and delete variables that are not in use
			var _el = elements.FirstOrDefault(a => a.life > MAX_LIFE);
			if(_el != null)
				elements.Remove(_el);
		}
		
		public void ResetIdIterator(){
			idIterator = 0;
		}
	}
	
	public class RandomAccessElement<T> {
		public T el = default(T);
		public int
			id = -1,
			life = 0;
			
		public RandomAccessElement(){
			el = default(T);
			id = -1;
			life = 0;
		}
		
		public RandomAccessElement(T element, int id, int life){
			this.el = element;
			this.id = id;
			this.life = life;
		}
	}
}
