﻿
	////////////////////////////////////////
	//    Shared Editor Tool Utilities    //
	////////////////////////////////////////
	
	// NOTE:
	//   The purpose of SETUtil is to act as a hub,
	// isolating serialization weak points
	// and providing common utilities,
	// to allow for more streamlined editor tool development.
	
	//GitLab: https://gitlab.com/KrisDevelopment/SETUtil
	
using System;

namespace SETUtil.Common.Extend
{
    public static class CommonExtend
    {
        public static string ShowIf (this string left, bool condition)
        {
            return (condition ? left : string.Empty);
        }
    }
}