﻿
	////////////////////////////////////////
	//    Shared Editor Tool Utilities    //
	//    by Kris Development             //
	////////////////////////////////////////

	//License: MIT
	//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System.Collections;
using System.Collections.Generic;
using SETUtil;
using SETUtil.UI;
using SETUtil.SceneUI;
using SETUtil.Common;
using SETUtil.Extend;
using SETUtil.Types;

using U = UnityEngine;
using G = UnityEngine.GUI;
using Gl = UnityEngine.GUILayout;

#if UNITY_EDITOR
using E = UnityEditor;
using EGl = UnityEditor.EditorGUILayout;
#endif

namespace SETUtil
{
	//ENUMS:
	
	public enum ArrayFieldOption
	{
		Default = 0, //do nothing
		ShowAsLabels = (1 << 0), //show content as labels
		PreviewOnly = (1 << 1), //preview content as GUI
		FixedSize = (1 << 2), //allow content modification, but no direct resizing through interface
		NoClearButton = (1 << 3), //content modification and array resizing but no "clear" button
		NoIndex = (1 << 4),
		NoBoundries = (1 << 5), //hide the array drawer boundries
	}
	
	//CLASSES:
	public static class EditorUtil
	{
		public delegate void DebugLog(object msg);
		public static DebugLog debugLog;
		
		private static Stack<U.Color> colorStack = new Stack<U.Color>(2);
		private static Common.RandomAccessStack<iGUIElement> guiQueue = new Common.RandomAccessStack<iGUIElement>();
		private static NavigationFieldData lastDrawnNavField = null;
		
		//DEBUGGING

		///<summary>
		/// Prints the debug using the current editor message mode (message, warning, error)
		/// and the debugLog(msg) callback is invoked. Useful for custom console implementations.
		///</summary>
		public static void Debug(object msg)
		{
			Debug(msg, Preferences.DEBUG_PREF);
		}
		
		///<summary>
		/// Prints the debug and the debugLog(msg) callback is invoked. Useful for custom console implementations.
		/// The pref allows you to specify a Debug Preference (message, warning, error)
		///</summary>
		public static void Debug(object msg, DebugPreference pref)
		{
			//push to Unity console
			switch (pref){
				case DebugPreference.Message:
					U.Debug.Log(msg);
				break;
				
				case DebugPreference.Warning:
					U.Debug.LogWarning(msg);
				break;
				
				case DebugPreference.Error:
					U.Debug.LogError(msg);
				break;
			}
			
			if(debugLog != null)
				debugLog(msg);
		}
		
		//SCENE GUI		
		public static void DrawSceneElement(iGUIElement element)
		{
			SubGUIDelegate();
			guiQueue.SmartPush(element, guiQueue.Next);
		}
		
		public static void DrawSceneElement(iGUIElement element, U.Rect newRect)
		{
			element.rect = newRect;
			DrawSceneElement(element);
		}
		
		public static void DrawSceneElement(iGUIElement element, U.Vector3 newPos)
		{
			element.position = newPos;
			DrawSceneElement(element);
		}
		
		//utilities for calling specific elements with less code:
		///<summary> Draws a label in the scene view at the given position </summary>
		public static void DrawSceneLabel(string text, U.Vector3 position)
		{
			GUILabel _label = new GUILabel(text);
			_label.CenterRect();
			DrawSceneElement(_label, position);
		}
		
		///<summary> Draws a label in the scene view at the given rect </summary>
		public static void DrawSceneLabel(string text, U.Rect rect)
		{
			DrawSceneElement(new GUILabel(text, rect));
		}

		///<summary> Draws a button in the scene view at the given position </summary>
		public static void DrawSceneButton(string text, U.Vector3 position, System.Action onClick)
		{
			GUIButton _button = new GUIButton(text, onClick);
			_button.CenterRect();
			DrawSceneElement(_button, position);
		}
		
		///<summary> Draws a button in the scene view at the given rect </summary>
		public static void DrawSceneButton(string text, U.Rect rect, System.Action onClick)
		{
			DrawSceneElement(new GUIButton(text, rect, onClick));
		}
		
		//WINDOW GUI

		///<summary> Draws simple horizontal GUI line </summary>
		public static void HorizontalRule ()
		{
			Gl.Box("", Gl.ExpandWidth(true), Gl.Height(2));
		}
		
		public static bool ExpandButton (bool b, string label, U.FontStyle fontStyle = U.FontStyle.Normal) { return ExpandButton(b, label, 20, fontStyle); }
		public static bool ExpandButton (bool b, string label, int height, U.FontStyle fontStyle = U.FontStyle.Normal) {
			bool _b = b;
			
			Gl.BeginHorizontal();
				U.GUIStyle _style = new U.GUIStyle(U.GUI.skin.button);
				_style.fontStyle = fontStyle;
				_style.richText = true;
				_style.alignment = U.TextAnchor.MiddleLeft;
				
				if(Gl.Button((b ? "▼ " : "► ") + label, _style, Gl.Height(height)))
					_b = !b;
			Gl.EndHorizontal();
			return _b;
		}
		
		/// <summary> Pushes current GUI colors to the stack. </summary>
		public static void BeginColorPocket()
		{
			colorStack.Push(U.GUI.color);
			colorStack.Push(U.GUI.contentColor);
			colorStack.Push(U.GUI.backgroundColor);
		}
		
		/// <summary> Opens the color pocked and pushes current GUI colors to the stack. </summary>
		public static void BeginColorPocket(U.Color clr)
		{
			BeginColorPocket();
			U.GUI.color = clr;
		}
		
		/// <summary> Returns the GUI colors to their previous state. </summary>
		public static void EndColorPocket()
		{
			U.GUI.backgroundColor = colorStack.Pop();
			U.GUI.contentColor = colorStack.Pop();
			U.GUI.color = colorStack.Pop();
		}
		

		/// <summary> Opens the navigation field drawer. </summary>
		public static void BeginNavigationField (ref NavigationFieldData navField, U.Vector2? restriction = null)
		{
			lastDrawnNavField = navField;
			
			U.Rect _fieldRect = FindLayoutAreaRect(ref navField.backupRect, navField.border);
			
			BeginColorPocket(navField.backgroundColor);
			Gl.BeginArea(_fieldRect, "", "Box");
			EndColorPocket();
			
			navField.DrawBackground();
			navField.DragUpdate();
			
			U.Vector2 _restriction = restriction ?? (new U.Vector2(_fieldRect.width, _fieldRect.height) - navField.scrollView);
			Gl.BeginArea(new U.Rect(navField.scrollView.x, navField.scrollView.y, _restriction.x, _restriction.y));
		}
		
		///<summary> Closes the Navigation Field. (Optional) Draws additional controls on top of the content. </summary>
		public static void EndNavigationField (bool showNativeControls = true)
		{
			Gl.EndArea(); //end offset area
			if(lastDrawnNavField != null){
				if (showNativeControls) {
					if (G.Button(new U.Rect(lastDrawnNavField.backupRect.width - lastDrawnNavField.border - 23, 5, 20, 20), new U.GUIContent("+", "Center View")))
						lastDrawnNavField.CenterView();
				}

				#if UNITY_EDITOR
				G.Button(new U.Rect(0,0,lastDrawnNavField.backupRect.width, lastDrawnNavField.backupRect.height), "", "Label"); //force hot control
				#endif
			}
			Gl.EndArea(); //end field viewport area
			
			if (lastDrawnNavField != null) {
				lastDrawnNavField.DragUpdate();
			}

			lastDrawnNavField = null;
		}
		
		public static U.Rect FindLayoutAreaRect(ref U.Rect backupRect, int border = 0)
		{
			//DRAW DUMMY LAYOUT GROUP TO GET THE RECT FROM
			Gl.BeginVertical(Gl.MaxWidth(U.Screen.width), Gl.MaxHeight(U.Screen.height));
			Gl.Label("");
			Gl.EndVertical();
			U.Rect _fieldRect = U.GUILayoutUtility.GetLastRect();
			
			//rect update handling (ignore dummy rect at layout event)
			if(U.Event.current.type != U.EventType.Repaint)
				_fieldRect = backupRect;
			else
				backupRect = _fieldRect;
			
			_fieldRect.x += border;
			_fieldRect.y += border;
			_fieldRect.width -= border*2;
			_fieldRect.height -= border*2;
			
			return _fieldRect;
		}
		
		//build-compatible GUI utilities
		
		///<summary> List GUI drawer utility </summary>
		public static List<T> ArrayFieldGUI<T>(List<T> list, ArrayFieldOption option = ArrayFieldOption.Default)
		{
			T[] arr = new T[list.Count];
			System.Array.Copy(list.ToArray(), arr, list.Count);
			list.Clear();
			list.AddRange(ArrayFieldGUI(arr, option));
			return list;
		}

		///<summary> Array GUI drawer utility </summary>
		public static T[] ArrayFieldGUI<T>(T[] arr, ArrayFieldOption option = ArrayFieldOption.Default)
		{
			if((option & ArrayFieldOption.NoBoundries ) != ArrayFieldOption.NoBoundries)
			{
				#if UNITY_EDITOR
				Gl.BeginVertical(E.EditorStyles.helpBox);
				#else
				Gl.BeginVertical("", "Box");
				#endif
			}
			{
				for (int i = 0; i < arr.Length; i++){
					Gl.BeginHorizontal();
					if((option & ArrayFieldOption.NoIndex ) != ArrayFieldOption.NoIndex)
						Gl.Label("("+ i + ")", Gl.Width(i.ToString().Length * 8 + 16));
					
					bool _castSuccess = false;
					
					//check if type T implements interface iDrawableProperty, if not call DrawPropertyField
					if((option & ArrayFieldOption.ShowAsLabels ) != ArrayFieldOption.ShowAsLabels)
					{
						if(typeof(iDrawableProperty).IsAssignableFrom(typeof(T))){
							iDrawableProperty _drawableObject = null;
							_drawableObject = arr[i] as iDrawableProperty;
							if(_drawableObject != null){
								_drawableObject.DrawAsProperty((option & ArrayFieldOption.PreviewOnly ) != ArrayFieldOption.PreviewOnly);
								_castSuccess = true;
							}
						}else{
							_castSuccess = DrawPropertyField(ref arr[i], (option & ArrayFieldOption.PreviewOnly ) != ArrayFieldOption.PreviewOnly);
						}
						
						if(!_castSuccess)
							Gl.Label("Object of type " + typeof(T) + " to is NULL or cast has failed.");
					}
					else {
						if(arr[i] != null)
							Gl.Label(arr[i].ToString());
						else
							Gl.Label("NULL");
					}
					
					if((option & ArrayFieldOption.PreviewOnly ) != ArrayFieldOption.PreviewOnly)
					{
						if((option & ArrayFieldOption.NoClearButton ) != ArrayFieldOption.NoClearButton)
							if(Gl.Button(new U.GUIContent("C", "Clear Field"), U.GUILayout.MaxWidth(32)))
								arr[i] = default(T);

						if((option & ArrayFieldOption.FixedSize ) != ArrayFieldOption.FixedSize)
							if (Gl.Button(new U.GUIContent("X" , "Remove"), U.GUILayout.MaxWidth(32))) {
								List<T> _arrList = new List<T>();
								_arrList.AddRange(arr);
								_arrList.RemoveAt(i);
								arr = _arrList.ToArray();
							}
					}
					
					Gl.EndHorizontal();
				}

				//navigation
				if ((option & ArrayFieldOption.FixedSize ) != ArrayFieldOption.FixedSize)
				{
					if (Gl.Button("Add Element")) {
						List<T> _arrList = new List<T>();
						_arrList.AddRange(arr);
						_arrList.Add(default(T));
						arr = _arrList.ToArray();
					}
				}
			}
			if((option & ArrayFieldOption.NoBoundries ) != ArrayFieldOption.NoBoundries)
			{
				Gl.EndVertical();
			}
			
			return arr;
		}
		
		//OBJECT HANDLING
		///<summary> Method meant to prevent unwanted asset termination </summary>
		public static bool ValidatePrefabTermination<T> (T obj) where T : U.Object
		{
			if(obj == null){
				EditorUtil.Debug("[ValidatePrefabTermination] NULL object");
				return false;
			}
			
			bool _validTermination = false; 
				
			#if UNITY_EDITOR
				bool _isPrefab = false;
				U.Object _prefab = null;
					
				#if UNITY_2018_3_OR_NEWER
					_prefab = E.PrefabUtility.GetPrefabInstanceHandle(obj);
				#else
					_prefab = E.PrefabUtility.GetPrefabObject(obj);
				#endif
			
				_isPrefab = _prefab != null && _prefab.Equals(obj);
				
				if(!_isPrefab || !E.EditorUtility.DisplayDialog("Confirm action", "Seems like object " + obj.name + " is a prefab! This action will permanently delete the asset from your project!\nContinue?", "Yes", "No"))
					_validTermination = true;
			#else
				_validTermination = true;
			#endif
			
			return _validTermination;
		}
		
		//Scene GUI Control
		///<summary>
		/// Method intended for internal use by the library.
		/// Subscribes the scene GUI drawer to the onSceneGUIDelegate.
		///</summary>
		public static void SubGUIDelegate () {
			#if UNITY_EDITOR
				E.SceneView.onSceneGUIDelegate -= DrawQueuedSceneGUI;
				E.SceneView.onSceneGUIDelegate += DrawQueuedSceneGUI;
			#endif
		}
		
		public static void UnsubGUIDelegate(){
			#if UNITY_EDITOR
				E.SceneView.onSceneGUIDelegate -= DrawQueuedSceneGUI;
				ResetQueueIterator();
			#endif
		}
		
		//private:
		#if UNITY_EDITOR
		private static void DrawQueuedSceneGUI (E.SceneView sceneView)
		{
			if(sceneView != null){				
				for(int i = 0; i < guiQueue.Count; i++){
					if(guiQueue[i] != null){
						E.Handles.BeginGUI();
						guiQueue[i].Draw(sceneView);
						
						E.Handles.EndGUI();
					}
				}
			}
			ResetQueueIterator();
		}
		#endif
		
		
		private static void ResetQueueIterator ()
		{			
			guiQueue.ResetIdIterator();
			guiQueue.Age();
		}
		
		private static bool DrawPropertyField<T> (ref T property, bool allowModify = true)
		{
			//method returns true if casting was successful
			bool _castSuccess = false;
			
			//Bool
			if(typeof(bool).IsAssignableFrom(typeof(T))){
				bool _elementAsBool = default(bool);
				if(_castSuccess = property.TryCast<bool>(out _elementAsBool))
					Gl.Toggle(_elementAsBool, "State");
				if(allowModify)
					_elementAsBool.TryCast<T>(out property);
			}
			
			//Int
			else if(typeof(int).IsAssignableFrom(typeof(T))){
				int _elementAsInt = default(int);
				if(_castSuccess = property.TryCast<int>(out _elementAsInt)){
					#if UNITY_EDITOR
					_elementAsInt = EGl.IntField(_elementAsInt);
					#else
						int.TryParse(Gl.TextField(_elementAsInt.ToString()), out _elementAsInt);
					#endif
				}
				if(allowModify)
					_elementAsInt.TryCast<T>(out property);
			}
			
			//Float
			else if(typeof(float).IsAssignableFrom(typeof(T))){
				float _elementAsFloat = default(float);
				if(_castSuccess = property.TryCast<float>(out _elementAsFloat)){
					#if UNITY_EDITOR
					_elementAsFloat = EGl.FloatField(_elementAsFloat);
					#else
						float.TryParse(Gl.TextField(_elementAsFloat.ToString()), out _elementAsFloat);
					#endif
				}
				if(allowModify)
					_elementAsFloat.TryCast<T>(out property);
			}
			
			//String
			else if(typeof(string).IsAssignableFrom(typeof(T))){
				string _elementAsString = property as string;
				_elementAsString = Gl.TextField(_elementAsString);
				_castSuccess = true;
			}
			
			//UnityEngine.Object
			else if(typeof(U.Object).IsAssignableFrom(typeof(T))){
				_castSuccess = true;

				U.Object _elementAsObject = default(U.Object);
				property.TryCast<U.Object>(out _elementAsObject);
					
				#if UNITY_EDITOR
				_elementAsObject = EGl.ObjectField(_elementAsObject, typeof(T), true);
				#else
				Gl.Label(_elementAsObject.ToString());
				#endif

				if(allowModify)
					_elementAsObject.TryCast<T>(out property);
			}

			return _castSuccess;
		}
	}
}
