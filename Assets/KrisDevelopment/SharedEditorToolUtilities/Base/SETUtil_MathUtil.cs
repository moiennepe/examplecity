﻿
	////////////////////////////////////////
	//    Shared Editor Tool Utilities    //
	//    by Kris Development             //
	////////////////////////////////////////

	//License: MIT
	//GitLab: https://gitlab.com/KrisDevelopment/SETUtil
	
using System.Collections;
using System.Collections.Generic;
using U = UnityEngine;

namespace SETUtil {
	public static class MathUtil {
		public static float Map (float val, float low1, float high1, float low2, float high2) {
			//this remaps the value from its expected low and high to the new low2 and high2
			return low2 + (val - low1) * ((high2 - low2)/(high1 - low1));
		}
		
		public static float Saturate (float val){
			return UnityEngine.Mathf.Clamp(val, 0f, 1f);
		}
		
		public static float PerlinVolume (U.Vector3 point, float perlinScale, int seed){
			float
				_passXY = U.Mathf.PerlinNoise((float)(seed + point.x) * perlinScale, (float)(seed + point.y) * perlinScale),
				_passZY = U.Mathf.PerlinNoise((float)(seed*3 + point.z) * perlinScale, (float)(seed*3 + point.y) * perlinScale),
				_passXZ = U.Mathf.PerlinNoise((float)(seed*5 + point.x) * perlinScale, (float)(seed*5 + point.z) * perlinScale);
			float _val = _passXY * _passZY * _passXZ;
			return _val;
		}
	}
}
