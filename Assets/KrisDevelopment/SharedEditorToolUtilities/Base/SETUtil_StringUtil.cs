﻿
	////////////////////////////////////////
	//    Shared Editor Tool Utilities    //
	//    by Kris Development             //
	////////////////////////////////////////

	//License: MIT
	//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System.Collections;
using System.Collections.Generic;

namespace SETUtil {
	public static class StringUtil {		
		public readonly static char[] defPathSeparators = {'.', '/', '\\'};
		
		//--word split
		public static string WordSplit (string str) {
			return WordSplit(str, (str.Length > 0) ? char.IsUpper(str[0]) : false);
		}
		public static string WordSplit (string str, bool firstIsUpper) {
			string _str2 = "";
			
			if(str.Length > 0)
				for(int i = 0; i < str.Length; i++){
					string st = str[i].ToString();
					_str2 += (i == 0 && firstIsUpper) ? st.ToUpper() : st;
					if(i < str.Length - 1)
						if(char.IsUpper(str[i + 1]))
							if(char.IsLower(str[i]))
								_str2 += " ";
				}
			
			return _str2;
		}
		
		public static string[] ToPathArray (string path){
			return path.Split(defPathSeparators);
		}
	}
}
