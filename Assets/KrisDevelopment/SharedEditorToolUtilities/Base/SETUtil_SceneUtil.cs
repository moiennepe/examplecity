﻿
	////////////////////////////////////////
	//    Shared Editor Tool Utilities    //
	//    by Kris Development             //
	////////////////////////////////////////

	//License: MIT
	//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System.Collections;
using System.Collections.Generic;
using U = UnityEngine;
using Gl = UnityEngine.GUILayout;
using SETUtil.Extend;

#if UNITY_EDITOR
using E = UnityEditor;
using EGl = UnityEditor.EditorGUILayout;
#endif

namespace SETUtil 
{
	public static class SceneUtil
	{
		///<summary> Calls appropriate destroy method based on the build conditions </summary>
		public static void SmartDestroy<T> (T instance) where T : U.Object
		{
			if(instance == null){
				EditorUtil.Debug("[SmartDestroy ERROR] Target instance is NULL.");
				return;
			}

			#if UNITY_EDITOR
				if(U.Application.isPlaying)
					U.Object.Destroy(instance);
				else
					U.Object.DestroyImmediate(instance, EditorUtil.ValidatePrefabTermination(instance));
			#else
				U.Object.Destroy(instance);
			#endif
		}
		
		///<summary> Calls SmartDestroy for reach element in the array </summary>
		public static void DestroyArray<T> (ref T[] instances) where T : U.Object
		{
			foreach(T instance in instances)
				SmartDestroy(instance);

			instances = new T[0];
		}
		
		///<summary> Spawns a prefab-linked instance if in editor and normal instance during run-time, at default coordinates. </summary>
		public static U.GameObject Instantiate(U.GameObject prefab) { return Instantiate(prefab, U.Vector3.zero, U.Quaternion.identity); }
		///<summary> Spawns a prefab-linked instance if in editor and normal instance during run-time, with default rotation. </summary>
		public static U.GameObject Instantiate(U.GameObject prefab, U.Vector3 position) { return Instantiate(prefab, position, U.Quaternion.identity); }
		///<summary> Spawns a prefab-linked instance if in editor and normal instance during run-time </summary>
		public static U.GameObject Instantiate(U.GameObject prefab, U.Vector3 position, U.Quaternion rotation)
		{
			U.GameObject _instance = null;
			if(prefab != null) {
				#if UNITY_EDITOR
					_instance = (U.GameObject) E.PrefabUtility.InstantiatePrefab(prefab);
					_instance.transform.position = position;
					_instance.transform.rotation = rotation;
				#else
					_instance = U.GameObject.Instantiate(prefab, position, rotation);
				#endif
			} else
				_instance = new U.GameObject();
			return _instance;
		}
		
		///<summary> Returns all children inside the given root transform, does not include parent.</summary>
		public static U.Transform[] CollectAllChildren (U.Transform root) { return CollectAllChildren(root, false); }
		///<summary> Returns all children inside the given root transform. </summary>
		public static U.Transform[] CollectAllChildren (U.Transform root, bool includeParent)
		{
			List<U.Transform> _children = new List<U.Transform>();
			IterateChildren(ref _children, root);
			if(!includeParent)
				_children.Remove(root);
			return _children.ToArray();
		}
		
		public static U.Vector3 FindRelativePosition (U.Vector3 point, U.Transform transform)
		{
			return new U.Vector3(U.Vector3.Project(transform.right, point - transform.position).x, U.Vector3.Project(transform.up, point - transform.position).y, U.Vector3.Project(transform.forward, point - transform.position).z);
		}

		public static U.Quaternion FindRelativeRotation (U.Quaternion worldRotation, U.Transform directParent)
		{
			return worldRotation * ((directParent) ? U.Quaternion.Inverse(directParent.rotation) : U.Quaternion.identity);  //returns the value unchanged if no parent is found
		}

		public static U.Vector3 ApplyRelativePosition (U.Vector3 point, U.Transform transform)
		{
			return transform.position + transform.right * point.x + transform.up * point.y + transform.forward * point.z;
		}

		//private methods:
		private static void IterateChildren (ref List<U.Transform> children, U.Transform currentNode)
		{
			//used by CollectAllChildren() to realize children iteration
			children.Add(currentNode);
			foreach(U.Transform child in currentNode)
				IterateChildren(ref children, child);
		}
		
	}
}
