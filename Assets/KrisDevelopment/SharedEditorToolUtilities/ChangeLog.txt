v1.5:
-Split the project utilities into multiple files

v1.6:
-Added Generic array drawer
-Added Component Ordering Utility

v1.9:
-Added DrawGUIElement

v1.12:
-Added SETUtil_Extend
-Fixed SceneUtil.Destroy runtime exception

v1.20:
-SceneUtil Generic Destroy methods
-MathUtil PerlinVolume

v1.21:
-"int orderIndex" of iOrderedComponent is now "int OrderIndex()"

v1.22:
-Removed Preferences serialization flag

v1.23:
-Removed EditorUtil serialization block methods
-Improved EditorUtil.BeginColorPocket() to utilize SETUtil.Common.Stack
-Fixed SceneUtil.Instantiate group;

v1.24:
-Navigation Field improvements

v1.31:
-Changed TransformData to struct instead of a class
-Deprecated ArrUtil as it was suggesting outdated programming standards
-Removed Common.Stack as it was obsolete
-Updated & Optimized ArrayFieldGUI
-Removed Char from StringUtil
-Removed GetChar from StringUtil
-Removed Internal Tools as they were unfitting to the purposes of the library

v1.35:
-ArrayFieldGUI ArrayFieldOption changed to bitmask

v1.36:
-Added ArrayFieldGUI List<T> overload
-ArrayFieldGUI now doesn't take ref parameters
-Improved NavigationFieldData

v1.36c:
-Added Common.Extend namespace
-Added /Common/Extend/SETUtil_StringExtend.cs
-Extension method: "string".ShowIf(condition)

v1.37:
-Added SceneUtil.FindRelativePosition
-Changed SceneUtil.RotationToLocal to SceneUtil.FindRelativeRotation
-Improved SceneUtil.DestroyArray
-Changed SceneUtil.Destroy to SceneUtil.SmartDestroy

v1.38:
-SETUtil types definitions moved to SETUtil.Types namespace
-Removed Add from CompUtil as obsolete
-Removed Delete from CompUtil as obsolete
-Removed HasComponent from CompUtil as obsolete
-Replaced GUIButtonDelegate with System.Action
-Added OBJExporter
-Added FileUtil
-Changed ValidatePrefabTermination check

v1.38.1:
-Fixed GetPrefabObject warning