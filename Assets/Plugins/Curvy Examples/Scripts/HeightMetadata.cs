// =====================================================================
// Copyright 2013-2018 ToolBuddy
// All rights reserved
// 
// http://www.toolbuddy.net
// =====================================================================

using UnityEngine;
using System.Collections;
using FluffyUnderware.DevTools;

namespace FluffyUnderware.Curvy.Examples
{
    public class HeightMetadata : CurvyMetadataBase, ICurvyInterpolatableMetadata<float>
    {
        [SerializeField]
        [RangeEx(0,1,Slider=true)]
#pragma warning disable 649
        float m_Height;
#pragma warning restore 649

        

        public object Value
        {
            get { return m_Height; }
        }

        public object InterpolateObject(ICurvyMetadata b, float f)
        {
            var mdb = b as HeightMetadata;
            return (mdb != null) ? Mathf.Lerp((float)Value, (float)mdb.Value, f) : Value;
        }

        public float Interpolate(ICurvyMetadata b, float f)
        {
            return (float)InterpolateObject(b, f);
        }

        
    }

}
